package com.example.doctoroncalls.Activityes

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.nfc.Tag
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.VolleyError
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.example.doctoroncalls.Models.*
import com.example.doctoroncalls.R
import kotlinx.android.synthetic.main.activity_update_profile.*
import org.json.JSONArray
import kotlin.math.log

class UpdateProfileActivity : LoaderActivity() {
    var status = ""
    private var urlUtills: UrlUtills? = UrlUtills()
    private var sessionManager: SessionManager? = null
    val SHARED_PREFS = "shared_prefs"
    val ID: String = "id"
    var AUTH_TOKEN = "AUTH_TOKEN"
    var loginId = ""
    private var stateList = ArrayList<String>()
    private var allStateList = ArrayList<StateL>()
    private val distList = ArrayList<String>()
    private val allDistList = ArrayList<Dist>()
    var name = ""
    var email = ""
    var experience = ""
    var specialty = ""
    var state = ""
    var dist = ""
    var license = ""
    var ambulance = ""
    var token = ""
    var stateId = ""
    var distId = ""
    var stateName = ""
    var address = ""
    var id = ""
    var distName = ""
    var gender = ""
    var onlineConsultationFees = ""
    var offlineConsultationFees = ""
    var sharedpreferences: SharedPreferences? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_update_profile)
        sessionManager = SessionManager(this@UpdateProfileActivity)
        sharedpreferences = getSharedPreferences(SHARED_PREFS, Context.MODE_PRIVATE)
        loginId = sharedpreferences?.getString(ID,"").toString()
        token = sharedpreferences?.getString(AUTH_TOKEN,"").toString()
        selectState()
        arrowBackUpdateProfile_imageView.setOnClickListener {
            val intent = Intent(this,SelectCatogaryActivity::class.java)
            startActivity(intent)
        }

        previous_btn.setOnClickListener {
            onBackPressed()
        }
        status = intent.getStringExtra("status").toString()
        var v = status
        Log.e("sts",v)
        if (status == "3"){
            specialty_editText.visibility = View.GONE
            licenceNo_editText.visibility = View.VISIBLE
            onlineConsultationFee_editText.visibility = View.GONE
            offlineConsultationFee_editText.visibility = View.GONE
            ambulanceNo_editText.visibility = View.VISIBLE
        } else if (status == "6"){
            specialty_editText.visibility = View.GONE
            licenceNo_editText.visibility = View.VISIBLE
            onlineConsultationFee_editText.visibility = View.GONE
            offlineConsultationFee_editText.visibility = View.GONE
        } else if (status == "2"){
            specialty_editText.visibility = View.GONE
            licenceNo_editText.visibility = View.VISIBLE
            onlineConsultationFee_editText.visibility = View.GONE
            offlineConsultationFee_editText.visibility = View.GONE
        }
        submitProfileDetails.setOnClickListener {
            validation()
        }

        state_editText.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                stateId = allStateList[p2].STATE_ID.toString()
                stateName = allStateList[p2].STATE_NAME.toString()
                //sessionManager?.locationDetails(stateName,"","","")
                selectDist()
            }

            override fun onNothingSelected(p0: AdapterView<*>?) {
                TODO("Not yet implemented")
            }
        }

        dist_editText.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                 distId = allDistList[p2].DISTRICT_ID.toString()
                distName = allDistList[p2].DISTRICT_NAME.toString()

               // sessionManager?.locationDetails("","",distId,distName)

                val sharedPreference =  getSharedPreferences("PREFERENCE_NAME", Context.MODE_PRIVATE)
                val editor = sharedPreference.edit()
                editor.putString("distid",distId)
                editor.putString("stateName",stateName)
                editor.putString("distName",distName)
                editor.putLong("l",100L)
                editor.apply()
            }

            override fun onNothingSelected(p0: AdapterView<*>?) {
                TODO("Not yet implemented")
            }
        }
    }
    private fun validation(){
          name = name_editText.text.toString().trim()
         email = email_editText.text.toString().trim()
        experience = experience_editText.text.toString().trim()
        specialty = specialty_editText.text.toString().trim()
        license = licenceNo_editText.text.toString().trim()
        onlineConsultationFees = onlineConsultationFee_editText.text.toString().trim()
        offlineConsultationFees = offlineConsultationFee_editText.text.toString().trim()
        address = address_editText.text.toString().trim()
        gender = gender_editText.text.toString().trim()
        ambulance = ambulanceNo_editText.text.toString().trim()
        if (name.isEmpty()){
            AlertDialog.Builder(this)
                .setMessage("please enter your name")
                .show()
        } else if (email.isEmpty()) {
            AlertDialog.Builder(this)
                .setMessage("please enter your email")
                .show()
        }
        else if (experience.isEmpty()){
            AlertDialog.Builder(this)
                .setMessage("please enter your experience")
                .show()
        } else if (address.isEmpty()){
            AlertDialog.Builder(this)
                .setMessage("please enter your experience")
                .show()
        }else if (gender.isEmpty()){
            AlertDialog.Builder(this)
                .setMessage("please enter your experience")
                .show()
        }
        else{
            updateProfile()
        }
    }

    private fun updateProfile(){
        var volleyRequestQueue: RequestQueue? = null
        val TAG = "Handy Opinion Tutorials"

        volleyRequestQueue = Volley.newRequestQueue(this)
        val strReq: StringRequest = object : StringRequest(Method.POST,urlUtills?.updateBusinessProfile(), Response.Listener { response ->
            Log.e(TAG, "response: " + response)
            try {
                dismissLoaderActivity()
                val jsonArray = JSONArray("[$response]")
                val jsonObject = jsonArray.getJSONObject(0)
                val message = jsonObject.getString("message")
                if (message == "updated"){
                    val intent = Intent(this,UpdateKYCActivity::class.java)
                    intent.putExtra("status", status)
                    startActivity(intent)
                    finish()
                }
            } catch (e: Exception) {
                Toast.makeText(this, "Something went wrong!!!", Toast.LENGTH_SHORT).show()
                e.printStackTrace()
            }
        },
            Response.ErrorListener { volleyError -> // error occurred
                Toast.makeText(applicationContext, "Server encountered some problem !!!", Toast.LENGTH_SHORT).show()
            }) {

            override fun getParams(): MutableMap<String, String> {
                val params: MutableMap<String, String> = java.util.HashMap()
                params.put("name", name)
                params.put("email", email)
                params.put("experience", experience)
                params.put("speciality", specialty)
                params.put("state", status)
                params.put("district", distId)
                params.put("status", status)
                params.put("loginid",loginId)
                params.put("licence_no",license)
                params.put("ambulance_no", ambulance)
                params.put("consultationFee", onlineConsultationFees)
                params.put("offlineConsultationFee",offlineConsultationFees)
                params.put("gender", gender)
                params.put("address",address)
                return params
            }

            override fun getHeaders(): Map<String, String> {

                val headers: MutableMap<String, String> = HashMap()
                headers.put("Authorizations", token)
                return headers
            }
        }
        volleyRequestQueue?.add(strReq)
        showLoaderActivity()
    }

    private fun selectState(){
        var volleyRequestQueue: RequestQueue? = null
        val TAG = "Handy Opinion Tutorials"

        volleyRequestQueue = Volley.newRequestQueue(this)
        val strReq: StringRequest = object : StringRequest(Method.POST,urlUtills?.stateApi(), Response.Listener { response ->
            Log.e(TAG, "response: " + response)
            try {
                //   dismissLoaderActivity()
                val jsonArray = JSONArray("[$response]")
                val messageObj = jsonArray.getJSONObject(0).get("STATE") as JSONArray
                for (i in 0 until messageObj.length()) {
                    val stateId = messageObj.getJSONObject(i).getString("STATE_ID")
                    val stateName = messageObj.getJSONObject(i).getString("STATE_NAME")
                    val countryId = messageObj.getJSONObject(i).getString("COUNTRY_ID")
                    val state = StateL(stateId,stateName,countryId)
                    allStateList.add(state)
                    stateList.add(stateName)
                }
                showStateList()
                Log.d("profile", stateList.toString())

            } catch (e: Exception) { // caught while parsing the response
                //dismissLoaderActivity()
                Toast.makeText(this, "Something went wrong!!!", Toast.LENGTH_SHORT).show()
                e.printStackTrace()
            }
        },
            Response.ErrorListener { volleyError -> // error occurred
                //
                //dismissLoaderActivity()
                Toast.makeText(applicationContext, "Server encountered some problem !!!", Toast.LENGTH_SHORT).show()
            }) {
            override fun getParams(): MutableMap<String, String> {
                val params: MutableMap<String, String> = java.util.HashMap()
                return params
            }
            override fun getHeaders(): Map<String, String> {
                val headers: MutableMap<String, String> = HashMap()
                headers.put("Authorizations", token)
                return headers
            }
        }
        volleyRequestQueue?.add(strReq)
        //showLoaderActivity()
    }

    private fun showStateList(){
        val adapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, stateList)
        state_editText.adapter = adapter
    }


    private fun selectDist(){
        var volleyRequestQueue: RequestQueue? = null
        val TAG = "Handy Opinion Tutorials"

        volleyRequestQueue = Volley.newRequestQueue(this)
        val strReq: StringRequest = object : StringRequest(Method.POST,urlUtills?.distApi() , Response.Listener { response ->
            Log.e(TAG, "response: " + response)
            try {
                dismissLoaderActivity()
                val jsonArray = JSONArray("[$response]")
                val messageObj = jsonArray.getJSONObject(0).get("DISTRICT") as JSONArray
                val item = messageObj.length()
                if (!distList.isEmpty()) distList.clear()
                if (!allDistList.isEmpty()) allDistList.clear()
                for (i in 0 until messageObj.length()) {
                    var distId = messageObj.getJSONObject(i).getString("DISTRICT_ID")
                    var distName = messageObj.getJSONObject(i).getString("DISTRICT_NAME")
                    val dist = Dist(distId, distName)
                    allDistList.add(dist)
                    distList.add(distName)
                    showDistList()
                }

            } catch (e: Exception) { // caught while parsing the response
                dismissLoaderActivity()
                Toast.makeText(this, "Something went wrong!!!", Toast.LENGTH_SHORT).show()
                e.printStackTrace()
            }
        },
            Response.ErrorListener { volleyError -> // error occurred
                dismissLoaderActivity()
                //
                Toast.makeText(applicationContext, "Server encountered some problem !!!", Toast.LENGTH_SHORT).show()
            }) {

            override fun getParams(): MutableMap<String, String> {
                val params: MutableMap<String, String> = java.util.HashMap()
                params.put("stateid", stateId)
                return params
            }

            override fun getHeaders(): Map<String, String> {
                val headers: MutableMap<String, String> = HashMap()
                // Add your Header paramters here
                headers.put("Authorizations", token)
                return headers
            }
        }
        // Adding request to request queue
        volleyRequestQueue?.add(strReq)
        showLoaderActivity()
    }

    private fun showDistList(){
        val dataAdapter = ArrayAdapter(applicationContext, android.R.layout.simple_spinner_item,distList)
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        dist_editText.adapter = dataAdapter
    }
}