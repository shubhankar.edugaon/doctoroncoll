package com.example.doctoroncalls.Fragments

import android.app.AlertDialog
import android.content.*
import android.graphics.Color
import android.os.Bundle
import android.os.Handler
import android.provider.MediaStore
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.example.doctoroncalls.Models.*
import com.example.doctoroncalls.R
import com.example.doctoroncalls.Activityes.DoctorProfileListActivity
import com.example.doctoroncalls.Activityes.SelectLocationActivity
import kotlinx.android.synthetic.main.fragment_services.*
import kotlinx.android.synthetic.main.fragment_services.view.*
import kotlinx.android.synthetic.main.location_item.view.*
import kotlinx.android.synthetic.main.meeting_schedule_items.*


class ServicesFragment : LoaderFragment() {

lateinit var hemoDoctor: LinearLayout
lateinit var allopathyDoctor: LinearLayout
lateinit var ayuravedaDoctor: LinearLayout
lateinit var pathology: LinearLayout
lateinit var ambulance: LinearLayout
lateinit var chemist: LinearLayout
lateinit var location: ImageView
lateinit var locationName: TextView
    private var sessionManager: SessionManager? = null
    var distId:String = ""
    val KEY_DIST_ID = "KEY_DIST_ID"
    val KEY_DIST_NAME = "KEY_DIST_NAME"
    private var urlUtills: UrlUtills? = UrlUtills()
    var distName = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {
        // Inflate the layout for this fragment
        val view =  inflater.inflate(R.layout.fragment_services, container, false)

        hemoDoctor = view.findViewById(R.id.homopathy_layout)
        location = view.findViewById(R.id.locationIcon_imageView)
        locationName = view.findViewById(R.id.location_address)
        allopathyDoctor = view.findViewById(R.id.allopathy_doctor)
        ayuravedaDoctor = view.findViewById(R.id.secondRow_layout)
        pathology = view.findViewById(R.id.pathology_layout)
        ambulance = view.findViewById(R.id.thirdRow_layout)
        chemist = view.findViewById(R.id.chemist_layout)

        location.setOnClickListener {
            val intent = Intent(activity?.applicationContext, SelectLocationActivity::class.java)
            startActivity(intent)
        }
        hemoDoctor.setOnClickListener {
            val intent = Intent(activity?.applicationContext,DoctorProfileListActivity::class.java)
            intent.putExtra("id", "4")
            intent.putExtra("distId", distId)
            startActivity(intent)
        }
        allopathyDoctor.setOnClickListener {
            val intent = Intent(activity?.applicationContext,DoctorProfileListActivity::class.java)
                intent.putExtra("id","1")
                intent.putExtra("distId",distId)
            startActivity(intent)
        }
        pathology.setOnClickListener {
            val intent = Intent(activity?.applicationContext,DoctorProfileListActivity::class.java)
            intent.putExtra("id","2")
            intent.putExtra("distId",distId)
            startActivity(intent)
        }
        ayuravedaDoctor.setOnClickListener {
            val intent = Intent(activity?.applicationContext,DoctorProfileListActivity::class.java)
                intent.putExtra("id","5")
                intent.putExtra("distId",distId)
            startActivity(intent)
        }
        ambulance.setOnClickListener {
            val intent = Intent(activity?.applicationContext,DoctorProfileListActivity::class.java)
            intent.putExtra("id","3")
            intent.putExtra("distId",distId)
            startActivity(intent)
        }
        chemist.setOnClickListener {
            val intent = Intent(activity?.applicationContext, DoctorProfileListActivity::class.java)
            intent.putExtra("id","6")
            intent.putExtra("distId",distId)
            startActivity(intent)
        }
//        view.swipeRefresh.setOnClickListener {
            sessionManager = activity?.let { SessionManager(it.applicationContext) }
            val sharedPreference =
                activity?.getSharedPreferences("PREFERENCE_NAME", Context.MODE_PRIVATE)
            val state = sharedPreference?.getString("stateName", "")
            distId = sharedPreference?.getString("distid", "").toString()
            var distName = sharedPreference?.getString("distName", "")
            if (distName !=null) {
                locationName.visibility = View.VISIBLE
                locationName.text = distName
            }



        return view
    }
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

           }

    override fun onResume() {
        super.onResume()
        sessionManager = activity?.let { SessionManager(it.applicationContext) }
        val sharedPreference = activity?.getSharedPreferences("PREFERENCE_NAME", Context.MODE_PRIVATE)
        distId = sharedPreference?.getString("distid", "").toString()
         distName = sharedPreference?.getString("distName", "").toString()
        if (distName != null) {
            locationName.visibility = View.VISIBLE
            locationName.text = distName
        }
        if (distId.isEmpty()){
            LocationnDialog()
        }else{
            locationName.visibility  = View.VISIBLE
            locationName.text = distName
        }
    }

    private fun LocationnDialog(){
        val mDialogView = LayoutInflater.from(activity).inflate(R.layout.location_item, null)
        val mBuilder = androidx.appcompat.app.AlertDialog.Builder(this.requireContext()).setView(mDialogView)
        val  mAlertDialog = mBuilder.show()
        mAlertDialog.setCanceledOnTouchOutside(false)
        mDialogView.location_title.text = "Select Location"
        mDialogView.locationHome_button.setOnClickListener {
           val intent = Intent(activity,SelectLocationActivity::class.java)
            startActivity(intent)
            mAlertDialog.dismiss()
        }
        mDialogView.locationCancel_button.setOnClickListener {
           mAlertDialog.dismiss()
        }
        mBuilder.setCancelable(false)
    }
}