package com.example.doctoroncalls.Models

import com.android.volley.toolbox.StringRequest

object DoctorModels {
        var authId = String()
        var token = String()
        var phone = String()
        var searchPhone  = String()
        var name = String()
        var contact = String()
        var email = String()
        var gender = String()
        var  address  = String()
        var appLinks = String()
        var image = String()
}

class OrderList(
    val BRANCH_NAME:String = "",
    val BRANCH_CONTACT:String = "",
    val PFL_ORDER_ID:String = "",
    val BRANCH_TT:String = "",
    val DOCTOR_CONSULTATION_FEE:String = "",
    var PFL_PATIENT_NAME: String = "",
    var PFL_PATIENT_AGE: String = "",
    var PFL_REPORT_FILE: String = "",
    var PFL_PATIENT_PHONE: String = "",
    var PROFILE_STATUS:String = "",
    var BRANCH_ID: String = "",
    var PFL_STATUS: String = "",
    var PFL_DATE: String = "",
    var PFL_PRICE: String = "",
    var PATIENT_PHOTO: String = "",
    var PFL_BRANCH_ID: String = ""

)

class DoctorList(
    var PATIENT_NAME: String? = null,
    var BRANCH_NAME: String? = null,
    var PATIENT_ADDRESS:String? = null,
    var DA_DISEAS:String? = null,
    var APPOINT_DATE:String? = null,
    var MESSAGE:String? = null,
    var DA_ID:String? = null,
    var BRANCH_TOKEN: String? = null,
    var DA_LOGIN_ID: String? = null,
    var DMS_START_TIME: String? = null,
    var DMS_END_TIME: String? = null,
    var PATIENT_PHOTO: String?  =  null,
    var PATIENT_MOBILE:String? = null,
    var DA_MEETING_LINK:String? = null,
    var APPOINT_TYPE:String? =  null

)

class FAQList(
    var FQUESTION: String? = null,
    var FANSWER: String? = null
)


class Doctor(
    var BRANCH_ID: String? = null,
    var FIRM_ID: String? = null,
    var BRANCH_NAME: String? = null,
    var BRANCH_CODE: String? = null,
    var BRANCH_CONTACT: String? = null,
    var PROFILE_STATUS:String? = null,
    var DOCTOR_CONSULTATION_FEE: String? = null,
    var BRANCH_EMAIL: String? = null,
    var EXPERIENCE: String? = null,
    var LOCATION:String? = null,
    var BRANCH_TOKEN:String? = null,
    var PATIENT_PHOTO:String? = null,
    var BRANCH_ADDRESS:String? = null,
    var SPECIALITY:String? = null,
    var BRANCH_ONLINE_FEE:String? = null,
    var BRANCH_STATUS:String? = null,
    var LICENCE_NO: String? = null,
    var AMBULANCE_NO: String? = null
)
class ImageList(
    var slider_image: String? = null
)
class SearchDoctorName(
    var name: String? = null
)
class SearchView(
    var BRANCH_NAME:String? = null
)

object Location{
    var distName = String()
    var stateName = String()
}

class Days(
    var DAYS_NAME:String? = null,
    var DAYS_ID:String? = null
)
class MeetingSchedule(
    var ONLINE_OFFLINE_STATUS:String? = null,
    var DISTRICT_NAME:String? = null,
    var STATE_NAME:String? = null,
    var DAYS_NAME:String? = null,
    var DMS_START_TIME:String? = null,
    var DMS_END_TIME:String? = null,
    var DMS_ENABLE_DISABLE: String? = null,
    var DMS_ID: String? = null
)
class UpcomingList(
    var DMS_END_TIME: String? = null,
    var APPOINT_DATE: String? = null,
    var DMS_START_TIME: String? = null,
    var DISTRICT_NAME:String? = null,
    var STATE_NAME: String? = null,
    var PATIENT_NAME: String? = null,
    var PATIENT_MOBILE:String? = null,
    var PATIENT_ADDRESS: String? = null,
    var APPOINT_TYPE: String? = null,
    var PATIENT_BOOKING_ID:String? = null,
    var BRANCH_NAME:String? = null,
    var DOCTOR_CONSULTATION_FEE:String? = null,
    var DMS_DAY_ID:String? = null,
    var PATIENT_PHOTO:String? = null,
    var DA_LOGIN_ID:String? = null,
    var DA_ID:String? = null,
    var BRANCH_TOKEN:String? = null,
    var MESSAGE:String? = null
)

class HospitalList(
    var DOS_HOSPITAL_NAME: String? = null,
    var DMS_START_TIME: String? = null,
    var DMS_END_TIME: String? = null,
    var DAYS_NAME: String? = null,
    var DOS_ADDRESS: String? = null,

)
