package com.example.doctoroncalls.Models

class UrlUtills {
        var mainUrl = "http://software.doctoroncalls.in/android/"
        var domainName = "http://software.doctoroncalls.in/"

    fun register(): String{
        var url = mainUrl+"customer_signup"
            return url
    }
    fun login(): String{
        var url = mainUrl+"/user_login"
        return url
    }
    fun stateApi(): String{
        val url = mainUrl+"/get_states"
        return url
    }
    fun distApi(): String{
        val url = mainUrl+"/get_district"
        return url
    }
    fun doctorDetailsListAPi():String{
        val url = mainUrl+"get_profile_by_status?status="
        return url
    }

    fun searchDoctor(): String{
        val url = mainUrl+"get_profile_by_name"
        return url
    }
    fun prescriptionDetails(): String{
        val url = mainUrl+"uploads_patient_prescription"
            return url
    }
    fun bookAppointment():String{
        val url = mainUrl+"/add_appointment"
        return url
    }
    fun availableSlots():String{
        val url = mainUrl+"get_available_slots"
        return url
    }

    fun availableAppointmentTime(): String{
        val url = mainUrl+"get_appointment_detail_by_date"
        return url
    }
    fun appointList():String{
        val url = mainUrl+"get_appointment_list"
        return url
    }
    fun orderList():String{
        val url = mainUrl+"get_order_list"
            return url
    }
    fun giveFeedBack():String{
        val url = mainUrl+"add_feedback"
        return url
    }
    fun faqDetails(): String{
        val url = mainUrl+"fquestion_list"
        return url
    }
    fun termsAndCondition(): String{
        val url = domainName+"terms-conditions"
        return url
    }
    fun privacyPolicy():String{
        val url = domainName+"privacy-policy"
        return url
    }
    fun updateProfile():String{
        val  url = mainUrl+"update_kyc"
        return url
    }
    fun updateAppoint():  String{
        val url = mainUrl+"update_appointment"
        return url
    }
    fun getSliderImages(): String{
        val url = mainUrl+"get_sliders"
        return url
    }
    fun getCurrentBalance(): String{
        val  url = mainUrl+"get_current_balance"
        return url
    }
    fun addAmount(): String{
        val url = mainUrl+"add_wallet"
        return url
    }
    fun getUserDetails(): String{
        val url = mainUrl+"get_my_profile"
        return url
    }
    fun aboutUs():String{
        val url = domainName+"about-us"
        return url
    }
    fun updateBusinessProfile():String{
        val url = mainUrl+"update_business_profile"
        return url
    }
    fun referalId():String{
        val url = mainUrl+"verify_referal_code"
        return url
    }
    fun updateKYc(): String{
        val url = mainUrl+"upload_bank_detail"
        return url
    }
    fun addUserImage():String{
        val url = mainUrl+"uploadUserImage"
        return url
    }

    fun addAddharFont(): String{
        val url = mainUrl+"uploadAadharFontPhoto"
        return url
    }
    fun addAadharBack(): String{
        val url = mainUrl+"uploadAadharBackPhoto"
        return url
    }
    fun getDayApi(): String{
        val  url = mainUrl+"get_days"
        return url
    }
    fun updateAppointment(): String{
        val url = mainUrl+"update_appointment"
        return url
    }
    fun createSchedule():String{
        val url  = mainUrl+"schedule_meeting"
        return url
    }

    fun meetingSchedule(): String{
        val url = mainUrl+"get_meeting_schedule"
        return url
    }
    fun updateMeetingSchedule(): String{
        val  url = mainUrl+"update_meeting_schedule"
        return url
    }
}