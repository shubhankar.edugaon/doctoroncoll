package com.example.doctoroncalls.Activityes

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.widget.FrameLayout
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import com.example.doctoroncalls.Fragments.*
import com.example.doctoroncalls.Models.SessionManager
import com.example.doctoroncalls.R
import com.google.android.material.bottomnavigation.BottomNavigationView

class DashActivity : AppCompatActivity() {
    lateinit var frameLayout: FrameLayout
    private var sessionManager: SessionManager? = null
    var drawerLayout: DrawerLayout? = null
    var distId = ""
    var actionBarDrawerToggle: ActionBarDrawerToggle? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dash)
        distId = intent.getStringExtra("distId").toString()
        frameLayout = findViewById(R.id.fragment_frameLayout)

//        if (distId != null){
//            openFragment(ServicesFragment())
//        }
        setupNavigationView()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.mymenu, menu)
        return true
    }

    private fun openFragment(fragment: Fragment?) {
        if (fragment == null) return
        val fragmentManager = supportFragmentManager
        try {
            if (fragmentManager != null) {
                val transaction: FragmentTransaction = supportFragmentManager.beginTransaction()
                if (transaction != null) {
                    transaction.replace(R.id.swipeRefresh, fragment, "aman")
                    transaction.commit()
                }
            }
        }catch (e : Exception){
            Log.d("openFrag", "" + e.message)
        }

    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId
        when(id){
            R.id.termsAndCondition ->{
                startActivity(Intent(this,TermsAndCondicationActivity::class.java))
            }
            R.id.privacyPolicy -> {
                startActivity(Intent(this,PrivacyPolicyActivity::class.java))
            }
            R.id.logOut ->{
                sessionManager?.logoutUser(this)
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }


    private fun setupNavigationView() {
        val bottomNavigationView: BottomNavigationView? = findViewById(R.id.bottom_navigation) as BottomNavigationView?
        if (bottomNavigationView != null) {
            // Select first menu item by default and show Fragment accordingly.
            val menu: Menu = bottomNavigationView.getMenu()
            selectFragment(menu.getItem(0))

            // Set action to perform when any menu-item is selected.
            bottomNavigationView.setOnNavigationItemSelectedListener(
                object : BottomNavigationView.OnNavigationItemSelectedListener {
                    override fun onNavigationItemSelected(item: MenuItem): Boolean {
                        selectFragment(item)
                        return false
                    }
                })
        }
    }

    protected fun selectFragment(item: MenuItem) {
        item.isChecked = true
        when (item.itemId) {
            R.id.home ->                 // Action to perform when Home Menu item is selected.
                pushFragment(HomeFragment())
            R.id.serviceList ->               //  Toast.makeText(this, "work is under progress", Toast.LENGTH_SHORT).show();
                supportFragmentManager.beginTransaction()
                    .replace(R.id.fragment_frameLayout, OrderListFragment(), "loanno").commit()
            R.id.transation ->                 //Toast.makeText(this, "work is under progress", Toast.LENGTH_SHORT).show();
                supportFragmentManager.beginTransaction()
                    .replace(R.id.fragment_frameLayout, TransationFragment()).commit()
//            R.id.Listing ->
//                supportFragmentManager.beginTransaction()
//                    .replace(R.id.fragment_frameLayout,MyProfileListFragment()).commit()
            R.id.services ->                 //Toast.makeText(this, "work is under progress", Toast.LENGTH_SHORT).show();
                supportFragmentManager.beginTransaction()
                    .replace(R.id.fragment_frameLayout, ServicesFragment()).commit()
        }
    }

    fun pushFragment(fragment: Fragment?) {
        if (fragment == null) return
        val fragmentManager = fragmentManager
        if (fragmentManager != null) {
            val transaction: FragmentTransaction = supportFragmentManager.beginTransaction()
            if (transaction != null) {
                transaction.replace(R.id.fragment_frameLayout, fragment, "aman")
                transaction.commit()
            }
        }
    }
}