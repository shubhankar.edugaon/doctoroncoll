package com.example.doctoroncalls.Adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.example.doctoroncalls.Fragments.AppointmentPendingFragment
import com.example.doctoroncalls.Fragments.ApponitmetCompletedFragment
import com.example.doctoroncalls.Fragments.MeetingUpcomigFragment


class TabPageAdapter(activity: FragmentActivity, private val tabCount: Int): FragmentStateAdapter(activity){
    override fun getItemCount(): Int = tabCount

    override fun createFragment(position: Int): Fragment {
        return when (position){
            0 -> AppointmentPendingFragment()
            1 -> ApponitmetCompletedFragment()
            2 -> MeetingUpcomigFragment()
            else ->AppointmentPendingFragment()
        }
    }

}