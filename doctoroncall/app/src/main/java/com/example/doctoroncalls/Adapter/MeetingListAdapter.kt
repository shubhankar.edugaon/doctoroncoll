package com.example.doctoroncalls.Adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.ui.graphics.Color
import androidx.recyclerview.widget.RecyclerView
import com.example.doctoroncalls.Models.Doctor
import com.example.doctoroncalls.Models.FAQList
import com.example.doctoroncalls.Models.Location.stateName
import com.example.doctoroncalls.Models.MeetingSchedule
import com.example.doctoroncalls.R
import kotlinx.android.synthetic.main.faq_item_view.view.*
import kotlinx.android.synthetic.main.meeting_schedule_items.view.*
import java.util.zip.Inflater

class MeetingListAdapter(var meetingScheduleList: List<MeetingSchedule>, private val onItemClickListener: OnItemClickListener): RecyclerView.Adapter<MeetingListAdapter.MeetinhScheduleListHolder>(){
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MeetinhScheduleListHolder{
        return MeetinhScheduleListHolder(LayoutInflater.from(parent.context).inflate(R.layout.meeting_schedule_items,parent,false), onItemClickListener)
    }

    override fun onBindViewHolder(holder: MeetinhScheduleListHolder, position: Int) {
        holder.bind(meetingScheduleList[position])
        val data = meetingScheduleList[position]
        holder.meetingSchedule = data
    }

    override fun getItemCount(): Int {
        return meetingScheduleList.size
    }

    class MeetinhScheduleListHolder(itemView: View, onItemClickListener: OnItemClickListener): RecyclerView.ViewHolder(itemView){
        @SuppressLint("ResourceAsColor")
        lateinit var meetingSchedule: MeetingSchedule
        var enableDisableValue  = ""
        @SuppressLint("ResourceAsColor")
        fun bind(meetingSchedule: MeetingSchedule){
             enableDisableValue = meetingSchedule.DMS_ENABLE_DISABLE.toString()
           itemView.stateName.text = meetingSchedule.STATE_NAME
           itemView.distName.text = meetingSchedule.DISTRICT_NAME
           itemView.dayName.text = meetingSchedule.DAYS_NAME
            itemView.time.text = meetingSchedule.DMS_START_TIME+" - "+meetingSchedule.DMS_END_TIME
           if (meetingSchedule.ONLINE_OFFLINE_STATUS == "1"){
               itemView.appointmentType.text = "online Appointment"
           } else if (meetingSchedule.ONLINE_OFFLINE_STATUS == "2"){
               itemView.appointmentType.text = "offline Appointment"
           }
            if (enableDisableValue == "1"){
                itemView.buttonEnableDisable.text = "Click to Enable"
                itemView.meeting_details.setBackgroundColor(R.color.red)
                itemView.buttonEnableDisable.setBackgroundColor(R.color.AppBackgroundColor)
            } else if (enableDisableValue == "0"){
                itemView.buttonEnableDisable.text ="Click to Disable"
                }
        }
        init {
            itemView.buttonEnableDisable.setOnClickListener {
                onItemClickListener.editClick(meetingSchedule)
            }
        }
    }
    interface OnItemClickListener{
        fun editClick(meetingSchedule: MeetingSchedule){
        }
    }
}