package com.example.doctoroncalls.Activityes

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.widget.*
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentTransaction
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.example.doctoroncalls.Models.*
import com.example.doctoroncalls.R
import kotlinx.android.synthetic.main.activity_select_location.*
import kotlinx.android.synthetic.main.location_item.view.*
import org.json.JSONArray


class SelectLocationActivity : FragmentActivity() {
    lateinit var selectState: Spinner
    lateinit var selectDist: Spinner
    lateinit var stateNameT: TextView
    lateinit var arrowBack: ImageView
    private var urlUtills: UrlUtills? = UrlUtills()
    var stateId = ""
    val KEY_LOGIN_TOKEN = "KEY_LOGIN_TOKEN"
    var AUTH_TOKEN = "AUTH_TOKEN"
    var distId = ""
    var stateName = ""
    var id = ""
    var distName = ""
    var auth_token = ""
    var states = java.util.HashMap<String, String>()
    private var stateList = ArrayList<String>()
    private var allStateList = ArrayList<StateL>()
    private val distList = ArrayList<String>()
    private val allDistList = ArrayList<Dist>()
    private var sessionManager: SessionManager? = null
    var token = ""
    var sharedpreferences: SharedPreferences? = null
    val SHARED_PREFS = "shared_prefs"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_select_location)
        selectState = findViewById(R.id.state_list)
        selectDist = findViewById(R.id.select_dist)
        arrowBack = findViewById(R.id.arrowBack)
        submit_btn.setOnClickListener {
          LocationnDialog()
        }


        sessionManager = SessionManager(this@SelectLocationActivity)
        id = intent.getStringExtra("id").toString()
        sharedpreferences = getSharedPreferences(SHARED_PREFS, Context.MODE_PRIVATE)
        auth_token = sharedpreferences?.getString(AUTH_TOKEN,"").toString()
        var b = auth_token
        arrowBack.setOnClickListener {
            onBackPressed()
        }
            selectState()

        selectState.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                stateId = allStateList[p2].STATE_ID.toString()
                 stateName = allStateList[p2].STATE_NAME.toString()
                sessionManager?.locationDetails(stateName,"","","")
                selectDist()
            }

            override fun onNothingSelected(p0: AdapterView<*>?) {
                TODO("Not yet implemented")
            }
        }

        selectDist.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
               val distId = allDistList[p2].DISTRICT_ID.toString()
                 distName = allDistList[p2].DISTRICT_NAME.toString()

                sessionManager?.locationDetails("","",distId,distName)

                val sharedPreference =  getSharedPreferences("PREFERENCE_NAME", Context.MODE_PRIVATE)
                val editor = sharedPreference.edit()
                editor.putString("distid",distId)
                editor.putString("stateName",stateName)
                editor.putString("distName",distName)
                editor.putLong("l",100L)
                editor.apply()
            }

            override fun onNothingSelected(p0: AdapterView<*>?) {
                TODO("Not yet implemented")
            }
        }
    }

    fun fragmentMethod() {
        Toast.makeText(this, "Method called From Fragment",
            Toast.LENGTH_LONG).show()
    }

    private fun selectState(){
        var volleyRequestQueue: RequestQueue? = null
        val TAG = "Handy Opinion Tutorials"

        volleyRequestQueue = Volley.newRequestQueue(this)
        val strReq: StringRequest = object : StringRequest(Method.POST,urlUtills?.stateApi(), Response.Listener { response ->
            Log.e(TAG, "response: " + response)
            try {
             //   dismissLoaderActivity()
                val jsonArray = JSONArray("[$response]")
                val messageObj = jsonArray.getJSONObject(0).get("STATE") as JSONArray
                        for (i in 0 until messageObj.length()) {
                            val stateId = messageObj.getJSONObject(i).getString("STATE_ID")
                            val stateName = messageObj.getJSONObject(i).getString("STATE_NAME")
                            val countryId = messageObj.getJSONObject(i).getString("COUNTRY_ID")
                            val state = StateL(stateId,stateName,countryId)
                            allStateList.add(state)
                            stateList.add(stateName)
                        }
                showStateList()
                Log.d("profile", stateList.toString())

            } catch (e: Exception) { // caught while parsing the response
                //dismissLoaderActivity()
                Toast.makeText(this, "Something went wrong!!!", Toast.LENGTH_SHORT).show()
                e.printStackTrace()
            }
        },
            Response.ErrorListener { volleyError -> // error occurred
                //
                //dismissLoaderActivity()
                Toast.makeText(applicationContext, "Server encountered some problem !!!", Toast.LENGTH_SHORT).show()
            }) {
            override fun getParams(): MutableMap<String, String> {
                val params: MutableMap<String, String> = java.util.HashMap()
                return params
            }
            override fun getHeaders(): Map<String, String> {
                val headers: MutableMap<String, String> = HashMap()
                headers.put("Authorizations", auth_token)
                return headers
            }
        }
        volleyRequestQueue?.add(strReq)
        //showLoaderActivity()
    }

    private fun showStateList(){
        val adapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, stateList)
        selectState.adapter = adapter
    }


    private fun selectDist(){
        var volleyRequestQueue: RequestQueue? = null
        val TAG = "Handy Opinion Tutorials"

        volleyRequestQueue = Volley.newRequestQueue(this)
        val strReq: StringRequest = object : StringRequest(Method.POST,urlUtills?.distApi() , Response.Listener { response ->
            Log.e(TAG, "response: " + response)
            try {
               // dismissLoaderActivity()
                val jsonArray = JSONArray("[$response]")
                val messageObj = jsonArray.getJSONObject(0).get("DISTRICT") as JSONArray
                val item = messageObj.length()
                if (!distList.isEmpty()) distList.clear()
                if (!allDistList.isEmpty()) allDistList.clear()
                for (i in 0 until messageObj.length()) {
                    var distId = messageObj.getJSONObject(i).getString("DISTRICT_ID")
                    var distName = messageObj.getJSONObject(i).getString("DISTRICT_NAME")
                    val dist = Dist(distId,distName)
                    allDistList.add(dist)
                    distList.add(distName)
                    showDistList()
                }


            } catch (e: Exception) { // caught while parsing the response
                //dismissLoaderActivity()
                Toast.makeText(this, "Something went wrong!!!", Toast.LENGTH_SHORT).show()
                e.printStackTrace()
            }
        },
            Response.ErrorListener { volleyError -> // error occurred
               // dismissLoaderActivity()
                //
                Toast.makeText(applicationContext, "Server encountered some problem !!!", Toast.LENGTH_SHORT).show()
            }) {

            override fun getParams(): MutableMap<String, String> {
                val params: MutableMap<String, String> = java.util.HashMap()
                params.put("stateid", stateId)
                return params
            }

            override fun getHeaders(): Map<String, String> {
                val headers: MutableMap<String, String> = HashMap()
                // Add your Header paramters here
                headers.put("Authorizations", auth_token)
                return headers
            }
        }
        // Adding request to request queue
        volleyRequestQueue?.add(strReq)
        //showLoaderActivity()
    }

    private fun showDistList(){
//        val adapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, distList)
//        selectDist.adapter = adapter

        val dataAdapter = ArrayAdapter(applicationContext, android.R.layout.simple_spinner_item,distList)
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        selectDist.adapter = dataAdapter
    }

    private fun pushFragment(fragment: Fragment?) {
        if (fragment == null) return
        val fragmentManager = supportFragmentManager
        try {
            if (fragmentManager != null) {
                val transaction: FragmentTransaction = supportFragmentManager.beginTransaction()
                if (transaction != null) {
                    transaction.replace(R.id.blank_fragment, fragment, "aman")
                    transaction.commit()
                }
            }
        }catch (e : Exception){
            Log.d("openFrag", "" + e.message)
        }
    }

    private fun LocationnDialog(){
        val mDialogView = LayoutInflater.from(this).inflate(R.layout.location_item, null)
        val mBuilder = androidx.appcompat.app.AlertDialog.Builder(this).setView(mDialogView)
        val  mAlertDialog = mBuilder.show()
        mAlertDialog.setCanceledOnTouchOutside(false)
        mDialogView.success_location.visibility = View.VISIBLE
        mDialogView.locationName.visibility = View.GONE
        mDialogView.location_title.visibility = View.GONE
        mDialogView.locationHome_button.visibility = View.GONE
        mDialogView.ok_button.visibility = View.VISIBLE
        mDialogView.locationCancel_button.visibility = View.GONE
        mDialogView.locationHome_button.setOnClickListener {
            onBackPressed()
        }
        mDialogView.ok_button.setOnClickListener {
            onBackPressed()
        }
        mBuilder.setCancelable(false)
    }
}


