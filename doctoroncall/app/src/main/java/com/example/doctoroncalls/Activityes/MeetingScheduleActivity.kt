package com.example.doctoroncalls.Activityes

import android.app.AlertDialog
import android.app.TimePickerDialog
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.widget.*
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.example.doctoroncalls.Adapter.DaysAdapter
import com.example.doctoroncalls.Adapter.MeetingListAdapter
import com.example.doctoroncalls.Models.*
import com.example.doctoroncalls.R
import kotlinx.android.synthetic.main.activity_meeting_schedule.*
import org.json.JSONArray
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class MeetingScheduleActivity : LoaderActivity(),DaysAdapter.OnItemClickListener, MeetingListAdapter.OnItemClickListener {
    var selectedDate = ""
    private var urlUtills: UrlUtills = UrlUtills()
    private var PRIVATE_MODE = 0
    private val PREF_NAME = "mindorks-welcome"
    private var sessionManager: SessionManager? = null
    private var dayList = ArrayList<Days>()
    private var meetingScheduleList = ArrayList<MeetingSchedule>()
    val SHARED_PREFS = "shared_prefs"
    var sharedpreferences: SharedPreferences? = null
    var id = ""
    val ID = "id"
    var AUTH_TOKEN = "AUTH_TOKEN"
    private var stateList = ArrayList<String>()
    private var allStateList = ArrayList<StateL>()
    private val distList = ArrayList<String>()
    private val allDistList = ArrayList<Dist>()
    var distId = ""
    var stateName = ""
    var stateId = ""
    var distName = ""
    var startTime = ""
    var endTime = ""
    var status = ""
    var day = ""
    var day_id = ""
    var appointmentType = ""
    var dayLimits = ""
    var hospitalName = ""
    var message = ""
    var limits = ""
    var meetingId = ""
    var loginToken = ""
    var hospitlaAddress = ""
    var startTimeAMPM = ""
    var endTimeAMPM = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_meeting_schedule)
        sessionManager = SessionManager(this@MeetingScheduleActivity)
        sharedpreferences = getSharedPreferences(SHARED_PREFS, Context.MODE_PRIVATE)
        loginToken = sharedpreferences?.getString(AUTH_TOKEN,"").toString()
        id = sharedpreferences?.getString(ID,"").toString()
        status = intent.getStringExtra("status").toString()
        status = intent.getStringExtra("status").toString()
        startTimeAMPM()
        endTimeTimeAMPM()
        submit_schedule.setOnClickListener {
            validation()
        }
        arrowBack_meeting.setOnClickListener {
            onBackPressed()
        }
        selectState()
        selectStatus_button.setOnClickListener{
            selectCatogaryDialog()
        }
        slectAppointmentType.setOnClickListener {
            walletsDialog()
        }
        check_list.setOnClickListener {
            mailItemLayout.visibility = View.GONE
            list_itemLayout.visibility = View.VISIBLE
            check_list.visibility = View.GONE
        }
        gotTo.setOnClickListener {
            val intent = Intent(this,DashActivity::class.java)
            startActivity(intent)
        }
        state_spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                stateId = allStateList[p2].STATE_ID.toString()
                stateName = allStateList[p2].STATE_NAME.toString()
              //  sessionManager?.locationDetails(stateName, "", "", "")
                
//                if (stateName != null) {
//                    state_textView.text = stateName
//                    state_spinner.visibility = View.GONE
//                }
//                change_state.setOnClickListener {
//                    state_spinner.visibility = View.VISIBLE
//                    state_textView.visibility = View.GONE
//                }
                selectDist()
            }

            override fun onNothingSelected(p0: AdapterView<*>?) {
                TODO("Not yet implemented")
            }
        }

        dist_spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                 distId = allDistList[p2].DISTRICT_ID.toString()
                distName = allDistList[p2].DISTRICT_NAME.toString()

                //sessionManager?.locationDetails("", "", distId, distName)

//                val sharedPreference = getSharedPreferences("PREFERENCE_NAME", Context.MODE_PRIVATE)
//                val editor = sharedPreference.edit()
//                editor.putString("distid", distId)
//                editor.putString("stateName", stateName)
//                editor.putString("distName", distName)
//                editor.putLong("l", 100L)
//                editor.apply()
            }

            override fun onNothingSelected(p0: AdapterView<*>?) {
                TODO("Not yet implemented")
            }
        }


        arrowBack_meeting.setOnClickListener {
            onBackPressed()
        }
        getDay()
        fromTime_textView.setOnClickListener {
            medicineFromTime()
        }
        toTime_textView.setOnClickListener {
            medicineToTime()
        }
    }

    private fun validation() {
        startTime = fromTime_textView.text.toString().trim()
        endTime = toTime_textView.text.toString().trim()
        dayLimits = appointmentLint_edit.toString().trim()
         limits = appointmentLint_edit.text.toString().trim()
        hospitalName = hispitalName_edit.text.toString().trim()
        hospitlaAddress = hispitalAddress_edit.text.toString().trim()

        if (startTime.isEmpty()) {
            AlertDialog.Builder(this)
                .setMessage("Select start time")
                .show()
        } else if (limits.isEmpty()){
            AlertDialog.Builder(this)
                .setMessage("Select a appointment limits")
                .show()
        }
        else if (endTime.isEmpty()) {
            AlertDialog.Builder(this)
                .setMessage("Select end time")
                .show()
        } else if (distId.isEmpty()) {
            AlertDialog.Builder(this)
                .setMessage("Select district")
                .show()
        } else if (status.isEmpty()){
            AlertDialog.Builder(this)
                .setMessage("Select a status")
                .show()
        }
        else if (stateName.isEmpty()) {
            AlertDialog.Builder(this)
                .setMessage("Select state")
                .show()
        } else if (dayLimits.isEmpty()){
            AlertDialog.Builder(this)
                .setMessage("Select day appointment limits")
                .show()
        } else if (hospitalName.isEmpty()){
            AlertDialog.Builder(this)
                .setMessage("Select day appointment limits")
                .show()
        } else if (hospitlaAddress.isEmpty()){
            AlertDialog.Builder(this)
                .setMessage("Select day appointment limits")
                .show()
        } else if(startTimeAMPM.isEmpty()){
            AlertDialog.Builder(this)
                .setMessage("Select AM PM time")
                .show()
        } else if (endTimeAMPM.isEmpty()){
            AlertDialog.Builder(this)
                .setMessage("Select AM PM time")
                .show()
        }
        else{
            addSchedule()
        }
    }
    private fun selectState() {
        var volleyRequestQueue: RequestQueue? = null
        val TAG = "Handy Opinion Tutorials"

        volleyRequestQueue = Volley.newRequestQueue(this)
        val strReq: StringRequest = object :
            StringRequest(Method.POST, urlUtills?.stateApi(), Response.Listener { response ->
                Log.e(TAG, "response: " + response)
                try {
                    dismissLoaderActivity()
                    val jsonArray = JSONArray("[$response]")
                    val messageObj = jsonArray.getJSONObject(0).get("STATE") as JSONArray
                    for (i in 0 until messageObj.length()) {
                        val stateId = messageObj.getJSONObject(i).getString("STATE_ID")
                        val stateName = messageObj.getJSONObject(i).getString("STATE_NAME")
                        val countryId = messageObj.getJSONObject(i).getString("COUNTRY_ID")
                        val state = StateL(stateId, stateName, countryId)
                        allStateList.add(state)
                        stateList.add(stateName)
                    }
                    showStateList()

                } catch (e: Exception) { // caught while parsing the response
                    dismissLoaderActivity()
                    Toast.makeText(this, "Something went wrong!!!", Toast.LENGTH_SHORT).show()
                    e.printStackTrace()
                }
            },
                Response.ErrorListener { volleyError -> // error occurred
                    //
                    dismissLoaderActivity()
                    Toast.makeText(
                        applicationContext,
                        "Server encountered some problem !!!",
                        Toast.LENGTH_SHORT
                    ).show()
                }) {
            override fun getParams(): MutableMap<String, String> {
                val params: MutableMap<String, String> = java.util.HashMap()
                return params
            }

            override fun getHeaders(): Map<String, String> {
                val headers: MutableMap<String, String> = HashMap()
                headers.put("Authorizations", loginToken)
                return headers
            }
        }
        volleyRequestQueue.add(strReq)
//        showLoaderActivity()
    }

    private fun showStateList() {
        val adapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, stateList)
        state_spinner.adapter = adapter
    }

    private fun fromTime() {
        val mTimePicker: TimePickerDialog
        val mcurrentTime = Calendar.getInstance()
        val hour = mcurrentTime.get(Calendar.HOUR_OF_DAY)
        val minute = mcurrentTime.get(Calendar.MINUTE)
        mTimePicker = TimePickerDialog(this, object : TimePickerDialog.OnTimeSetListener {
            override fun onTimeSet(view: TimePicker?, hourOfDay: Int, minute: Int) {
                fromTime_textView.setText(String.format("%d : %d", hourOfDay, minute))
            }
        }, hour, minute, false)
    }

    private fun medicineFromTime() {
        val cal = Calendar.getInstance()
        val timeListener = TimePickerDialog.OnTimeSetListener { timePicker, hour, minute ->

            cal.set(Calendar.HOUR_OF_DAY, hour)
            cal.set(Calendar.MINUTE, minute)
            fromTime_textView.text = SimpleDateFormat("HH: mm").format(cal.time)
            if (startTime !=null){

            }
        }

        TimePickerDialog(
            this,
            timeListener,
            cal.get(Calendar.HOUR_OF_DAY),
            cal.get(Calendar.MINUTE),
            true
        ).show()
    }

    private fun medicineToTime() {
        val cal = Calendar.getInstance()
        val timeListener = TimePickerDialog.OnTimeSetListener { timePicker, hour, minute ->

            cal.set(Calendar.HOUR_OF_DAY, hour)
            cal.set(Calendar.MINUTE, minute)
            toTime_textView.text = SimpleDateFormat("HH: mm").format(cal.time)
        }

        TimePickerDialog(
            this,
            timeListener,
            cal.get(Calendar.HOUR_OF_DAY),
            cal.get(Calendar.MINUTE),
            true
        ).show()

    }

    private fun getDay() {
        var volleyRequestQueue: RequestQueue? = null
        val TAG = "Handy Opinion Tutorials"

        volleyRequestQueue = Volley.newRequestQueue(this)
        val strReq: StringRequest = object : StringRequest(
            Method.POST, urlUtills?.getDayApi(), Response.Listener { response ->
                Log.e(TAG, "response: " + response)
                dismissLoaderActivity()
                try {
                    val jsonArray = JSONArray("[$response]")
                    val messageObj = jsonArray.getJSONObject(0).get("days") as JSONArray
                    val count = messageObj.length()
                    for (i in 0 until messageObj.length()) {
                        val days_name = messageObj.getJSONObject(i).getString("DAYS_NAME")
                        val days_id  = messageObj.getJSONObject(i).getString("DAYS_ID")
                        val days = Days(days_name,days_id)
                        dayList.add(days)
                        if (dayList != null) {
                            showDaysList(dayList)
                        }
                    }
                } catch (e: Exception) { // caught while parsing the response
                    dismissLoaderActivity()
                    Toast.makeText(this, "Something went wrong!!!", Toast.LENGTH_SHORT).show()
                    e.printStackTrace()
                }
            },
            Response.ErrorListener { volleyError -> // error occurred
                dismissLoaderActivity()
                Toast.makeText(
                    applicationContext,
                    "Server encountered some problem !!!",
                    Toast.LENGTH_SHORT
                ).show()
            }) {

            override fun getParams(): MutableMap<String, String> {
                val params: MutableMap<String, String> = java.util.HashMap()
                return params
            }

            override fun getHeaders(): Map<String, String> {
                val headers: MutableMap<String, String> = HashMap()
                headers.put("Authorizations", loginToken)
                return headers
            }
        }
        volleyRequestQueue?.add(strReq)
        showLoaderActivity()
    }

    private fun selectDist() {
        var volleyRequestQueue: RequestQueue? = null
        val TAG = "Handy Opinion Tutorials"

        volleyRequestQueue = Volley.newRequestQueue(this)
        val strReq: StringRequest = object :
            StringRequest(Method.POST, urlUtills.distApi(), Response.Listener { response ->
                Log.e(TAG, "response: " + response)
                try {
                    dismissLoaderActivity()
                    val jsonArray = JSONArray("[$response]")
                    val messageObj = jsonArray.getJSONObject(0).get("DISTRICT") as JSONArray
                    val item = messageObj.length()
                    if (!distList.isEmpty()) distList.clear()
                    if (!allDistList.isEmpty()) allDistList.clear()
                    for (i in 0 until messageObj.length()) {
                        var distId = messageObj.getJSONObject(i).getString("DISTRICT_ID")
                        var distName = messageObj.getJSONObject(i).getString("DISTRICT_NAME")
                        val dist = Dist(distId, distName)
                        allDistList.add(dist)
                        distList.add(distName)
                        showDistList()
                    }

                } catch (e: Exception) { // caught while parsing the response
                    dismissLoaderActivity()
                    Toast.makeText(this, "Something went wrong!!!", Toast.LENGTH_SHORT).show()
                    e.printStackTrace()
                }
            },
                Response.ErrorListener { volleyError -> // error occurred
                    dismissLoaderActivity()
                    //
                    Toast.makeText(
                        applicationContext,
                        "Server encountered some problem !!!",
                        Toast.LENGTH_SHORT
                    ).show()
                }) {

            override fun getParams(): MutableMap<String, String> {
                val params: MutableMap<String, String> = java.util.HashMap()
                params.put("stateid", stateId)
                return params
            }

            override fun getHeaders(): Map<String, String> {
                val headers: MutableMap<String, String> = HashMap()
                // Add your Header paramters here
                headers.put("Authorizations", loginToken)
                return headers
            }
        }
        // Adding request to request queue
        volleyRequestQueue?.add(strReq)
        showLoaderActivity()
    }

    override fun onResume() {
        super.onResume()
        getMeetingDetails()
    }
    private fun showDistList() {
        val dataAdapter =
            ArrayAdapter(applicationContext, android.R.layout.simple_spinner_item, distList)
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        dist_spinner.adapter = dataAdapter
    }

    private fun showDaysList(daysList: ArrayList<Days>) {
        val faqAdapter = DaysAdapter(daysList, this)
        val layoutManage = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        calendarRecyclerView.layoutManager = layoutManage
        calendarRecyclerView.adapter = faqAdapter
    }

    override fun dayClick(days: Days) {
         day = days.DAYS_NAME.toString()
            day_id = days.DAYS_ID.toString()
        Toast.makeText(applicationContext, day, Toast.LENGTH_SHORT).show()
    }

    private fun addSchedule(){
        var volleyRequestQueue: RequestQueue? = null
        val TAG = "Handy Opinion Tutorials"

        volleyRequestQueue = Volley.newRequestQueue(this)
        val strReq: StringRequest = object : StringRequest(
            Method.POST, urlUtills?.createSchedule(), Response.Listener { response ->
                Log.e(TAG, "response: " + response)
                dismissLoaderActivity()
                try {
                    val jsonArray = JSONArray("[$response]")
                    val message = jsonArray.getJSONObject(0).getString("message")
                    if (message == "Added"){
                       Toast.makeText(this,"Schedule created",Toast.LENGTH_LONG).show()
                        successDialog()
                        getMeetingDetails()
                    } else if (message =="Already Added"){
                        getMeetingDetails()
                        Toast.makeText(this,"that time already Added",Toast.LENGTH_LONG).show()
                    } else if (message == ""){

                    }
                } catch (e: Exception) { // caught while parsing the response
                    dismissLoaderActivity()
                    Toast.makeText(this, "Something went wrong!!!", Toast.LENGTH_SHORT).show()
                    e.printStackTrace()
                }
            },
            Response.ErrorListener { volleyError -> // error occurred
                dismissLoaderActivity()
                Toast.makeText(
                    applicationContext,
                    "Server encountered some problem !!!",
                    Toast.LENGTH_SHORT
                ).show()
            }) {

            override fun getParams(): MutableMap<String, String> {
                val params: MutableMap<String, String> = java.util.HashMap()
                val sTime = startTime+" "+startTimeAMPM
                val eTime = endTime+" "+endTimeAMPM
                params.put("doctorid",id)
                params.put("starttime",sTime)
                params.put("endtime", eTime)
                params.put("status",status)
                params.put("limit",limits)
                params.put("dayid",day_id)
                params.put("districtid",distId)
                params.put("appointment_type",appointmentType)
                params.put("hospital_name", hospitalName)
                params.put("address",hospitlaAddress)
                return params
            }

            override fun getHeaders(): Map<String, String> {
                val headers: MutableMap<String, String> = HashMap()
                headers.put("Authorizations", loginToken)
                return headers
            }
        }
        volleyRequestQueue?.add(strReq)
        showLoaderActivity()
    }

    override fun editClick(meetingSchedule: MeetingSchedule) {
                meetingId = meetingSchedule.DMS_ID.toString()
        if(meetingId !=null){
            updateMeeting()
        }
    }
    private fun selectCatogaryDialog(){
        //Inflate the dialog with custom view
        val mDialogView = LayoutInflater.from(this).inflate(R.layout.doctor_status_dialog, null)
        //AlertDialogBuilder
        val mBuilder = AlertDialog.Builder(this).setView(mDialogView)
        val  mAlertDialog = mBuilder.show()
        val submit = mDialogView.findViewById<Button>(R.id.submit_button)
        val homeopathy = mAlertDialog.findViewById<CheckBox>(R.id.homeopathy_checkBox)
        val allopathic = mDialogView.findViewById<CheckBox>(R.id.allpathy_checkBox)
        val ayurdeva = mDialogView.findViewById<CheckBox>(R.id.ayurvedaLayot)
        mAlertDialog.setCanceledOnTouchOutside(false)
        submit.setOnClickListener {
            if (homeopathy.isChecked){
                val txt = "4"
                status = txt
                mAlertDialog.dismiss()
            } else if (allopathic.isChecked){
                val txt = "1"
                status = txt
                mAlertDialog.dismiss()
            } else if(ayurdeva.isChecked){
                val txt = "5"
                status = txt
                mAlertDialog.dismiss()
            }else{

            }
        }
        mBuilder.setCancelable(false)
    }

    private fun walletsDialog(){
        val mDialogView = LayoutInflater.from(this).inflate(R.layout.appointment_type, null)
        val mBuilder = androidx.appcompat.app.AlertDialog.Builder(this).setView(mDialogView)
        val appointmentTypeConfirmBtn = mDialogView.findViewById<Button>(R.id.appointmentType_Btn)
        var onlineApp = mDialogView.findViewById<CheckBox>(R.id.online_appointment)
        val offlineApp = mDialogView.findViewById<CheckBox>(R.id.offline_appointment)
        val  mAlertDialog = mBuilder.show()
        mAlertDialog.setCanceledOnTouchOutside(false)

        appointmentTypeConfirmBtn.setOnClickListener {
            if (onlineApp.isChecked){
                val txt ="1"
                message = "online Appointment"
                appointmentType = txt
                slectAppointmentType.text = message
                mAlertDialog.dismiss()
            } else if (offlineApp.isChecked){
                val txt = "2"
                message  = "offline Appointment"
                appointmentType = txt
                slectAppointmentType.text  = message
                mAlertDialog.dismiss()
            } else{
            }
        }
        mBuilder.setCancelable(false)
    }
    private fun getMeetingDetails(){
        var volleyRequestQueue: RequestQueue? = null
        val TAG = "Handy Opinion Tutorials"
        volleyRequestQueue = Volley.newRequestQueue(this)
        val strReq: StringRequest = object : StringRequest(
            Method.POST, urlUtills?.meetingSchedule(), Response.Listener { response ->
                dismissLoaderActivity()
                try {
                    val jsonArray = JSONArray("[$response]")
                    val message = jsonArray.getJSONObject(0).get("message")
                            val meetingArray = JSONArray("[$message]")
                    val meetingObj = meetingArray.getJSONObject(0).get("meetings") as JSONArray
                        meetingObj.length()
                        meetingScheduleList.clear()
                    for (i in 0 until meetingObj.length()){
                        val appointmentStatus = meetingObj.getJSONObject(i).getString("ONLINE_OFFLINE_STATUS")
                        val appointmentStartTime = meetingObj.getJSONObject(i).getString("DMS_START_TIME")
                        val appointmentEndTime = meetingObj.getJSONObject(i).getString("DMS_END_TIME")
                        val DISTRICT_NAME = meetingObj.getJSONObject(i).getString("DISTRICT_NAME")
                        val STATE_NAME = meetingObj.getJSONObject(i).getString("STATE_NAME")
                        val DAYS_NAME = meetingObj.getJSONObject(i).getString("DAYS_NAME")
                        val DMS_ENABLE_DISABLE = meetingObj.getJSONObject(i).getString("DMS_ENABLE_DISABLE")
                        var DMS_ID = meetingObj.getJSONObject(i).getString("DMS_ID")
                        val meetingSchedule = MeetingSchedule(appointmentStatus,DISTRICT_NAME,STATE_NAME,DAYS_NAME,appointmentStartTime,appointmentEndTime,DMS_ENABLE_DISABLE,DMS_ID)
                        meetingScheduleList.add(meetingSchedule)
                        if (meetingScheduleList !=null){
                            showList(meetingScheduleList)
                        }
                    }

                } catch (e: Exception) { // caught while parsing the response
                    dismissLoaderActivity()
                    Toast.makeText(this, "Something went wrong!!!", Toast.LENGTH_SHORT).show()
                    e.printStackTrace()
                }
            },
            Response.ErrorListener { volleyError -> // error occurred
                dismissLoaderActivity()
                Toast.makeText(
                    applicationContext, "Server encountered some problem !!!", Toast.LENGTH_SHORT
                ).show()
            }) {

            override fun getParams(): MutableMap<String, String> {
                val params: MutableMap<String, String> = java.util.HashMap()
                params.put("loginid",id)
                return params
            }

            override fun getHeaders(): Map<String, String> {
                val headers: MutableMap<String, String> = HashMap()
                headers.put("Authorizations", loginToken)
                return headers
            }
        }
        volleyRequestQueue?.add(strReq)
       // showLoaderActivity()
    }

    private fun showList(scheduleList: List<MeetingSchedule>){
        val doctorAdapter = MeetingListAdapter(scheduleList, this)
        val layoutManage = LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false)
        schedule_recyclerView.layoutManager = layoutManage
        schedule_recyclerView.adapter = doctorAdapter
    }

    private fun successDialog(){
            val mDialogView = LayoutInflater.from(this).inflate(R.layout.success_dialog, null)
            val mBuilder = androidx.appcompat.app.AlertDialog.Builder(this).setView(mDialogView)
                val ok = mDialogView.findViewById<Button>(R.id.ok_title)
            val  mAlertDialog = mBuilder.show()
            mAlertDialog.setCanceledOnTouchOutside(false)
                    ok.setOnClickListener {
                        mAlertDialog.dismiss()
                    }
            mBuilder.setCancelable(false)
        }

    private fun updateMeeting(){
        var volleyRequestQueue: RequestQueue? = null
        val TAG = "Handy Opinion Tutorials"

        volleyRequestQueue = Volley.newRequestQueue(this)
        val strReq: StringRequest = object : StringRequest(
            Method.POST, urlUtills?.updateMeetingSchedule(), Response.Listener { response ->
                Log.e(TAG, "response: " + response)
                dismissLoaderActivity()
                try {
                    val jsonArray = JSONArray("[$response]")
                    val message = jsonArray.getJSONObject(0).getString("message")
                    if (message == "Success"){
                        Toast.makeText(this,"Meeting update",Toast.LENGTH_LONG).show()
                        val intent = Intent(this,DashActivity::class.java)
                        startActivity(intent)
                        finish()
                    }
                } catch (e: Exception) { // caught while parsing the response
                    dismissLoaderActivity()
                    Toast.makeText(this, "Something went wrong!!!", Toast.LENGTH_SHORT).show()
                    e.printStackTrace()
                }
            },
            Response.ErrorListener { volleyError -> // error occurred
                dismissLoaderActivity()
                Toast.makeText(
                    applicationContext,
                    "Server encountered some problem !!!",
                    Toast.LENGTH_SHORT
                ).show()
            }) {

            override fun getParams(): MutableMap<String, String> {
                val params: MutableMap<String, String> = java.util.HashMap()
                params.put("meeting_id",meetingId)
                return params
            }

            override fun getHeaders(): Map<String, String> {
                val headers: MutableMap<String, String> = HashMap()
                headers.put("Authorizations", loginToken)
                return headers
            }
        }
        volleyRequestQueue?.add(strReq)
        showLoaderActivity()

    }
    private fun startTimeAMPM(){
        val languages = resources.getStringArray(R.array.time)
        if (FromTimeSpinner != null){
            val adapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, languages)
            FromTimeSpinner.adapter = adapter

            FromTimeSpinner.onItemSelectedListener = object: AdapterView.OnItemSelectedListener {
                override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                    startTimeAMPM = languages[position]
                    //Toast.makeText(this@MeetingScheduleActivity, startTimeAMPM + " " + "" + languages[position], Toast.LENGTH_SHORT
//                    ).show()
                }

                override fun onNothingSelected(p0: AdapterView<*>?) {
                    Toast.makeText(this@MeetingScheduleActivity, "", Toast.LENGTH_LONG).show()
                }
            }

        }
    }

    private fun endTimeTimeAMPM(){
        val languages = resources.getStringArray(R.array.time)
        if (EndTimeSpinner != null){
            val adapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, languages)
            EndTimeSpinner.adapter = adapter

            EndTimeSpinner.onItemSelectedListener = object: AdapterView.OnItemSelectedListener {
                override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                    endTimeAMPM = languages[position]
                    //Toast.makeText(this@MeetingScheduleActivity, startTimeAMPM + " " + "" + languages[position], Toast.LENGTH_SHORT
//                    ).show()
                }

                override fun onNothingSelected(p0: AdapterView<*>?) {
                    Toast.makeText(this@MeetingScheduleActivity, "", Toast.LENGTH_LONG).show()
                }
            }

        }
    }
}