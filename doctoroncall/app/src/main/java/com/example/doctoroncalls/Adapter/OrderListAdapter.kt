package com.example.doctoroncalls.Adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.doctoroncalls.Models.DoctorList
import com.example.doctoroncalls.Models.OrderList
import com.example.doctoroncalls.R
import kotlinx.android.synthetic.main.order_list_item.view.*

class OrderListAdapter(var OrderList: List<OrderList>, private val onItemClickListener: OnItemClickListener): RecyclerView.Adapter<OrderListAdapter.OrderListViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): OrderListAdapter.OrderListViewHolder {
       return OrderListViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.order_list_item, parent, false),onItemClickListener)
    }

    override fun onBindViewHolder(holder: OrderListAdapter.OrderListViewHolder, position: Int) {
            holder.bind(OrderList[position])
        val data = OrderList[position]
        holder.appointmentList = data
    }

    override fun getItemCount(): Int {
      return OrderList.size
    }
    class OrderListViewHolder(itemView: View, onItemClickListener: OnItemClickListener): RecyclerView.ViewHolder(itemView){
        lateinit var appointmentList: OrderList
        var prifleStatus:String = ""
            @SuppressLint("ResourceType")
            fun bind(orderList: OrderList) {
                itemView.orderId.text = "Order Id: " + orderList.PFL_ORDER_ID
                itemView.oderDate.text = orderList.BRANCH_TT
                itemView.orderFees.text = orderList.DOCTOR_CONSULTATION_FEE
                val profileStatus = orderList.PROFILE_STATUS
                if (orderList.PATIENT_PHOTO != null) {
                    Glide.with(itemView.context).load(orderList.PATIENT_PHOTO).into(itemView.orderImage_circleImageView)
                }else{
                    itemView.orderImage_circleImageView?.setCircleBackgroundColorResource(R.drawable.doctor)
                }
            }
        init {
                val profileView = itemView.findViewById<Button>(R.id.viewProfile_button)
                    profileView.setOnClickListener {
                        onItemClickListener.profileView(appointmentList)
                    }
        }
    }
    interface OnItemClickListener{
            fun profileView(orderList: OrderList)
    }
}


