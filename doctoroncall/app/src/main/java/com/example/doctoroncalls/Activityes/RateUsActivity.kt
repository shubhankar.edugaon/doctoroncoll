package com.example.doctoroncalls.Activityes

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.doctoroncalls.Models.DoctorModels
import com.example.doctoroncalls.R
import kotlinx.android.synthetic.main.activity_rate_us.*

class RateUsActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_rate_us)
        rateUs.setOnClickListener {
            onBackPressed()
        }
        links.text = DoctorModels.appLinks
        inviteFriends.setOnClickListener {
            val intent = Intent(Intent.ACTION_SEND)
            intent.putExtra(Intent.EXTRA_EMAIL, DoctorModels.appLinks)
            startActivity(Intent.createChooser(intent, "Select email"))
        }
    }


}