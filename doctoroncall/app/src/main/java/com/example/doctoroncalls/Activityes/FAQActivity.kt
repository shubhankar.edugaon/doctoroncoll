package com.example.doctoroncalls.Activityes

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.example.doctoroncalls.Adapter.FAQAdapter
import com.example.doctoroncalls.Models.*
import com.example.doctoroncalls.R
import kotlinx.android.synthetic.main.activity_faqactivity.*
import org.json.JSONArray
import java.util.HashMap

class FAQActivity : AppCompatActivity(),FAQAdapter.FaqOnItemClickListener {
    private var urlUtills: UrlUtills? = UrlUtills()
    private var faqList = ArrayList<FAQList>()
    var logginToken = ""
    val SHARED_PREFS = "shared_prefs"
    val KEY_LOGIN_TOKEN = "KEY_LOGIN_TOKEN"
    var sharedpreferences: SharedPreferences? = null
    private var sessionManager: SessionManager? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        sessionManager = SessionManager(this@FAQActivity)
        sharedpreferences = getSharedPreferences(SHARED_PREFS, Context.MODE_PRIVATE)
        logginToken = sharedpreferences?.getString(KEY_LOGIN_TOKEN,"").toString()

        setContentView(R.layout.activity_faqactivity)
        UploadPrescription()
        arroback.setOnClickListener {
            onBackPressed()
        }

    }

    private fun UploadPrescription(){
        var volleyRequestQueue: RequestQueue? = null
        val TAG = "Handy Opinion Tutorials"

        volleyRequestQueue = Volley.newRequestQueue(this)
        val strReq: StringRequest = object : StringRequest(Method.POST, urlUtills?.faqDetails(), Response.Listener { response ->
            Log.e(TAG, "response: " + response)
            try {
                val jsonArray = JSONArray("[$response]")
                val messageObj = jsonArray.getJSONObject(0).get("message")
                val confirmMessageArray = JSONArray("[$messageObj]")
                   val faq = confirmMessageArray.getJSONObject(0).get("fquestion")  as JSONArray
                faq.length()
                for (i in 0..faq.length()) {
                    val FQUESTION = faq.getJSONObject(i).get("FQUESTION")
                    val FANSWER = faq.getJSONObject(i).get("FANSWER")
                    val faqLis = FAQList(FANSWER.toString(), FQUESTION.toString())
                    faqList.add(faqLis)
                    showFaqList(faqList)
                }

            } catch (e: Exception) { // caught while parsing the response
                Toast.makeText(this, "Something went wrong!!!", Toast.LENGTH_SHORT).show()
                e.printStackTrace()
            }
        },
            Response.ErrorListener { volleyError -> // error occurred
                //
                Toast.makeText(applicationContext, "Server encountered some problem !!!", Toast.LENGTH_SHORT).show()
            }) {

            override fun getParams(): MutableMap<String, String> {
                val params: MutableMap<String, String> = java.util.HashMap()


                return params
            }

            override fun getHeaders(): Map<String, String> {
                val headers: MutableMap<String, String> = HashMap()
                params["Authorizations"] = logginToken
                return headers
            }
        }
        // Adding request to request queue
        volleyRequestQueue?.add(strReq)
    }
    private fun showFaqList(faqList: List<FAQList>){
        val faqAdapter = FAQAdapter(faqList,this)
        val layoutManage = LinearLayoutManager(this, LinearLayoutManager.VERTICAL,false)
        faq_recyclerView.layoutManager = layoutManage
        faq_recyclerView.adapter = faqAdapter
    }

    override fun onClick(faqList: FAQList) {
        val intent = Intent(this,FaqDetailsActivity::class.java)
        intent.putExtra("FQUESTION", faqList.FQUESTION)
        startActivity(intent)
    }


}