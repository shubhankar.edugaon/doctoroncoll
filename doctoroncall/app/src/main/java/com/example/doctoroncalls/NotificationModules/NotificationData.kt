package com.example.doctoroncalls.NotificationModules

data class NotificationData(
    val title: String,
    val message: String
)