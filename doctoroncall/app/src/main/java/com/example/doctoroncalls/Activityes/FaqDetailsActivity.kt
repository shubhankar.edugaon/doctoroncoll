package com.example.doctoroncalls.Activityes

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.doctoroncalls.R
import kotlinx.android.synthetic.main.activity_faq_details.*

class FaqDetailsActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_faq_details)
        val p = intent.getStringExtra("FQUESTION")
        answer_textView.text = p
        ok_textView.setOnClickListener {
            onBackPressed()
        }
    }
}