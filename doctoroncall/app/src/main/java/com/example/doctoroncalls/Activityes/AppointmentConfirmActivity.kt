package com.example.doctoroncalls.Activityes

import android.content.ContentValues.TAG
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.TextView
import com.example.doctoroncalls.Models.SessionManager
import com.example.doctoroncalls.NotificationModules.NotificationData
import com.example.doctoroncalls.NotificationModules.PushNotification
import com.example.doctoroncalls.NotificationModules.RetrofitInstance
import com.example.doctoroncalls.R
import com.google.firebase.messaging.FirebaseMessaging
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_appointment_confirm.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
const val TOPIC1 = "/topics/myTopic2"
class AppointmentConfirmActivity : LoaderActivity() {

    lateinit var patientName: TextView
    lateinit var patientNumber: TextView
    lateinit var appointmentTime: TextView
    lateinit var doctorFees: TextView
    lateinit var confirmAppointment: Button
    private var sessionManager: SessionManager? = null
    var sharedpreferences: SharedPreferences? = null
    val SHARED_PREFS = "shared_prefs"
    val BRANCH_NAME = "BRANCH_NAME"
    val BRANCH_ID = "BRANCH_ID"
    val DOCOR_ID = "DOCOR_ID"
    val PATIENT_ADDRESS = "PATIENT_ADDRESS"
    val PATIENT_NAME = "PATIENT_NAME"
    val PATIENT_PHONE = "PATIENT_PHONE"
    val APPOINTMENT_DATE = "APPOINTMENT_DATE"
    val APPOINT_SLOT = "APPOINT_SLOT"
    val APPOINT_TYPE = "APPOINT_TYPE"
    val PATIENT_BOOKING_ID = "PATIENT_BOOKING_ID"
    var BRANCH_TOKEN = ""
    var patientToken = ""
    var prescriptionToken = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_appointment_confirm)
        arroeBack.setOnClickListener {
            onBackPressed()
        }
        BRANCH_TOKEN = intent.getStringExtra("BRANCH_TOKEN").toString()

        val status = intent.getStringExtra("PROFILE_STATUS")
        var message = intent.getStringExtra("message")
        if (message == "ok"){
            getChemistAndPathDetails()
        }
        if (status == "4"){
            doctorAppoinmentDetais()
        } else if (status == "1"){
            doctorAppoinmentDetais()
        } else if (status == "5"){
            doctorAppoinmentDetais()
        }
        if(status == "6"){
            getChemistAndPathDetails()
        } else if (status == "3"){
            getChemistAndPathDetails()
            appointmentName.setOnClickListener {
                val title = "Your prescription schedule is created"
                val messageP = "Successfully"
                if (title.isNotEmpty() && messageP.isNotEmpty()){
                    PushNotification(
                        NotificationData(title, messageP),
                        BRANCH_TOKEN
                    ).also {
                     sendNotificationToPrescription(it)
                    }
                }
            }
        }
        FirebaseMessaging.getInstance().subscribeToTopic(TOPIC)
        appointmentName.setOnClickListener {
            val title = "Your appointment successfully, Go to Appointment List"
            val messageT = "appointment schedule"
            if (title.isNotEmpty() && messageT.isNotEmpty()) {
                PushNotification(
                    NotificationData(title, messageT),
                    BRANCH_TOKEN
                     ).also {
                    sendNotificationToPatient(it)
                }
            }
        }
            }

    private fun getChemistAndPathDetails(){
        val branchName = intent.getStringExtra("BRANCH_NAME")
        val referBy = intent.getStringArrayListExtra("REFERED_BY")
        val message = intent.getSerializableExtra("MESSAGE")
        val patientNam = intent.getStringExtra("PATIENT_NAME")
        val patienyNu = intent.getStringExtra("PATIENT_PHONE")
        val cosutltationFee = intent.getStringExtra("DOCTOR_CONSULTATION_FEE")
        doctorName.text = branchName
        patientName_title.text = patientNam
        mobileNumber.text = patienyNu
        appointmentDateTitle.visibility = View.GONE

    }

    private fun doctorAppoinmentDetais(){
        val branchName = intent.getStringExtra("BRANCH_NAME")
        val patientName = intent.getStringExtra("PATIENT_NAME")
        val patientMobile = intent.getStringExtra("PATIENT_MOBILE")
        val appointmentDate = intent.getStringExtra("APPOINT_DATE")
        val bookingId = intent.getStringExtra("PATIENT_BOOKING_ID")
        val specialist = intent.getStringExtra("SPECIALITY")
        doctorName.text = branchName.toString()
        doctorspecilat.text = specialist
        bookingIdt.text = bookingId
        patientName_title.text = patientName
        mobileNumber.text = patientMobile
        appointment_date.text = appointmentDate
    }

    private fun sendNotificationToPatient(notification: PushNotification) = CoroutineScope(Dispatchers.IO).launch {
        try {
            val response = RetrofitInstance.api.postNotification(notification)
            if (response.isSuccessful){
                Log.d(TAG,"Response: ${Gson().toJson(response)}")
                val intent = Intent(this@AppointmentConfirmActivity,DashActivity::class.java)
                startActivity(intent)
                finish()
            } else{
                Log.e(TAG, response.errorBody().toString())
            }
        } catch (e: Exception){
            Log.e(TAG, e.toString())
        }
    }

    private fun sendNotificationToPrescription(notification: PushNotification) = CoroutineScope(Dispatchers.IO).launch {
        try {
            val response = RetrofitInstance.api.postNotification(notification)
            if (response.isSuccessful){
                Log.d(TAG,"Response: ${Gson().toJson(response)}")
                val intent = Intent(this@AppointmentConfirmActivity,DashActivity::class.java)
                startActivity(intent)
                finish()
            } else{
                Log.e(TAG, response.errorBody().toString())
            }
        } catch (e: Exception){
            Log.e(TAG, e.toString())
        }
    }
    }
