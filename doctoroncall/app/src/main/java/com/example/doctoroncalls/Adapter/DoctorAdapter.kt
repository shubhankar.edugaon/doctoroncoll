package com.example.doctoroncalls.Adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.LinearLayout
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.doctoroncalls.Models.Doctor
import com.example.doctoroncalls.Models.HospitalList
import com.example.doctoroncalls.Models.SessionManager
import com.example.doctoroncalls.R
import kotlinx.android.synthetic.main.doctor_list_item.view.*

class DoctorAdapter(var doctorList: List<Doctor>, private var onItemClickListener: OnItemClickListener): RecyclerView.Adapter<DoctorAdapter.DoctorViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DoctorAdapter.DoctorViewHolder {
       return DoctorViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.doctor_list_item, parent, false), onItemClickListener)
    }

    override fun onBindViewHolder(holder: DoctorAdapter.DoctorViewHolder, position: Int) {
            holder.bind(doctorList[position])
        val data =doctorList[position]
            holder.doctorListHolder = data
    }

    override fun getItemCount(): Int {
      return doctorList.size
    }
    class DoctorViewHolder(itemView: View, onItemClickListener: OnItemClickListener): RecyclerView.ViewHolder(itemView){
        lateinit var doctorListHolder: Doctor
        private var sessionManager:SessionManager? = null
        var prifleStatus:String = ""
            fun bind(doctor: Doctor){
                        val name = itemView.findViewById<TextView>(R.id.name_listView)
                       // val bookAppointment = itemView.findViewById<CardView>(R.id.visit_clinic)
                        val collNow = itemView.findViewById<Button>(R.id.CollNow_button)
                        val addPrescription = itemView.findViewById<Button>(R.id.addPrescription_button)
                Glide.with(itemView.context).load(doctor.PATIENT_PHOTO).into(itemView.image_recyclerView)
                //itemView.consultFeeOnline.text = "₹"+doctor.BRANCH_ONLINE_FEE
                if (doctor.SPECIALITY !=null){
                    itemView.specilist_titile.text = doctor.SPECIALITY
                }else{
                    itemView.specilist_titile.text = "Speciality no found"
                }
                if (doctor.BRANCH_ONLINE_FEE !=null){
                    itemView.consultFeeOnline.text = "₹"+doctor.BRANCH_ONLINE_FEE
                }else{
                    itemView.onlineDataNotLayout.visibility = View.VISIBLE
                    itemView.onlineLayout.visibility = View.GONE
                }
                if (doctor.DOCTOR_CONSULTATION_FEE !=null){
                    itemView.consultFeeOffline.text = "₹"+doctor.DOCTOR_CONSULTATION_FEE
                }else{
                    itemView.offlineVisit_clinic.visibility = View.VISIBLE
                }
                itemView.experience_title.text = doctor.EXPERIENCE
                itemView.address_title.text = doctor.BRANCH_ADDRESS+"}"
                    name.text = doctor.BRANCH_NAME
                prifleStatus = doctor.PROFILE_STATUS.toString()

                if (prifleStatus == "6"){
                    itemView.onlineLayout.visibility = View.GONE
                    itemView.visit_clinic_consult.visibility = View.GONE
                    itemView.offlineVisit_clinic.visibility = View.GONE
                    itemView.onlineDataNotLayout.visibility = View.GONE
                    collNow.visibility = View.GONE
                    addPrescription.visibility = View.VISIBLE
                    itemView.specilist_titile.text = doctor.LICENCE_NO
                } else if (prifleStatus == "2"){
                    itemView.onlineLayout.visibility = View.GONE
                    itemView.visit_clinic_consult.visibility = View.GONE
                    itemView.offlineVisit_clinic.visibility = View.GONE
                    itemView.onlineDataNotLayout.visibility = View.GONE
                    collNow.visibility = View.GONE
                    addPrescription.visibility = View.VISIBLE
                    itemView.specilist_titile.text = doctor.LICENCE_NO
                }else{
                    //bookAppointment.visibility = View.VISIBLE
                    collNow.visibility = View.GONE
                    addPrescription.visibility = View.GONE
                }
                if(prifleStatus == "3"){
                    itemView.visit_clinic_consult.visibility = View.GONE
                    collNow.visibility = View.VISIBLE
                    itemView.specilist_titile.text = doctor.AMBULANCE_NO
                    itemView.onlineLayout.visibility = View.GONE
                    itemView.visit_clinic_consult.visibility = View.GONE
                    itemView.offlineVisit_clinic.visibility = View.GONE
                    itemView.onlineDataNotLayout.visibility = View.GONE
                    itemView.addPrescription_button.visibility = View.GONE
                    itemView.experience_title.text = doctor.LICENCE_NO
                    itemView.specilist_titile.text = doctor.AMBULANCE_NO
                }
                 else{
                    //bookAppointment.visibility = View.VISIBLE
                    collNow.visibility = View.GONE
                }
            }
        init {
            val viewButton = itemView.findViewById<TextView>(R.id.viewProfile)
                viewButton.setOnClickListener {
                onItemClickListener.viewButton(doctorListHolder)
            }
            val callNow = itemView.findViewById<Button>(R.id.CollNow_button)
            callNow.setOnClickListener {
                onItemClickListener.callToAmbulance(doctorListHolder)
            }
            val offlineBookAppointment = itemView.findViewById<CardView>(R.id.visit_clinic_consult)
            offlineBookAppointment.setOnClickListener {
                onItemClickListener.bookAppointment(doctorListHolder)
//                doctorList.appointmentType = "2"
            }
            val onlineConsult = itemView.findViewById<CardView>(R.id.onlineLayout)
            onlineConsult.setOnClickListener {
                onItemClickListener.onlineConsult(doctorListHolder)
//                doctorList.appointmentType = "1"
            }
            val addPrescription = itemView.findViewById<Button>(R.id.addPrescription_button)
                addPrescription.setOnClickListener {
                    onItemClickListener.addPrescription(doctorListHolder)
                }
            itemView.image_recyclerView.setOnClickListener {
                onItemClickListener.viewProfile(doctorListHolder)
            }
        }
    }
    interface OnItemClickListener{
        fun viewButton(doctor: Doctor)
        fun bookAppointment(doctor: Doctor)
        fun addPrescription(doctor: Doctor)
        fun callToAmbulance(doctor: Doctor)
        fun onlineConsult(doctor: Doctor)
        fun viewProfile(doctor: Doctor)
    }
}


