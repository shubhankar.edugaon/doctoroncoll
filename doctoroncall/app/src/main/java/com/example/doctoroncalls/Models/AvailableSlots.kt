package com.example.doctoroncalls.Models

class AvailableSlots {
   val slots: Slots? = Slots()
}

class Slots(
        val DMS_START_TIME: String? = null,
        val DMS_END_TIME: String? = null,
        val DMS_ID: String? = null
)
