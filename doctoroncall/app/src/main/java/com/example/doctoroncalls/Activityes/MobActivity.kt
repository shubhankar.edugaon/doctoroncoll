package com.example.doctoroncalls.Activityes

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import android.widget.Button
import android.widget.RelativeLayout
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import com.chaos.view.PinView
import com.example.doctoroncalls.Models.DoctorModels
import com.example.doctoroncalls.Models.SessionManager
import com.example.doctoroncalls.Models.UserMobileVerifcation
import com.example.doctoroncalls.R
import com.google.android.material.textfield.TextInputEditText
import com.google.firebase.FirebaseException
import com.google.firebase.auth.*
import java.util.concurrent.TimeUnit

class MobActivity : AppCompatActivity() {
        lateinit var doctorNumberTIET: TextInputEditText
    lateinit var sendCodeBtn: Button
    lateinit var pinView: PinView
    lateinit var verifyNoBtn: Button
    lateinit var phoneLayout: RelativeLayout
    lateinit var otpLayout: RelativeLayout
    private lateinit var loader: AlertDialog
    private var sessionManager: SessionManager? = null
    private var userMobileVerifcation: UserMobileVerifcation? = UserMobileVerifcation
    var number: String = ""
    var VERIF_ID = "VERIFICATION_ID"
    var getVerfyId:String? = ""
    // firebase
    lateinit var resendingToken: PhoneAuthProvider.ForceResendingToken
    lateinit var mCallBack: PhoneAuthProvider.OnVerificationStateChangedCallbacks
    lateinit var storedVerificationId: String
    private var auth: FirebaseAuth = FirebaseAuth.getInstance()
    var sharedpreferences: SharedPreferences? = null
    val SHARED_PREFS = "shared_prefs"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_mob)
    sessionManager = SessionManager(this@MobActivity)
        doctorNumberTIET = findViewById(R.id.mobile_edit_text)
        sendCodeBtn = findViewById(R.id.sendOTP)
        phoneLayout = findViewById(R.id.phoneLayout)
        otpLayout = findViewById(R.id.otp_layout)
        pinView = findViewById(R.id.doctorEnterOtp_PinView)
        verifyNoBtn = findViewById(R.id.verfy_otp)

        sendCodeBtn.setOnClickListener {
            if (doctorNumberTIET.text?.isEmpty() == true) {
                doctorNumberTIET.error = "Please enter your mobile number"
                doctorNumberTIET.requestFocus()
                return@setOnClickListener
            } else {
                number = doctorNumberTIET.text.toString().trim()
                // sessionManager?.createMobileVerification(number,"")
                userMobileVerifcation?.phone = number
                sendVerificationCode(number)
            }
        }

        verifyNoBtn.setOnClickListener {
            val otpCode = pinView.text.toString().trim()
            if (TextUtils.isEmpty(otpCode)) {
                pinView.error = "Please enter OTP!"
                pinView.requestFocus()
                return@setOnClickListener
            } else {
                verifyVerificationCode(otpCode)
            }
        }
        mCallBack = object: PhoneAuthProvider.OnVerificationStateChangedCallbacks(){
            override fun onVerificationCompleted(phoneAuthCredential:  PhoneAuthCredential) {
                if (phoneAuthCredential.smsCode!!.isNotEmpty()){
                    pinView.setText(phoneAuthCredential.smsCode)
                }
            }

            override fun onVerificationFailed(e: FirebaseException) {
                e.printStackTrace()
                Toast.makeText(this@MobActivity,"Mobile number register", Toast.LENGTH_LONG).show()
            }

            override fun onCodeSent(verificationId: String, token: PhoneAuthProvider.ForceResendingToken) {
                storedVerificationId =verificationId
                resendingToken = token
                phoneLayout.visibility =View.GONE
                otpLayout.visibility = View.VISIBLE
            }
        }
    }
    private fun sendVerificationCode(phone: String){
        val optin = PhoneAuthOptions.newBuilder(auth)
            .setPhoneNumber("+91$phone")
            .setTimeout(60L, TimeUnit.SECONDS)
            .setCallbacks(mCallBack)
            .setActivity(this)
            .build()
        PhoneAuthProvider.verifyPhoneNumber(optin)
    }
    private fun verifyVerificationCode(code:String){
        val credential = PhoneAuthProvider.getCredential(storedVerificationId!!, code)
        signInWithPhoneAuthCredential(credential)
    }
    private fun signInWithPhoneAuthCredential(credential: PhoneAuthCredential){
        auth.signInWithCredential(credential)
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful){
                   val user = task.result.user!!.uid
//                    if (user != null) {
//                        isTokenAvailable(user)
//                    }
                    sessionManager?.createMobileVerification(number,user)
//                    Log.e("",user)
                    val intent = Intent(this,RegisterActivityActivity::class.java)
                    intent.putExtra("authId",user)
                    intent.putExtra("userMobile", number)
                    DoctorModels.authId = user
                    intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or  Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP
                    startActivity(intent)
                    finish()
                } else{
                    if (task.exception is FirebaseAuthInvalidCredentialsException){
                        Toast.makeText(this,"Authentication Failed",Toast.LENGTH_LONG).show()
                    }
                }
            }
    }
}