package com.example.doctoroncalls.Activityes

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import com.example.doctoroncalls.R
import kotlinx.android.synthetic.main.activity_select_catogary.*

class SelectCatogaryActivity : AppCompatActivity() {
        var status = ""
    override fun onCreate(savedInstanceState: Bundle?){
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_select_catogary)
        arrowBack_slectProfession.setOnClickListener {
            onBackPressed()
        }
        ayurvedaLayout.setOnClickListener {
            nextButton.visibility  = View.VISIBLE
            status = "5"
        }
        homeopathyLayout.setOnClickListener {
            nextButton.visibility = View.VISIBLE
            status = "4"
        }
        AllopathicLayout.setOnClickListener {
            status  = "1"
            nextButton.visibility = View.VISIBLE
        }
        PathologyLayout.setOnClickListener {
            status = "2"
            nextButton.visibility = View.VISIBLE
        }
        ChemistLayout.setOnClickListener {
            status = "6"
            nextButton.visibility = View.VISIBLE
        }
        AmbulanceLayout.setOnClickListener {
            status = "3"
            nextButton.visibility = View.VISIBLE
        }
        nextButton.setOnClickListener {
            val intent = Intent(this,UpdateProfileActivity::class.java)
                intent.putExtra("status", status)
            var b = status
            Log.e("status", b)
            startActivity(intent)
            finish()
        }
    }
}