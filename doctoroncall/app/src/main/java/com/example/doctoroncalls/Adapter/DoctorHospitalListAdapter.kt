package com.example.doctoroncalls.Adapter


import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.doctoroncalls.Models.HospitalList
import com.example.doctoroncalls.R
import kotlinx.android.synthetic.main.doctor_hospital_list.view.*

class DoctorHospitalListAdapter(var doctorHospitalList: List<HospitalList>): RecyclerView.Adapter<DoctorHospitalListAdapter.HospitalViewHolder>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DoctorHospitalListAdapter.HospitalViewHolder {
     return HospitalViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.doctor_hospital_list,parent,false))
    }

    override fun onBindViewHolder(holder: DoctorHospitalListAdapter.HospitalViewHolder, position: Int) {
       holder.bind(doctorHospitalList[position])
        val data = doctorHospitalList[position]
    }

    override fun getItemCount(): Int {
        return doctorHospitalList.size

    }
    class HospitalViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        @SuppressLint("ResourceAsColor")
        fun bind(hospitalList: HospitalList){
                itemView.hospitalName_textView.text = "Clinic - Hospital - "+hospitalList.DOS_HOSPITAL_NAME
            itemView.meetingTime_textView.text = "Meeting time: "+hospitalList.DMS_START_TIME+" "+hospitalList.DMS_END_TIME
            itemView.address_textView.text = "Address: "+hospitalList.DOS_ADDRESS
            if (hospitalList.DAYS_NAME == "Tue"){
                itemView.tue_textView.setBackgroundColor(R.color.nevy_blue)
            }else if (hospitalList.DAYS_NAME == "Sun"){
                itemView.sunday_textView.setBackgroundColor(R.color.nevy_blue)
            }else if (hospitalList.DAYS_NAME == "Mon"){
                itemView.monday_textView.setBackgroundColor(R.color.nevy_blue)
            }else if (hospitalList.DAYS_NAME == "Wed"){
                itemView.wed_textView.setBackgroundColor(R.color.nevy_blue)
            }else if (hospitalList.DAYS_NAME == "Thr"){
                itemView.thuTextView.setBackgroundColor(R.color.nevy_blue)
            }else if (hospitalList.DAYS_NAME == "Fri"){
                itemView.friTextView.setBackgroundColor(R.color.nevy_blue)
            } else if (hospitalList.DAYS_NAME == "Sat"){
                itemView.satTextView.setBackgroundColor(R.color.nevy_blue)
            }
        }

    }
}