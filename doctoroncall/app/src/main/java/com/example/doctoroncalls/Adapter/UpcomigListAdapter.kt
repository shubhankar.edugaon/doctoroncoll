package com.example.doctoroncalls.Adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.doctoroncalls.Models.UpcomingList
import com.example.doctoroncalls.R
import kotlinx.android.synthetic.main.upcoming_list_item.view.*
import kotlinx.android.synthetic.main.upcoming_list_item.view.patientName
import kotlinx.android.synthetic.main.upcoming_view_dialog.view.*

class UpcomigListAdapter(var upcomingList: List<UpcomingList>,  private val onItemClickListener: OnItemClickListener): RecyclerView.Adapter<UpcomigListAdapter.MeetinhScheduleListHolder>(){
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MeetinhScheduleListHolder{
        return MeetinhScheduleListHolder(LayoutInflater.from(parent.context).inflate(R.layout.upcoming_list_item,parent,false),onItemClickListener)
    }

    override fun onBindViewHolder(holder: MeetinhScheduleListHolder, position: Int) {
        holder.bind(upcomingList[position])
        val data = upcomingList[position]
        holder.upcomingList = data
    }

    override fun getItemCount(): Int {
        return upcomingList.size
    }

    class MeetinhScheduleListHolder(itemView: View, onItemClickListener: OnItemClickListener,): RecyclerView.ViewHolder(itemView){
        lateinit var upcomingList: UpcomingList
        fun bind(upcomingList: UpcomingList){
            itemView.time_values_textView.text = upcomingList.DMS_START_TIME+" - "+upcomingList.DMS_END_TIME
            itemView.date_values_textView.text = upcomingList.APPOINT_DATE
            itemView.patientName.text = upcomingList.PATIENT_NAME
            if (upcomingList.MESSAGE == "Cancelled"){
                itemView.Confirm_button.visibility = View.GONE
                itemView.cancel_button.visibility = View.GONE
            }
        }
        init {
             itemView.view_details.setOnClickListener {
                onItemClickListener.onItemClick(upcomingList)
            }
        }
    }

    interface OnItemClickListener{
        fun onItemClick(upcomingList: UpcomingList)
    }
}