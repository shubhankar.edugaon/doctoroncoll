package com.example.doctoroncalls.Activityes

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.webkit.WebView
import android.webkit.WebViewClient
import com.example.doctoroncalls.Models.UrlUtills
import com.example.doctoroncalls.R
import kotlinx.android.synthetic.main.activity_feed_back.*

class TermsAndCondicationActivity : AppCompatActivity() {
    lateinit var termsAndCondition: WebView
    private var urlUtil: UrlUtills = UrlUtills()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_terms_and_condication)

        arroback.setOnClickListener {
            onBackPressed()
        }
         termsAndCondition = findViewById<WebView>(R.id.webView)
        termsAndCondition.webViewClient = WebViewClient()
        termsAndCondition.loadUrl(urlUtil.termsAndCondition())
        termsAndCondition.settings.javaScriptEnabled = true
        termsAndCondition.settings.setSupportZoom(true)

    }
    override fun onBackPressed() {
        // if your webview can go back it will go back
        if (termsAndCondition.canGoBack())
            termsAndCondition.goBack()
        // if your webview cannot go back
        // it will exit the application
        else
            super.onBackPressed()
    }
}