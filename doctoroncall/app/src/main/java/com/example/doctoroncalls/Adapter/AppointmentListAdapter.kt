package com.example.doctoroncalls.Adapter

import android.content.Context
import android.content.SharedPreferences
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.doctoroncalls.Fragments.AppointmentPendingFragment
import com.example.doctoroncalls.Fragments.ApponitmetCompletedFragment
import com.example.doctoroncalls.Fragments.OrderListFragment
import com.example.doctoroncalls.Models.DoctorList
import com.example.doctoroncalls.Models.SessionManager
import com.example.doctoroncalls.R
import com.squareup.picasso.Picasso
import de.hdodenhof.circleimageview.CircleImageView
import kotlinx.android.synthetic.main.appointment_list_item.view.*

class AppointmentListAdapter(var AppointmentList: List<DoctorList>,  var doctorProfileOnItemClickListener: DoctorProfileOnItemClickListener): RecyclerView.Adapter<AppointmentListAdapter.ApplointmentViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AppointmentListAdapter.ApplointmentViewHolder {
       return ApplointmentViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.appointment_list_item, parent, false),doctorProfileOnItemClickListener)
    }

    override fun onBindViewHolder(holder: AppointmentListAdapter.ApplointmentViewHolder, position: Int) {
            holder.bind(AppointmentList[position])
        val data =AppointmentList[position]
            holder.appointmentList = data
    }

    override fun getItemCount(): Int {
      return AppointmentList.size
    }
    class ApplointmentViewHolder(itemView: View, doctorProfileOnItemClickListener: DoctorProfileOnItemClickListener): RecyclerView.ViewHolder(itemView){
        lateinit var appointmentList: DoctorList
        var prifleStatus:String = ""
        fun bind(doctorList: DoctorList){
                   val patientName = itemView.findViewById<TextView>(R.id.patient_name)
                    val description = itemView.findViewById<TextView>(R.id.patient_resigion)
                    val waiting = itemView.findViewById<TextView>(R.id.waitingForConfirmation)
                    val appoitment_date = itemView.findViewById<TextView>(R.id.appointmentDate)
                    val appointmentTime = itemView.findViewById<TextView>(R.id.appointmentTime)
                    val videoCall_btn = itemView.findViewById<TextView>(R.id.startVideo_textView)
                    val image = itemView.findViewById<CircleImageView>(R.id.patientImage)
                if (doctorList.PATIENT_PHOTO !=null) {
                    Glide.with(itemView.context).load(doctorList.PATIENT_PHOTO).into(image)
                }else{
                    R.drawable.doctor
                }
            try {
                    Picasso.get().load(doctorList.PATIENT_PHOTO).into(image)
                }catch (e:Exception){
                    e.toString()
                }
                    val appointmentDate = itemView.findViewById<TextView>(R.id.appointmentDate)
                            patientName.text = doctorList.PATIENT_NAME
                            appoitment_date.text = doctorList.APPOINT_DATE
                            appointmentTime.text = doctorList.DMS_START_TIME+" - "+doctorList.DMS_END_TIME
                            appointmentDate.text = doctorList.APPOINT_DATE
                            description.text = doctorList.DA_DISEAS
                            waiting.text = doctorList.MESSAGE
                            val message = doctorList.MESSAGE
                        if (message == "Appointment Confirmed"){
                            videoCall_btn.visibility = View.VISIBLE
                            if (doctorList.APPOINT_TYPE == "2"){
                                itemView.visitClinic_textView.visibility = View.VISIBLE
                                videoCall_btn.visibility = View.GONE
                            }else{
                                videoCall_btn.visibility = View.VISIBLE
                                itemView.visitClinic_textView.visibility = View.GONE
                            }
                            if (doctorList.APPOINT_TYPE == "1"){
                                itemView.visitClinic_textView.visibility = View.GONE
                                videoCall_btn.visibility = View.VISIBLE
                            }
                        } else if (message == "Waiting For Appointment"){
                            videoCall_btn.visibility = View.GONE
                        }

        }
        init {
            itemView.setOnClickListener {
                doctorProfileOnItemClickListener.docotrList(appointmentList)
            }
            val videoCallBtn = itemView.findViewById<TextView>(R.id.startVideo_textView)
            videoCallBtn.setOnClickListener {
                doctorProfileOnItemClickListener.startCall(appointmentList)
            }
        }
    }
    interface DoctorProfileOnItemClickListener {
                fun docotrList(doctorList: DoctorList)
                fun startCall(doctorList: DoctorList)
    }
}




