package com.example.doctoroncalls.Models

import android.content.Context
import android.content.Intent
import com.example.doctoroncalls.Activityes.LoginActivity


class SessionManager(val context: Context) {
    var pref = context.getSharedPreferences("shared_prefs" , Context.MODE_PRIVATE)
    var editor = pref.edit()
    var _context: Context? = null
    var PRIVATE_MODE = 0
    private val PREF_NAME = "CTL"
    val IS_USER_LOGIN = "IsUserLoggedIn"
    val KEY_CODE = "code"
    val ID = "id"
    val KEY_ID = "KEY_ID"
    val KEY_MOBILE = "mobile"
    val KEY_NAME = "KEY_NAME"
    val KEY_CONTACT = "KEY_CONTACT"
    val KEY_SECRET = "KEY_SECRET"
    val LOGIN_TOKEN = "KEY_TOKEN"

    //location details
        val KEY_STATE_NAME = "KEY_STATE_NAME"
        val KEY_STATE_ID = "KEY_STATE_ID"
        val KEY_DIST_ID = "KEY_DIST_ID"
        val KEY_DIST_NAME = "KEY_DIST_NAME"

    // mobile verfication
    var VERIFICATION_MOBILE = "KEY_MOBILE"
    var VERIF_ID = "VERIFICATION_ID"

        // prescription details
        val MESSAGE = "MESSAGE"
    val CUSTOMER_FILE = "CUSTOMER_FILE"
        val DA_ID = "DA_ID"
        val DA_TT = "DA_TT  "
        val DOCOR_ID = "DOCOR_ID"
        val PATIENT_ADDRESS = "PATIENT_ADDRESS"
        val PATIENT_NAME = "PATIENT_NAME"
        val PATIENT_PHONE = "PATIENT_PHONE"
        val APPOINTMENT_DATE = "APPOINTMENT_DATE"
    val APPOINT_SLOT = "APPOINT_SLOT"
    val APPOINT_TYPE = "APPOINT_TYPE"
    val PATIENT_BOOKING_ID = "PATIENT_BOOKING_ID"
    val KEY_LOGIN_TOKEN = "KEY_LOGIN_TOKEN"


    // appointmentDetails
    val DA_LOGIN_ID = "DA_LOGIN_ID"
    val DA_STATUS = "DA_STATUS"
    val FIRM_ID = "FIRM_ID"
    val BRANCH_CODE = "BRANCH_CODE"
    val BRANCH_CONTACT = "BRANCH_CONTACT"
    val BRANCH_EMAIL = "BRANCH_EMAIL"
    val BRANCH_TT = "BRANCH_TT"
    val BRANCH_USERNAME = "BRANCH_USERNAME"
    val BRANCH_PASSWORD = "BRANCH_PASSWORD"
    val BRANCH_STATUS = "BRANCH_STATUS"
    val SPONSER_NAME = "SPONSER_NAME"
    val PLAN_ID = "PLAN_ID"
    val BRANCH_VIEW_PASSWORD = "BRANCH_VIEW_PASSWORD"
    val BRANCH_TOKEN = "BRANCH_TOKEN"
    val PROFILE_STATUS = "PROFILE_STATUS"
    val PROFILE_IMAGE = "PROFILE_IMAGE"
    val PATIENT_PHOTO = "PATIENT_PHOTO"
    val SPECIALITY = "SPECIALITY"
    val DL_NO = "DL_NO"
    val LICENCE_NO = "LICENCE_NO"
    val AMBULANCE_NO = "AMBULANCE_NO"
    val LOCATION = "LOCATION"
    val ABOUT_US = "ABOUT_US"
    val BRANCH_STATE_ID = "BRANCH_STATE_ID"
    val BRANCH_DISTRICT_ID = "BRANCH_DISTRICT_ID"
    val DOCTOR_CONSULTATION_FEE = "DOCTOR_CONSULTATION_FEE"
    val AS_IS = "AS_IS"
    val AS_TT = "AS_TT"
    val AS_NAME = "AS_NAME"
    val DMS_ID = "DMS_ID"

    // user name
    var USER_NAME = "USER_NAME"
    var USER_CONTACT = "USER_CONTACT"
    var USER_EMAIL = "USER_EMAIL"
    var USER_GENDER = "USER_GENDER"
    var USER_ADDFRESS = "USER_ADDRESS"
    var REFERAL_CODE = "REFERRAL_CODE"
    //var PROFILE_STATUS = "PROFILE_STATUS"

    // store token
    var AUTH_TOKEN = "AUTH_TOKEN"
    var STATUS = "STATUS"

    fun createMobileVerification(verificationMobile:String, verficationId:String){
        editor?.putString(VERIFICATION_MOBILE,verificationMobile)
        editor?.putString(VERIF_ID, verficationId)
        editor?.commit()
    }
    fun storeToken(token:String){
        editor?.putString(AUTH_TOKEN, token)
        editor?.commit()
    }

    fun storeNotification(token:String){
        editor?.commit()
    }

    fun dmsId(dmsid:String){
        editor?.putString(DMS_ID,dmsid)
    }

    fun userDetails(name:String,contact:String, email:String,gender:String,address:String,profile_status:String, profileImage:String){
        editor?.putString(USER_NAME,name)
        editor?.putString(USER_CONTACT,contact)
        editor?.putString(USER_EMAIL,email)
        editor?.putString(USER_GENDER,gender)
        editor?.putString(USER_ADDFRESS,address)
        editor?.putString(PROFILE_STATUS,profile_status)
        editor?.putString(PROFILE_IMAGE,profileImage)
        editor?.commit()
    }

    fun createRegisterSession(id: String?, key_id: String,contact: String?,login_token:String,key_secret:String, referal_code:String) {
        editor?.putBoolean(IS_USER_LOGIN, true)
        editor?.putString(ID, id)
        editor?.putString(KEY_ID, key_id)
        editor?.putString(KEY_CONTACT, contact)
        editor?.putString(KEY_SECRET, key_secret)
        editor?.putString(LOGIN_TOKEN,login_token)
        editor?.putString(REFERAL_CODE,referal_code)
        editor?.commit()
    }

    fun DoctorStatus(status:String){
        editor.putString(STATUS,status)
        editor?.commit()

    }

    fun locationDetails(stateName:String, stateId:String,distId:String, distName:String){
        editor?.putString(KEY_STATE_NAME,stateName)
        editor?.putString(KEY_STATE_ID, stateId)
        editor?.putString(KEY_DIST_ID, distId)
        editor?.putString(KEY_DIST_NAME, distName)
        editor?.commit()
    }
    fun prescriptionDetails(branchName:String,refered_by:String,message:String,customer_file:String, patientName:String,patientPhone:String) {

    }

    fun logoutUser(main_context : Context){
        _context= main_context
        editor?.clear()?.apply()
       // editor?.commit()
        val intent = Intent(_context,LoginActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
        _context?.startActivity(intent)
    }

    fun appointmentDetails(dd:String,ddtt:String,doctorId:String, patientName:String,patientMobile:String,
                           patientAddress:String,
                           appointmentDate:String,
                           appointmentSlots:String,
                           appointmeType:String,
                           doctorLoginId:String,
                           doctorStatus:String,
                           patientBookingId:String,
                           branchId:String,
                           fromId:String,
                           branchName:String,
                           branchCode:String,
                           branchContact:String,
                           branchEmail:String,
                           branchTT:String,
                           branchUserName:String,
                           branchPass:String,
                           branchStatus:String,
                           sponerName:String,
                           planID:String,
                           branchViewPass:String,
                           branchToke:String,
                           pofileStatus:String,
                           patientPgoto:String,
                           docotoSpecialist:String,
                           doctorExpericen:String,
                           dl_no:String,
                           lincenceNo:String,
                           ambulance_No:String,
                           location:String,
                           about_us:String,
                           branch_id:String,
                           branchDistId:String,
                           doctorFee:String,
                           ss_is:String,
                           as_tt:String,
                           as_name:String) {
        editor?.putString(DOCOR_ID,doctorId)
        editor?.putString(PATIENT_NAME,patientName)
        editor?.putString(PATIENT_PHONE,patientMobile)
        editor?.putString(PATIENT_ADDRESS,patientAddress)
        editor?.putString(APPOINTMENT_DATE,appointmentDate)
        editor?.putString(APPOINT_SLOT,appointmentSlots)
        editor?.putString(APPOINT_TYPE,appointmeType)
        editor?.putString(PATIENT_BOOKING_ID,patientBookingId)

    }

}
