package com.example.doctoroncalls.Fragments

import android.app.AlertDialog
import android.content.Context
import android.net.ConnectivityManager
import androidx.fragment.app.Fragment
import com.example.doctoroncalls.R

open class LoaderFragment: Fragment() {
    private lateinit var dialog: AlertDialog

    fun displayLoaderFragment(){
        val builder = activity.let { AlertDialog.Builder(it) }
        val dialogView = layoutInflater.inflate(R.layout.progress_bar_layout,null)
        builder.setView(dialogView)
        builder.setCancelable(false)
        dialog = builder.create()
        dialog.show()
    }

    fun dismissLoader(){
        dialog.dismiss()
    }
        fun isOnline(): Boolean{
            val conManager = activity?.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val internetManager = conManager.activeNetworkInfo
            return internetManager != null && internetManager.isConnectedOrConnecting
        }

}