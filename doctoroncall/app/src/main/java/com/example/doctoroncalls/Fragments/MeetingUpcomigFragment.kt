package com.example.doctoroncalls.Fragments

import android.annotation.SuppressLint
import android.content.ContentValues
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.CheckBox
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.bumptech.glide.Glide
import com.example.doctoroncalls.Activityes.DashActivity
import com.example.doctoroncalls.Adapter.UpcomigListAdapter
import com.example.doctoroncalls.Models.*
import com.example.doctoroncalls.NotificationModules.NotificationData
import com.example.doctoroncalls.NotificationModules.PushNotification
import com.example.doctoroncalls.NotificationModules.RetrofitInstance
import com.example.doctoroncalls.R
import com.google.firebase.messaging.Constants.MessageTypes.MESSAGE
import com.google.gson.Gson
import kotlinx.android.synthetic.main.doctor_list_item.view.*
import kotlinx.android.synthetic.main.fragment_appointment_pending.*
import kotlinx.android.synthetic.main.fragment_apponitmet_completed.*
import kotlinx.android.synthetic.main.fragment_meeting_upcomig.*
import kotlinx.android.synthetic.main.fragment_transation.*
import kotlinx.android.synthetic.main.upcoming_view_dialog.*
import kotlinx.android.synthetic.main.upcoming_view_dialog.view.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.json.JSONArray


class MeetingUpcomigFragment : LoaderFragment(),UpcomigListAdapter.OnItemClickListener {
    private var sessionManager: SessionManager? = null
    private var urlUtills: UrlUtills? = UrlUtills()
    var sharedpreferences: SharedPreferences? = null
    val SHARED_PREFS = "shared_prefs"
    var AUTH_TOKEN = "AUTH_TOKEN"
    var id = ""
    val ID = "id"
    var loginToken = ""
    var appointmentType = ""
    var mobile = ""
    var status = ""
    var appointmentId = ""
    var token = ""
    var authId = ""
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view =  inflater.inflate(R.layout.fragment_meeting_upcomig, container, false)

        sessionManager = SessionManager(requireActivity().applicationContext)
        sharedpreferences = activity?.getSharedPreferences(SHARED_PREFS, Context.MODE_PRIVATE)
        id = sharedpreferences?.getString(ID,"").toString()
        loginToken = sharedpreferences?.getString(AUTH_TOKEN,"").toString()
        authId = DoctorModels.authId
       // getUpcomingAppointmentList()
        return view
    }


    override fun onResume() {
        super.onResume()
        getUpcomingAppointmentList()
    }

    private fun getUpcomingAppointmentList(){
        var volleyRequestQueue: RequestQueue? = null
        val TAG = "Handy Opinion Tutorials"
        volleyRequestQueue = Volley.newRequestQueue(activity)
        val strReq: StringRequest = @SuppressLint("SetTextI18n")
        object : StringRequest(
            Method.POST, urlUtills?.appointList(), Response.Listener { response ->
                try {
                   // dismissLoader()
                    val jsonArray = JSONArray("[$response]")
                    val messageObj = jsonArray.getJSONObject(0).get("upcoming") as JSONArray
                     var upcomingAppointmentList = ArrayList<UpcomingList>()
                    upcomingAppointmentList.clear()
                    val count = messageObj.length()
                    totalAppointment_textView.text = "Total Appointment: " + count.toString()
                    if (count == 0){
                        meet_data_not_found.visibility = View.VISIBLE
                        meet_data_not_found.text = "Data not found?"
                    }else{
                        meet_data_not_found.visibility = View.GONE
                    }
                    for (i in 0 until messageObj.length()) {
                        val message = messageObj.getJSONObject(i).get("MESSAGE")
                        val appointmentEndTime = messageObj.getJSONObject(i).getString("DMS_END_TIME")
                        var appointmentStartTime = messageObj.getJSONObject(i).getString("DMS_START_TIME")
                        var appointmentDate =  messageObj.getJSONObject(i).getString("APPOINT_DATE")
                        var stateName = messageObj.getJSONObject(i).getString("STATE_NAME")
                        var distName = messageObj.getJSONObject(i).getString("DISTRICT_NAME")
                        var PATIENT_NAME = messageObj.getJSONObject(i).getString("PATIENT_NAME")
                        var PATIENT_MOBILE  = messageObj.getJSONObject(i).getString("PATIENT_MOBILE")
                        var PATIENT_ADDRESS = messageObj.getJSONObject(i).getString("PATIENT_ADDRESS")
                        var APPOINT_TYPE = messageObj.getJSONObject(i).getString("APPOINT_TYPE")
                        var PATIENT_BOOKING_ID = messageObj.getJSONObject(i).getString("PATIENT_BOOKING_ID")
                        var BRANCH_NAME = messageObj.getJSONObject(i).getString("BRANCH_NAME")
                        var DOCTOR_CONSULTATION_FEE = messageObj.getJSONObject(i).getString("DOCTOR_CONSULTATION_FEE")
                        var PATIENT_PHOTO  = messageObj.getJSONObject(i).getString("PATIENT_PHOTO")
                        var DA_MEETING_LINK = messageObj.getJSONObject(i).getString("DA_MEETING_LINK")
                        var DA_LOGIN_ID = messageObj.getJSONObject(i).getString("DA_LOGIN_ID")
                        var DA_ID = messageObj.getJSONObject(i).getString("DA_ID")
                        var BRANCH_TOKEN = messageObj.getJSONObject(i).getString("BRANCH_TOKEN")
                        val upcomingList = UpcomingList(appointmentEndTime,appointmentDate,appointmentStartTime,distName,stateName,PATIENT_NAME,PATIENT_MOBILE,PATIENT_ADDRESS,APPOINT_TYPE,PATIENT_BOOKING_ID, BRANCH_NAME,DOCTOR_CONSULTATION_FEE,"",PATIENT_PHOTO,DA_LOGIN_ID,DA_ID,BRANCH_TOKEN,message.toString())
                        if (message != "Cancelled") {
                        upcomingAppointmentList.add(upcomingList)
                        }
                        showUpcomingAppointmentList(upcomingAppointmentList)
                    }
                } catch (e: Exception) { // caught while parsing the response
                    //dismissLoader()
                    Toast.makeText(activity, "Something went wrong!!!", Toast.LENGTH_SHORT).show()
                    e.printStackTrace()
                }
            },
            Response.ErrorListener { volleyError -> // error occurred
                //dismissLoader()
                Toast.makeText(activity, "Server encountered some problem !!!", Toast.LENGTH_SHORT).show()
            }) {

            override fun getParams(): MutableMap<String, String> {
                val params: MutableMap<String, String> = java.util.HashMap()
                params.put("loginid",id)
                return params
            }

            override fun getHeaders(): Map<String, String> {
                val headers: MutableMap<String, String> = HashMap()
                headers.put("Authorizations", loginToken)
                return headers
            }
        }
        volleyRequestQueue?.add(strReq)
    }

    private fun showUpcomingAppointmentList(showUpcomingAppointmentList: List<UpcomingList>){
        val doctorAdapter = UpcomigListAdapter(showUpcomingAppointmentList,this)
        val layoutManage = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL,false)
        upcomingAppointment_recyclerView.layoutManager = layoutManage
        upcomingAppointment_recyclerView.adapter = doctorAdapter
    }

    override fun onItemClick(upcomingList: UpcomingList) {
        appointmentId = upcomingList.DA_ID.toString()
        val patientName = upcomingList.PATIENT_NAME
        val  patientAddress = upcomingList.PATIENT_ADDRESS
        val  patientMobile = upcomingList.PATIENT_MOBILE
        val appointmentDate = upcomingList.APPOINT_DATE
        val appointmentTime = upcomingList.DMS_START_TIME+" "+upcomingList.DMS_END_TIME
        val appointmentDay = ""
        val doctorName = upcomingList.BRANCH_NAME
        val image = upcomingList.PATIENT_PHOTO
         appointmentType = upcomingList.APPOINT_TYPE.toString()
        val da_login_id = upcomingList.DA_LOGIN_ID
        token = upcomingList.BRANCH_TOKEN.toString()
        viewProfileDialog(patientName.toString(),patientAddress.toString(),patientMobile.toString(),appointmentDate.toString(),appointmentTime,appointmentDay,doctorName.toString(),image.toString(),da_login_id.toString())
    }

    private fun viewProfileDialog(patientN:String,PatientA:String,PatientM:String,appD:String,appTime:String,appDay:String,doctorName:String,image:String,da_login_id:String){
        val mDialogView = LayoutInflater.from(activity).inflate(R.layout.upcoming_view_dialog, null)
        val mBuilder = AlertDialog.Builder(this.requireContext()).setView(mDialogView)
        mDialogView.patientName.text = patientN
        mDialogView.patientAddress.text = PatientA
        mDialogView.patientMobile.text = PatientM
        mDialogView.bookingDate.text = appD
        mDialogView.appointment_time.text = appTime
        mDialogView.appointmentDay.text = appDay
        mDialogView.docctorName.text = doctorName
        if (id == da_login_id){
                mDialogView.Confirm_button.visibility = View.GONE
                mDialogView.cancel_button.visibility = View.GONE
        }
        mDialogView.cancel_button.setOnClickListener {
            status = "2"
            appointmentAcception()
            val title = "Your appointment has Cancel"
            if (title.isNotEmpty()){
                if (title.isNotEmpty()) {
                    PushNotification(
                        NotificationData(title,""),
                        token
                    ).also {
                        sendNotificationToPatient(it)
                    }
                }
            }
        }
        mDialogView.Confirm_button.setOnClickListener {
            status = "1"
            appointmentAcception()
            val title = "Your appointment is confirm"
            if (title.isNotEmpty()){
                PushNotification(NotificationData(title,""),
                token).also {
                    sendNotificationToPatient(it)
                }
            }
        }
        if (appointmentType == "1"){
            mDialogView.appointmentType_name.text = "online Appointment"
        } else if (appointmentType ==  "2"){
            mDialogView.appointmentType_name.text = "offline Appointment"
        }
        //Glide.with(this.requireActivity()).load(image).into(mDialogView.patientPhoto)
        val  mAlertDialog = mBuilder.show()
        mAlertDialog.setCanceledOnTouchOutside(false)
            mDialogView.ok_button.setOnClickListener {
                mAlertDialog.dismiss()
            }
        mBuilder.setCancelable(false)
    }

    private fun sendNotificationToPatient(notification: PushNotification) = CoroutineScope(
        Dispatchers.IO).launch {
        try {
            val response = RetrofitInstance.api.postNotification(notification)
            if (response.isSuccessful){
                Log.d(ContentValues.TAG,"Response: ${Gson().toJson(response)}")
                val intent = Intent(activity,DashActivity::class.java)
                startActivity(intent)
            } else{
                Log.e(ContentValues.TAG, response.errorBody().toString())
            }
        } catch (e: Exception){
            Log.e(ContentValues.TAG, e.toString())
        }
    }

    private fun appointmentAcception(){
        var volleyRequestQueue: RequestQueue? = null
        val TAG = "Handy Opinion Tutorials"

        volleyRequestQueue = Volley.newRequestQueue(activity)
        val strReq: StringRequest = object : StringRequest(Method.POST,urlUtills?.updateAppoint(), Response.Listener { response ->
            Log.e(TAG, "response: " + response)
            try {
                val jsonArray = JSONArray("[$response]")
                val jsonObject = jsonArray.getJSONObject(0)
                val message = jsonObject.getString("message")
                val contact = jsonObject.getString("CONTACT")
                mobile = contact
                if (mobile !=null){
                    val title = message.toString()
                    val description = "Congratls your appointment accepted"
                    if (title.isNotEmpty() && description.isNotEmpty()){
                    }
                }
                else if (message == "Appointment Confirmed"){
                    val intent = Intent(this.context, DashActivity::class.java)
                    startActivity(intent)
                } else if (message == "Cancelled"){
                    Toast.makeText(this.requireContext(),"Your appointment cancel",Toast.LENGTH_LONG).show()
                    val intent = Intent(this.context, DashActivity::class.java)
                    startActivity(intent)
                }
            } catch (e: Exception) { // caught while parsing the response
                Toast.makeText(this.requireContext(), "Something went wrong!!!", Toast.LENGTH_SHORT).show()
                e.printStackTrace()
            }
        },
            Response.ErrorListener { volleyError -> // error occurred
                Toast.makeText(this.requireContext(), "Server encountered some problem !!!", Toast.LENGTH_SHORT).show()
            }) {

            override fun getParams(): MutableMap<String, String> {
                val params: MutableMap<String, String> = java.util.HashMap()
                params.put("status",status)
                params.put("appointment_id",appointmentId)
                params.put("loginid", id)
                params.put("id", authId)
                params.put("MEETING_LINKS","")
                return params
            }

            override fun getHeaders(): Map<String, String> {
                val headers: MutableMap<String, String> = HashMap()
                headers.put("Authorizations", loginToken)
                return headers
            }
        }
        volleyRequestQueue?.add(strReq)
    }

}