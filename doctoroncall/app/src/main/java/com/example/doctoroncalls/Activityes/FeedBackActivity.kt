package com.example.doctoroncalls.Activityes

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.example.doctoroncalls.Models.SessionManager
import com.example.doctoroncalls.Models.UrlUtills
import com.example.doctoroncalls.R
import kotlinx.android.synthetic.main.activity_feed_back.*
import org.json.JSONArray

class FeedBackActivity : AppCompatActivity() {
    var title  = ""
    var description =  ""
    val ID = "id"
    var loginId = ""
    val KEY_LOGIN_TOKEN = "KEY_LOGIN_TOKEN"
    private val urlUtills: UrlUtills? = UrlUtills()
    var sharedpreferences: SharedPreferences? = null
    val SHARED_PREFS = "shared_prefs"
    var loginToken = ""
    private var sessionManager: SessionManager? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_feed_back)

        arroback.setOnClickListener {
            onBackPressed()
        }

        sessionManager = SessionManager(this)
        sharedpreferences = getSharedPreferences(SHARED_PREFS, Context.MODE_PRIVATE)
        loginId = sharedpreferences?.getString(ID,"").toString()
        loginToken = sharedpreferences?.getString(KEY_LOGIN_TOKEN,"").toString()


        feedBackSubmit.setOnClickListener {
            title = feedBack_title.text.toString().trim()
            description = faq_description_editText.text.toString().trim()

            if (title == "") {
                AlertDialog.Builder(this)
                    .setMessage("Please enter title")
                    .show()
            } else if (description == "") {
                AlertDialog.Builder(this)
                    .setMessage("Please enter description")
            } else {
                addFeedBack()
            }
        }
    }

    private fun addFeedBack(){
        var volleyRequestQueue: RequestQueue? = null
        volleyRequestQueue = Volley.newRequestQueue(this)
        val strReq: StringRequest = object : StringRequest(Method.POST, urlUtills?.giveFeedBack(), Response.Listener { response ->
            try {
                val jsonArray = JSONArray("[$response]")
                val messageObj = jsonArray.getJSONObject(0).get("message")
                if (messageObj == "ok"){
                    val intent =  Intent(this,DashActivity::class.java)
                    startActivity(intent)
                }
            } catch (e: Exception) { // caught while parsing the response
                Toast.makeText(this, "Something went wrong!!!", Toast.LENGTH_SHORT).show()
                e.printStackTrace()
            }
        },
            Response.ErrorListener { volleyError -> // error occurred
                //
                Toast.makeText(applicationContext, "Server encountered some problem !!!", Toast.LENGTH_SHORT).show()
            }) {

            override fun getParams(): MutableMap<String, String> {
                val params: MutableMap<String, String> = java.util.HashMap()
                params.put("participantid",loginId)
                params.put("subject", title)
                params.put("message", description)
                return params
            }
            override fun getHeaders(): Map<String, String> {
                val headers: MutableMap<String, String> = HashMap()
                params["Authorizations"] = loginToken
                return headers
            }
        }
        // Adding request to request queue
        volleyRequestQueue?.add(strReq)
    }

}
