package com.example.doctoroncalls.Fragments

import android.app.AlertDialog
import android.content.*
import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.example.doctoroncalls.Activityes.AppointmentViewDetailsActivity
import com.example.doctoroncalls.Activityes.DashActivity
import com.example.doctoroncalls.Adapter.AppointmentListAdapter
import com.example.doctoroncalls.Models.Doctor
import com.example.doctoroncalls.Models.DoctorList
import com.example.doctoroncalls.Models.SessionManager
import com.example.doctoroncalls.Models.UrlUtills
import com.example.doctoroncalls.NotificationModules.NotificationData
import com.example.doctoroncalls.NotificationModules.PushNotification
import com.example.doctoroncalls.NotificationModules.RetrofitInstance
import com.example.doctoroncalls.R
import com.google.gson.Gson
import kotlinx.android.synthetic.main.fragment_appointment_pending.*
import kotlinx.android.synthetic.main.fragment_meeting_upcomig.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.jitsi.meet.sdk.JitsiMeetActivity
import org.jitsi.meet.sdk.JitsiMeetConferenceOptions
import org.json.JSONArray

class AppointmentPendingFragment : LoaderFragment(),AppointmentListAdapter.DoctorProfileOnItemClickListener {
    private var sessionManager: SessionManager? = null
    private var urlUtills: UrlUtills? = UrlUtills()
    var sharedpreferences: SharedPreferences? = null
    private var doctorList = ArrayList<Doctor>()
    val SHARED_PREFS = "shared_prefs"
    var id = ""
    val ID = "id"
    var token = ""
    var patientName = ""
    var appointmentDate = ""
    var AUTH_TOKEN = "AUTH_TOKEN"
    var loginToken = ""
    var appointmentType = ""
    var authId = ""
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view =  inflater.inflate(R.layout.fragment_appointment_pending, container, false)
        sessionManager = SessionManager(requireActivity().applicationContext)
        sharedpreferences = activity?.getSharedPreferences(SHARED_PREFS, Context.MODE_PRIVATE)
        id = sharedpreferences?.getString(ID,"").toString()
        loginToken = sharedpreferences?.getString(AUTH_TOKEN,"").toString()
        authId = sharedpreferences?.getString("phoneAuth","").toString()

//        if (isOnline()) {
//            getAppointmetList()
//        }else{
//            showAlert()
//        }

        return view
    }

    override fun onResume() {
        super.onResume()
        getAppointmetList()
    }

    private fun getAppointmetList(){
        var volleyRequestQueue: RequestQueue? = null
        val TAG = "Handy Opinion Tutorials"

        volleyRequestQueue = Volley.newRequestQueue(activity)
        val strReq: StringRequest = object : StringRequest(
            Method.POST, urlUtills?.appointList(), Response.Listener { response ->
            Log.e(TAG, "response: " + response)
//                dismissLoader()
            try {
               // dismissLoader()
                val jsonArray = JSONArray("[$response]")
                var order = jsonArray.getJSONObject(0).getString("order")
                Log.e(TAG,"order: " + order)
                if (order == "Sorry No Data Found"){
                    Toast.makeText(activity,"Sorry No Data Found", Toast.LENGTH_LONG).show()
                }else {
                    val messageObj = jsonArray.getJSONObject(0).get("order") as JSONArray
                    var appointmentLiat = ArrayList<DoctorList>()
                    appointmentLiat.clear()
                    val count = messageObj.length()
                    if (count == 0) {
                        pending_data_not_found.visibility = View.VISIBLE
                    } else {
                        pending_data_not_found.visibility = View.GONE
                    }
                    for (i in 0 until messageObj.length()) {
                        patientName = messageObj.getJSONObject(i).getString("PATIENT_NAME")
                        val patientAddress =
                            messageObj.getJSONObject(i).getString("PATIENT_ADDRESS")
                        val description = messageObj.getJSONObject(i).get("DA_DISEAS")
                        val appointmentDate = messageObj.getJSONObject(i).get("APPOINT_DATE")
                        val da_id = messageObj.getJSONObject(i).get("DA_ID")
                        val message = messageObj.getJSONObject(i).get("MESSAGE")
                        token = messageObj.getJSONObject(i).getString("BRANCH_TOKEN")
                        val da_login_id = messageObj.getJSONObject(i).get("DA_LOGIN_ID")
                        val appointmentStartTime = messageObj.getJSONObject(i).getString("DMS_START_TIME")
                        val appointmentEndTime = messageObj.getJSONObject(i).getString("DMS_END_TIME")
                        val photo = messageObj.getJSONObject(i).getString("PATIENT_PHOTO")
                        val mobile = messageObj.getJSONObject(i).getString("PATIENT_MOBILE")
                        val branchName = messageObj.getJSONObject(i).getString("BRANCH_NAME")
                        val DA_MEETING_LINK =
                            messageObj.getJSONObject(i).getString("DA_MEETING_LINK")
                        val APPOINT_TYPE = messageObj.getJSONObject(i).getString("APPOINT_TYPE")
                        val appoinmte = DoctorList(
                            patientName,
                            branchName,
                            patientAddress,
                            description.toString(),
                            appointmentDate.toString(),
                            message.toString(),
                            da_id.toString(),
                            token,
                            da_login_id.toString(),
                            appointmentStartTime,
                            appointmentEndTime,
                            photo,
                            mobile,
                            DA_MEETING_LINK,
                            APPOINT_TYPE
                        )
                        if (message != "Cancelled" && message != "Completed") {
                            appointmentLiat.add(appoinmte)
                        }
                        if (appointmentLiat != null) {
                            showAppointmentList(appointmentLiat)
                        }
                        val count = appointmentLiat.size
                        totalPending_textView.text = "Total Appointment: " + count.toString()
                    }
                }
            } catch (e: Exception) { // caught while parsing the response
                //dismissLoader()
                Toast.makeText(activity, "Something went wrong!!!", Toast.LENGTH_SHORT).show()
                e.printStackTrace()
            }
        },
            Response.ErrorListener { volleyError -> // error occurred
                //dismissLoader()
                Toast.makeText(activity, "Server encountered some problem !!!", Toast.LENGTH_SHORT).show()
            }) {
            override fun getParams(): MutableMap<String, String> {
                val params: MutableMap<String, String> = java.util.HashMap()
                params.put("loginid",id)
                return params
            }

            override fun getHeaders(): Map<String, String> {
                val headers: MutableMap<String, String> = HashMap()
                headers.put("Authorizations", loginToken)
                return headers
            }
        }
        // Adding request to request queue
        volleyRequestQueue?.add(strReq)
        //displayLoaderFragment()
    }

    private fun showAppointmentList(showAppointmentList: List<DoctorList>){
        val doctorAdapter = AppointmentListAdapter(showAppointmentList,this)
        val layoutManage = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL,false)
        doctorAppointmentList.layoutManager = layoutManage
        doctorAppointmentList.adapter = doctorAdapter
    }
    private fun showAlert() {
        val alertDialog: AlertDialog.Builder = AlertDialog.Builder(activity)
        alertDialog.setTitle("Check Connection!")
        alertDialog.setIcon(R.drawable.ic_baseline_warning)
        alertDialog.setPositiveButton("Try Again") { _, _ ->
            if (isOnline()) {
//                displayLoaderFragment()
                getAppointmetList()
                Toast.makeText(activity, "You are Online!", Toast.LENGTH_SHORT).show()
            } else {
                showAlert()
                Toast.makeText(activity, "Please Go Online!", Toast.LENGTH_SHORT).show()
            }
        }
        alertDialog.setNegativeButton("Cancel") { dialog, id ->
            dialog.cancel()
        }

        val alert = alertDialog.create()
        alert.setCanceledOnTouchOutside(false)
        alert.show()
        val negativeButton = alert.getButton(DialogInterface.BUTTON_NEGATIVE)
        negativeButton.setTextColor(Color.RED)

        val positiveButton = alert.getButton(DialogInterface.BUTTON_POSITIVE)
        positiveButton.setTextColor(Color.GREEN)
    }

    override fun docotrList(doctorList: DoctorList) {
        val  intent = Intent(activity,AppointmentViewDetailsActivity::class.java)
        intent.putExtra("appointmentId",doctorList.DA_ID)
        intent.putExtra("token", doctorList.BRANCH_TOKEN)
        intent.putExtra("appointmentDate", doctorList.APPOINT_DATE)
        intent.putExtra("patientName", doctorList.PATIENT_NAME)
        intent.putExtra("doctor_name",doctorList.BRANCH_NAME)
        intent.putExtra("da_login_id",doctorList.DA_LOGIN_ID)
        intent.putExtra("DMS_START_TIME",doctorList.DMS_START_TIME)
        intent.putExtra("DMS_END_TIME", doctorList.DMS_END_TIME)
        intent.putExtra("imageUrl",  doctorList.PATIENT_PHOTO)
        intent.putExtra("message",doctorList.MESSAGE)
        intent.putExtra("appointmentType",doctorList.APPOINT_TYPE)
        intent.putExtra("status", "")
        startActivity(intent)
    }

    override fun startCall(doctorList: DoctorList) {
       val meetingLInks = doctorList.DA_MEETING_LINK
        val token = doctorList.BRANCH_TOKEN
        appointmentType = doctorList.DA_ID.toString()
        val status = "3"
        appointmentAcception(status)
        if (id ==  doctorList.DA_LOGIN_ID){
            joinVideoCall(meetingLInks.toString())
        }else{
            sendNotification(token.toString())
            joinVideoCall(meetingLInks.toString())
        }
    }
    private fun joinVideoCall(branchName: String) {
        (patientName.length > 0)
        val options = JitsiMeetConferenceOptions.Builder()
            .setRoom(branchName)
            .build()
        JitsiMeetActivity.launch(activity, options)
    }
    private fun sendNotification(token:String){
    val title = "Please join immediately to  doctor"
    val messageT = "?"
    if (title.isNotEmpty() && messageT.isNotEmpty()) {
        PushNotification(
            NotificationData(title, messageT),
            token
        ).also {
            sendNotificationToPatient(it)
        }
    }
}
    private fun sendNotificationToPatient(notification: PushNotification) = CoroutineScope(
        Dispatchers.IO).launch {
        try {
            val response = RetrofitInstance.api.postNotification(notification)
            if (response.isSuccessful){
                Log.d(ContentValues.TAG,"Response: ${Gson().toJson(response)}")
            } else{
                Log.e(ContentValues.TAG, response.errorBody().toString())
            }
        } catch (e: Exception){
            Log.e(ContentValues.TAG, e.toString())
        }
    }

    private fun appointmentAcception(status:String){
        var volleyRequestQueue: RequestQueue? = null
        val TAG = "Handy Opinion Tutorials"

        volleyRequestQueue = Volley.newRequestQueue(activity)
        val strReq: StringRequest = object : StringRequest(Method.POST,urlUtills?.updateAppoint(), Response.Listener { response ->
            Log.e(TAG, "response: " + response)
            try {
                val jsonArray = JSONArray("[$response]")
                val jsonObject = jsonArray.getJSONObject(0)
                val message = jsonObject.getString("message")
                val contact = jsonObject.getString("CONTACT")
                if (contact !=null){
                    val title = message.toString()
                    val description = "Congratls your appointment accepted"
                    if (title.isNotEmpty() && description.isNotEmpty()){
                    }
                }
                else if (message == "Appointment Confirmed"){
                    val intent = Intent(activity, DashActivity::class.java)
                    startActivity(intent)
                } else if (message == "Cancelled"){
                    Toast.makeText(activity,"Your appointment cancel",Toast.LENGTH_LONG).show()
                    val intent = Intent(activity, DashActivity::class.java)
                    startActivity(intent)
                }
            } catch (e: Exception) { // caught while parsing the response
                Toast.makeText(activity, "Something went wrong!!!", Toast.LENGTH_SHORT).show()
                e.printStackTrace()
            }
        },
            Response.ErrorListener { volleyError -> // error occurred
                Toast.makeText(activity, "Server encountered some problem !!!", Toast.LENGTH_SHORT).show()
            }) {

            override fun getParams(): MutableMap<String, String> {
                val params: MutableMap<String, String> = java.util.HashMap()
                params.put("status",status)
                params.put("appointment_id",appointmentType)
                params.put("loginid", id)
                params.put("id", "dmkgm")
                params.put("MEETING_LINKS","https://meet.jit.si/"+patientName)
                return params
            }

            override fun getHeaders(): Map<String, String> {
                val headers: MutableMap<String, String> = HashMap()
                headers.put("Authorizations", loginToken)
                return headers
            }
        }
        volleyRequestQueue?.add(strReq)
    }
}