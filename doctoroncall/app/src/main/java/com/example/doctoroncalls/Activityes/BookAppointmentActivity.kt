package com.example.doctoroncalls.Activityes

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.PagerSnapHelper
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.example.doctoroncalls.Adapter.AvailableSlotsAdapter
import com.example.doctoroncalls.Adapter.HorizontalRecyclerCalendarAdapter
import com.example.doctoroncalls.Models.*
import com.example.doctoroncalls.R
import kotlinx.android.synthetic.main.activity_book_appointment.*
import kotlinx.android.synthetic.main.activity_book_appointment.patientName
import kotlinx.android.synthetic.main.upcoming_view_dialog.*
import org.json.JSONArray
import java.util.*
import kotlin.collections.ArrayList

class BookAppointmentActivity : LoaderActivity(), AvailableSlotsAdapter.OnItemClickListener {

    private var slotsList = ArrayList<Slots>()
    private var sessionManager: SessionManager? = null
    var sharedpreferences: SharedPreferences? = null
    val SHARED_PREFS = "shared_prefs"
    var AUTH_TOKEN = "AUTH_TOKEN"
    val DMS_ID = "DMS_ID"
    var authToken = ""
    var appointmentType = ""
    val ID = "id"
    var name = ""
    var doctorId = ""
    var loginId = ""
    var mobile = ""
    var address = ""
    var branchId = ""
    var selectedDate = ""
    var slotsStartTime = ""
    var slotsEndTime = ""
    var desc = ""
    var BRANCH_TOKEN = ""
    var loginToken = ""
    var distid = ""
    var appointmentOnlineRupya = ""
    var offlinneConsult = ""
    var appointmentTypeStatus = ""
    var dams_id = ""
    private val urlUtills: UrlUtills = UrlUtills()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_book_appointment)
        //walletsDialog()
        appointmentType = intent.getStringExtra("appointmentType").toString()
        doctorId = intent.getStringExtra("doctorId").toString()
        BRANCH_TOKEN = intent.getStringExtra("BRANCH_TOKEN").toString()
        val onlineConsult  = intent.getStringExtra("fee").toString()
        offline_title.text = onlineConsult

        sessionManager = SessionManager(this@BookAppointmentActivity)
        sharedpreferences = getSharedPreferences(SHARED_PREFS, Context.MODE_PRIVATE)
        loginId = sharedpreferences?.getString(ID,"").toString()
        authToken = sharedpreferences?.getString(AUTH_TOKEN,"").toString()
        distid = intent.getStringExtra("distId").toString()


        val date = Date()
        date.time = System.currentTimeMillis()

        val startCal = Calendar.getInstance()

        val endCal = Calendar.getInstance()
        endCal.time = date
        endCal.add(Calendar.MONTH, 3)

        val configuration: RecyclerCalendarConfiguration =
            RecyclerCalendarConfiguration(
                calenderViewType = RecyclerCalendarConfiguration.CalenderViewType.HORIZONTAL,
                calendarLocale = Locale.getDefault(),
                includeMonthHeader = true
            )
        configuration.weekStartOffset = RecyclerCalendarConfiguration.START_DAY_OF_WEEK.MONDAY

       val st  = CalendarUtils.dateStringFromFormat(locale = configuration.calendarLocale, date = date, format = CalendarUtils.LONG_DATE_FORMAT) ?: ""
        currentMonths_title.text = st


        val calendarAdapterHorizontal: HorizontalRecyclerCalendarAdapter =
            HorizontalRecyclerCalendarAdapter(startDate = startCal.time, endDate = endCal.time, configuration = configuration, selectedDate = date,
                dateSelectListener = object : HorizontalRecyclerCalendarAdapter.OnDateSelected {
                    override fun onDateSelected(date: Date) {
                        selectedDate = CalendarUtils.dateStringFromFormat(locale = configuration.calendarLocale, date = date, format = CalendarUtils.LONG_DATE_FORMAT)
                                ?: ""
                        currentMonths_title.text = selectedDate
                        if (selectedDate != null){
                            getAvailableTime()
                        }
                    }

                }
            )

        calendarRecyclerView.adapter = calendarAdapterHorizontal

        val snapHelper = PagerSnapHelper() // Or LinearSnapHelper
        snapHelper.attachToRecyclerView(calendarRecyclerView)

        val branchName = intent.getStringExtra("BRANCH_NAME")
        val consultationFees = intent.getStringExtra("consultationFees")
        branchId = intent.getStringExtra("branchId").toString()

        // branchId = doctorModels?.doctor?.BRANCH_ID.toString().trim()
        arrowBack.setOnClickListener {
            onBackPressed()
        }

        doctorName.text = branchName
        //consulatinFee.text = "₹"+consultationFees+"/-"

        appointment_name.setOnClickListener {
            mobile = mobileNumber.text.toString().trim()
            name = patientName.text.toString().trim()
            address = appointment_address.text.toString().trim()
            desc = userDescription.text.toString().trim()
            if (mobile == "") {
                AlertDialog.Builder(this)
                    .setMessage("Please enter your mobile")
                    .show()
            } else if (name == "") {
                AlertDialog.Builder(this)
                    .setMessage("Please enter your name")
                    .show()
            } else if (address == "") {
                AlertDialog.Builder(this)
                    .setMessage("Please enter your Address")
            } else if (desc == ""){
                AlertDialog.Builder(this)
                    .setMessage("Please Enter your description")
                    .show()
            } else {
                bookAppointment()
            }
        }
    }
    override fun onSlotsClick(slots: Slots, itemView: View) {
        slotsStartTime = slots.DMS_START_TIME.toString()
        slotsEndTime = slots.DMS_END_TIME.toString()
        dams_id = slots.DMS_ID.toString()
        Toast.makeText(this,"Slots selected",Toast.LENGTH_LONG).show()

    }

    private fun bookAppointment(){
            var volleyRequestQueue: RequestQueue? = null
            val TAG = "Handy Opinion Tutorials"

            volleyRequestQueue = Volley.newRequestQueue(this)
            val strReq: StringRequest = object : StringRequest(Method.POST, urlUtills?.bookAppointment(), Response.Listener { response ->
                Log.e(TAG, "response: " + response)
                try {
                    dismissLoaderActivity()
                    val jsonArray = JSONArray("[$response]")
                    val messageObj = jsonArray.getJSONObject(0).get("appoint_detail") as JSONArray
                    val message = jsonArray.getJSONObject(0).get("message")
                       //val f = jsonArray.getJSONObject(0).get("[$messageObj]") as JSONArray
                        val dd = messageObj.getJSONObject(0).get("DA_ID")
                        val ddtt = messageObj.getJSONObject(0).get("DA_TT")
                        val doctorId = messageObj.getJSONObject(0).get("DOCOR_ID")
                        val patientName = messageObj.getJSONObject(0).get("PATIENT_NAME")
                        val patientMobile = messageObj.getJSONObject(0).get("PATIENT_MOBILE")
                        val patientAddress = messageObj.getJSONObject(0).get("PATIENT_ADDRESS")
                        val appointmentDate = messageObj.getJSONObject(0).get("APPOINT_DATE")
                        val appointmentSlots = messageObj.getJSONObject(0).get("APPOINT_SLOT")
                        val appointmeType = messageObj.getJSONObject(0).get("APPOINT_TYPE")
                        val doctorLoginId = messageObj.getJSONObject(0).get("DA_LOGIN_ID")
                        val doctorStatus = messageObj.getJSONObject(0).get("DA_STATUS")
                        val patientBookingId = messageObj.getJSONObject(0).get("PATIENT_BOOKING_ID")
                        val branchId = messageObj.getJSONObject(0).get("BRANCH_ID")
                        val fromId = messageObj.getJSONObject(0).get("FIRM_ID")
                     val branchName = messageObj.getJSONObject(0).get("BRANCH_NAME")
                    val branchCode = messageObj.getJSONObject(0).get("BRANCH_CODE")
                    val branchContact = messageObj.getJSONObject(0).get("BRANCH_CONTACT")
                    val branchEmail = messageObj.getJSONObject(0).get("BRANCH_EMAIL")
                    val branchTT = messageObj.getJSONObject(0).get("BRANCH_TT")
                    val branchUserName = messageObj.getJSONObject(0).get("BRANCH_USERNAME")
                    val branchPass = messageObj.getJSONObject(0).get("BRANCH_PASSWORD")
                    val branchStatus = messageObj.getJSONObject(0).get("BRANCH_STATUS")
                    val sponerName = messageObj.getJSONObject(0).get("SPONSER_NAME")
                    val planID = messageObj.getJSONObject(0).get("PLAN_ID")
                    val branchViewPass = messageObj.getJSONObject(0).get("BRANCH_VIEW_PASSWORD")
                    val branchToke = messageObj.getJSONObject(0).get("BRANCH_TOKEN")
                    val pofileStatus = messageObj.getJSONObject(0).get("PROFILE_STATUS")
                    val patientPgoto = messageObj.getJSONObject(0).get("PATIENT_PHOTO")
                    val docotoSpecialist = messageObj.getJSONObject(0).get("SPECIALITY")
                    val doctorExpericen = messageObj.getJSONObject(0).get("EXPERIENCE")
                    val dl_no = messageObj.getJSONObject(0).get("DL_NO")
                    val lincenceNo = messageObj.getJSONObject(0).get("LICENCE_NO")
                    val ambulance_No = messageObj.getJSONObject(0).get("AMBULANCE_NO")
                    val location = messageObj.getJSONObject(0).get("LOCATION")
                    val about_us = messageObj.getJSONObject(0).get("ABOUT_US")
                    val branch_id = messageObj.getJSONObject(0).get("BRANCH_STATE_ID")
                    val branchDistId = messageObj.getJSONObject(0).get("BRANCH_DISTRICT_ID")
                    val doctorFee = messageObj.getJSONObject(0).get("DOCTOR_CONSULTATION_FEE")
                   val ss_is = messageObj.getJSONObject(0).get("AS_IS")
                    val as_tt = messageObj.getJSONObject(0).get("AS_TT")
                    val as_name = messageObj.getJSONObject(0).get("AS_NAME")
                    sessionManager?.appointmentDetails(dd.toString(),ddtt.toString(),doctorId.toString(),patientName.toString(),patientMobile.toString(),patientAddress.toString(),appointmentDate.toString(),
                        appointmentSlots.toString(),
                        appointmeType.toString(),
                        doctorLoginId.toString(),
                        doctorStatus.toString(),
                        patientBookingId.toString(),
                        branchId.toString(),
                        fromId.toString(),
                        branchName.toString(),
                        branchCode.toString(),
                        branchContact.toString(),
                        branchEmail.toString(),
                        branchTT.toString(),
                        branchUserName.toString(),
                        branchPass.toString(),
                        branchStatus.toString(),
                        sponerName.toString(),
                        planID.toString(),
                        branchViewPass.toString(),
                        branchToke.toString(),
                        pofileStatus.toString(),
                        patientPgoto.toString(),
                        docotoSpecialist.toString(),
                        doctorExpericen.toString(),
                        dl_no.toString(),
                        lincenceNo.toString(),
                        ambulance_No.toString(),
                        location.toString(),
                        about_us.toString(),
                        branch_id.toString(),
                        branchDistId.toString(),
                        doctorFee.toString(),
                        ss_is.toString(),
                        as_tt.toString(),
                        as_name.toString())
                    if(message == "sucess"){
                        Toast.makeText(this,"Appointment successfully",Toast.LENGTH_LONG).show()
                       val intent = Intent(this,AppointmentConfirmActivity::class.java)
                            intent.putExtra("PATIENT_NAME",patientName.toString())
                            intent.putExtra("PATIENT_MOBILE",patientMobile.toString())
                            intent.putExtra("APPOINT_DATE", appointmentDate.toString())
                            intent.putExtra("PATIENT_BOOKING_ID", patientBookingId.toString())
                            intent.putExtra("BRANCH_NAME", branchName.toString())
                            intent.putExtra("SPECIALITY",docotoSpecialist.toString())
                            intent.putExtra("PROFILE_STATUS",pofileStatus.toString())
                            intent.putExtra("SPECIALITY",docotoSpecialist.toString())
                            intent.putExtra("district_id", distid)
                            intent.putExtra("doctorToken", branchToke.toString())
                            intent.putExtra("BRANCH_TOKEN", BRANCH_TOKEN)
                        startActivity(intent)
                    }

                } catch (e: Exception) { // caught while parsing the response
                    Toast.makeText(this, "Something went wrong!!!", Toast.LENGTH_SHORT).show()
                    e.printStackTrace()
                }
            },
                Response.ErrorListener { volleyError -> // error occurred
                    //
                    Toast.makeText(applicationContext, "Server encountered some problem !!!", Toast.LENGTH_SHORT).show()
                }) {

                override fun getParams(): MutableMap<String, String> {
                    val params: MutableMap<String, String> = java.util.HashMap()
                            params.put("patient_name", name)
                            params.put("patient_mobile", mobile)
                            params.put("patient_address", address)
                            params.put("apointment_date", selectedDate)
                            params.put("apointment_slot", slotsStartTime+slotsEndTime)
                            params.put("appointment_type",appointmentType)
                            params.put("doctorid", doctorId)
                            params.put("loginid", loginId)
                            params.put("district_id", distid)
                            params.put("desc",desc)
                            params.put("dms_id",dams_id)
                    return params
                }

                override fun getHeaders(): Map<String, String> {

                    val headers: MutableMap<String, String> = HashMap()
                    // Add your Header paramters here
                    headers.put("Authorizations", authToken)
                    return headers
                }
            }
            // Adding request to request queue
            volleyRequestQueue?.add(strReq)
        showLoaderActivity()
        }

    private fun getAvailableTime(){
        var volleyRequestQueue: RequestQueue? = null
        val TAG = "Handy Opinion Tutorials"

        volleyRequestQueue = Volley.newRequestQueue(this)
        val strReq: StringRequest = object : StringRequest(Method.POST, urlUtills?.availableAppointmentTime(), Response.Listener { response ->
            Log.e(TAG, "response: " + response)
            try {
//                dismissLoaderActivity()
                val jsonArray = JSONArray("[$response]")
                val message = jsonArray.getJSONObject(0).get("message")
                if(message == "Available"){
                    view_slots_layout.visibility = View.VISIBLE
                    message_title.visibility  = View.GONE
                    getSlots()
                } else if (message == "Not Available"){
                    message_title.visibility = View.VISIBLE
                    view_slots_layout.visibility = View.GONE
                }
            } catch (e: Exception) { // caught while parsing the response
                Toast.makeText(this, "Something went wrong!!!", Toast.LENGTH_SHORT).show()
                e.printStackTrace()
            }
        },
            Response.ErrorListener { volleyError -> // error occurred
                //
                Toast.makeText(applicationContext, "Server encountered some problem !!!", Toast.LENGTH_SHORT).show()
            }) {

            override fun getParams(): MutableMap<String, String> {
                val params: MutableMap<String, String> = java.util.HashMap()
                params.put("appointment_date",selectedDate)
                params.put("doctorid", doctorId)
                params.put("online_offline_status", appointmentType)
                params.put("district_id",  distid)
                return params
            }

            override fun getHeaders(): Map<String, String> {
                val headers: MutableMap<String, String> = HashMap()
                headers.put("Authorizations", authToken)
                return headers
            }
        }
        volleyRequestQueue?.add(strReq)
//        showLoaderActivity()

    }

    private fun getSlots(){
        var volleyRequestQueue: RequestQueue? = null
        val TAG = "Handy Opinion Tutorials"

        volleyRequestQueue = Volley.newRequestQueue(this)
        val strReq: StringRequest = object : StringRequest(Method.POST, urlUtills?.availableSlots(), Response.Listener { response ->
            Log.e(TAG, "response: " + response)
            try {
                val jsonArray = JSONArray("[$response]")
                val message = jsonArray.getJSONObject(0).get("appointment_complete")
                if (message == "0") {
                    Toast.makeText(this, "Slots not  Available", Toast.LENGTH_LONG).show()
                }
          val messageObj = jsonArray.getJSONObject(0).get("total_slots") as JSONArray
                for (i in 0 until messageObj.length()){
                    val DMS_START_TIME = messageObj.getJSONObject(i).getString("DMS_START_TIME")
                    val  DMS_END_TIME = messageObj.getJSONObject(i).getString("DMS_END_TIME")
                    val DMS_ID = messageObj.getJSONObject(i).getString("DMS_ID")
                      sessionManager?.dmsId(DMS_ID)
                    val  slots = Slots(DMS_START_TIME,DMS_END_TIME,DMS_ID)
                    slotsList.add(slots)
                    if (slotsList !=null){
                        showSlotsList(slotsList)
                    }
                }
                if (slotsList !=null){
                    showSlotsList(slotsList)
                }

            } catch (e: Exception) { // caught while parsing the response
                //dismissLoaderActivity()
                Toast.makeText(this, "Something went wrong!!!", Toast.LENGTH_SHORT).show()
                e.printStackTrace()
            }

        },
            Response.ErrorListener { volleyError -> // error occurred
                //dismissLoaderActivity()
                Toast.makeText(applicationContext, "Server encountered some problem !!!", Toast.LENGTH_SHORT).show()
            }) {

            override fun getParams(): MutableMap<String, String> {
                val params: MutableMap<String, String> = java.util.HashMap()
                params.put("doctorid", doctorId)
                params.put("appointment_type", appointmentType)
                params.put("status", "1")
                params.put("date", selectedDate)
                return params
            }

            override fun getHeaders(): Map<String, String> {
                val headers: MutableMap<String, String> = HashMap()
                headers.put("Authorizations", authToken)
                return headers
            }
        }
        volleyRequestQueue?.add(strReq)
        //showLoaderActivity()

    }

    private fun showSlotsList(slotList: List<Slots>){
        val slotsAdapter = AvailableSlotsAdapter(slotsList,this)
        val layoutManager = LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL,false)
        SlotsRecyclerView.layoutManager = layoutManager
        SlotsRecyclerView.adapter = slotsAdapter

    }
    private fun walletsDialog(){
        val mDialogView = LayoutInflater.from(this).inflate(R.layout.appointment_type, null)
        val mBuilder = AlertDialog.Builder(this).setView(mDialogView)
        val appointmentTypeConfirmBtn = mDialogView.findViewById<Button>(R.id.appointmentType_Btn)
        var onlineApp = mDialogView.findViewById<CheckBox>(R.id.online_appointment)
        val offlineApp = mDialogView.findViewById<CheckBox>(R.id.offline_appointment)
        val  mAlertDialog = mBuilder.show()
        mAlertDialog.setCanceledOnTouchOutside(false)

        appointmentTypeConfirmBtn.setOnClickListener {
                if (onlineApp.isChecked){
                    val txt ="1"
                    appointmentType = txt
                    mAlertDialog.dismiss()
                } else if (offlineApp.isChecked){
                    val txt = "2"
                        appointmentType = txt
                    mAlertDialog.dismiss()
                } else{

                }
        }
        mBuilder.setCancelable(false)
    }
}
