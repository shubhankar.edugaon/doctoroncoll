package com.example.doctoroncalls.Activityes

import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.example.doctoroncalls.Models.SessionManager
import com.example.doctoroncalls.Models.UrlUtills
import com.example.doctoroncalls.R
import kotlinx.android.synthetic.main.activity_about_us.*
import org.json.JSONArray

class AboutUsActivity : AppCompatActivity() {
    private var urlUtills: UrlUtills? = UrlUtills()
    var sharedpreferences: SharedPreferences? = null
    private var sessionManager: SessionManager? = null
    val SHARED_PREFS = "shared_prefs"
    val ID = "id"
    var id = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_about_us)

        aboutUs_webView.loadUrl(urlUtills!!.aboutUs())
        aboutUs_webView.settings.javaScriptEnabled = true
        aboutUs_webView.settings.setSupportZoom(true)

        arrowBack.setOnClickListener {
            onBackPressed()
        }

    }
}