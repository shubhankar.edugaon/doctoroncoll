package com.example.doctoroncalls.Fragments

import android.content.ContentValues.TAG
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.CheckBox
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AlertDialog
import androidx.viewpager2.widget.ViewPager2
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.VolleyError
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.bumptech.glide.Glide
import com.denzcoskun.imageslider.constants.ScaleTypes
import com.denzcoskun.imageslider.models.SlideModel
import com.example.doctoroncalls.Activityes.*
import com.example.doctoroncalls.Models.DoctorModels
import com.example.doctoroncalls.Models.ImageList
import com.example.doctoroncalls.Models.SessionManager
import com.example.doctoroncalls.Models.UrlUtills
import com.example.doctoroncalls.R
import de.hdodenhof.circleimageview.CircleImageView
import kotlinx.android.synthetic.main.doctor_alert_dialog.view.*
import kotlinx.android.synthetic.main.fragment_home.*
import kotlinx.android.synthetic.main.fragment_home.view.*
import kotlinx.android.synthetic.main.home_nave_header_layout.view.*
import kotlinx.android.synthetic.main.location_item.view.*
import org.json.JSONArray


class HomeFragment : LoaderFragment() {
    lateinit var userName:TextView
    lateinit var userWelcome: TextView
    private lateinit var drawerToggle: ActionBarDrawerToggle
    var sharedpreferences: SharedPreferences? = null
    val SHARED_PREFS = "shared_prefs"
    var AUTH_TOKEN = "AUTH_TOKEN"
    val PROFILE_IMAGE = "PROFILE_IMAGE"
    var REFERAL_CODE = "REFERRAL_CODE"
    var STATUS = "STATUS"
    val ID = "id"
    val KEY_LOGIN_TOKEN = "KEY_LOGIN_TOKEN"
    val PROFILE_STATUS = "PROFILE_STATUS"
    private var sessionManager: SessionManager? = null
    private var urlUtills: UrlUtills = UrlUtills()
    var id = ""
    var login_token = ""
    var distId = ""
    var auth_token = ""
    var refferal_cose = ""
    var doctorStatus = ""
    var image = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        sessionManager = SessionManager(requireActivity().applicationContext)
        sharedpreferences = activity?.getSharedPreferences(SHARED_PREFS, Context.MODE_PRIVATE)
        id = sharedpreferences?.getString(ID,"").toString()
        auth_token = sharedpreferences?.getString(AUTH_TOKEN,"").toString()
        image = sharedpreferences?.getString(PROFILE_IMAGE,"").toString()
        doctorStatus = sharedpreferences?.getString(PROFILE_STATUS,"").toString()
        val state = sharedpreferences?.getString("stateName", "")
        distId = sharedpreferences?.getString("distid", "").toString()
        refferal_cose = sharedpreferences?.getString(REFERAL_CODE,"").toString()
        var distName = sharedpreferences?.getString("distName", "")
        Log.e(TAG, "tag: " + refferal_cose)
         distId =  "0"
        if (distName !=null){
            sliderImages()
        }else if (distName == "0"){
            sliderImages()
        } else{
            sliderImages()
        }
        if (isOnline()) {
        getUserData()
        }else{
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_home, container, false)
        //view.wallet_textView.text = auth_token
        view.wallets_layout.setOnClickListener {
            val  intent = Intent(activity?.applicationContext, WalletsActivity::class.java)
            startActivity(intent)
        }

            view.secondRow_layout.setOnClickListener {
                Log.e(TAG, "doctorStatus: " + doctorStatus)
                if (doctorStatus == "4"){
                    val intent = Intent(activity, MeetingScheduleActivity::class.java)
                    intent.putExtra("status", doctorStatus)
                    startActivity(intent)
                }else if (doctorStatus == "1"){
                    val intent = Intent(activity, MeetingScheduleActivity::class.java)
                    intent.putExtra("status", doctorStatus)
                    startActivity(intent)
                } else if (doctorStatus == "5"){
                    val intent = Intent(activity, MeetingScheduleActivity::class.java)
                    intent.putExtra("status", doctorStatus)
                    startActivity(intent)
                } else if (doctorStatus == "2"){
                    updateKycDialog()
                } else if (doctorStatus == "6"){
                    updateKycDialog()
                }else if (doctorStatus == "3"){
                    updateKycDialog()
                }
                else{
               updateKycDialog()
            }
        }

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        wallets_layout.setOnClickListener {
            val intent = Intent(activity, WalletsActivity::class.java)
            startActivity(intent)
        }

        drawer_imageView.setOnClickListener {
            my_drawer_layout.openDrawer(Gravity.LEFT)
        }
        drawerToggle = ActionBarDrawerToggle(activity, my_drawer_layout, R.string.open, R.string.close)
        my_drawer_layout.addDrawerListener(drawerToggle)
        drawerToggle.syncState()
        activity?.actionBar?.setDisplayHomeAsUpEnabled(true)

        val navHeaderView = homeNav_view.inflateHeaderView(R.layout.home_nave_header_layout)

        navHeaderView.termsAndCodition_in_homeDrawer.setOnClickListener {
            val intent = Intent(activity?.applicationContext, TermsAndCondicationActivity::class.java)
            startActivity(intent)
        }
        userName = navHeaderView.findViewById(R.id.name_textView_homeNaveHeader)
        userWelcome = navHeaderView.findViewById(R.id.welcome_textView_homeNaveHeader)
        Glide.with(requireActivity().applicationContext).load(image).into(navHeaderView.userProfileImage_CircleImage)

        navHeaderView.prvacy_policy.setOnClickListener {
            val intent = Intent(activity?.applicationContext, PrivacyPolicyActivity::class.java)
            startActivity(intent)
        }

        navHeaderView.logOut_textView.setOnClickListener {
            activity?.let { it1 -> sessionManager?.logoutUser(it1) }
        }
        navHeaderView.accounts_homeNaveDrawer_textView.setOnClickListener {
            my_drawer_layout.close()
        }
        navHeaderView.myEarnings_in_homeDrawer.setOnClickListener {
            shareIt()
        }
        navHeaderView.myRateUs_in_homeDrawer.setOnClickListener {
            shareIt()
        }
        navHeaderView.profileHeader_textView.setOnClickListener {
            val intent = Intent(activity?.applicationContext, UserProfileActivity::class.java)
            startActivity(intent)
        }
        navHeaderView.myFeedBack_in_homeDrawer.setOnClickListener {
            val intent = Intent(activity?.applicationContext, FeedBackActivity::class.java)
            startActivity(intent)
        }
        navHeaderView.faq_textView.setOnClickListener {
            val intent = Intent(activity?.applicationContext, FAQActivity::class.java)
            startActivity(intent)
        }
        navHeaderView.profileHeader_textView.setOnClickListener {
            val intent = Intent(activity?.applicationContext, UserProfileActivity::class.java)
            startActivity(intent)
        }
        navHeaderView.profileList.setOnClickListener { 
            val intent = Intent(activity?.applicationContext,ReferalActivity::class.java)
            startActivity(intent)
        }
//        navHeaderView.userProfileImage_CircleImage.setOnClickListener {
//            val intent = Intent(activity,NotificationActivity::class.java)
//            startActivity(intent)
//        }
        navHeaderView.aboutUs.setOnClickListener {
            val intent = Intent(activity,AboutUsActivity::class.java)
            startActivity(intent)
        }

        chemist_layout.setOnClickListener {
            val fragmentManager = requireActivity().supportFragmentManager
            fragmentManager.beginTransaction()
                .replace(R.id.fragment_frameLayout, ServicesFragment()).commit()
        }
        thirdRow_layout.setOnClickListener {
            val fragmentManager = requireActivity().supportFragmentManager
            fragmentManager.beginTransaction()
                .replace(R.id.fragment_frameLayout,OrderListFragment()).commit()
        }
    }

    private fun shareIt() {
        val intent = Intent()
        intent.action = Intent.ACTION_SEND
        intent.putExtra(Intent.EXTRA_SUBJECT, "Android Studio Pro")
        intent.putExtra(
            Intent.EXTRA_TEXT, DoctorModels.appLinks+"=$refferal_cose"
        )
        intent.type = "text/plain"
        startActivity(intent)
    }
    private fun sliderImages() {
        var volleyRequestQueue: RequestQueue? = null
        volleyRequestQueue = Volley.newRequestQueue(activity)
        val strReq: StringRequest = object : StringRequest(Method.POST, urlUtills?.getSliderImages(),
                Response.Listener { response ->
                    dismissLoader()
                    Log.e(TAG, "response: " + response)
                    try {
                        val jsonArray = JSONArray("[$response]")
                        val messageObj = jsonArray.getJSONObject(0).get("message") as JSONArray
                        val id = messageObj.length()
                        val imageList = java.util.ArrayList<SlideModel>()

                        for (i in 0 until messageObj.length()){
                            val imageSlide = messageObj.getJSONObject(i).get("slider_image")
                            val title = messageObj.getJSONObject(i).get("title")
                            val image = "http://software.doctoroncalls.in/uploads/$imageSlide"
                            imageList.add(SlideModel(image))
                            imageSlider1.setImageList(imageList, ScaleTypes.FIT)
                        }
                    } catch (e: Exception) { // caught while parsing the response
                        dismissLoader()
                       // Toast.makeText(this.requireContext(), "Something went wrong!!!", Toast.LENGTH_SHORT).show()
                        e.printStackTrace()
                    }
                },
                Response.ErrorListener { volleyError -> // error occurred
                    dismissLoader()
                    Toast.makeText(activity, "Server encountered some problem !!!", Toast.LENGTH_SHORT).show()
                }) {

                override fun getParams(): MutableMap<String, String> {
                    val params: MutableMap<String, String> = java.util.HashMap()
                    params.put("districtid", distId)
                    return params
                }

                override fun getHeaders(): Map<String, String> {
                    val headers: MutableMap<String, String> = HashMap()
                    headers.put("Authorizations", auth_token)
                    return headers
                }
            }
        volleyRequestQueue?.add(strReq)
        displayLoaderFragment()

    }
    private fun getUserData(){
        var volleyRequestQueue: RequestQueue? = null
        volleyRequestQueue = Volley.newRequestQueue(activity)
        var strReq: StringRequest = object : StringRequest(Method.POST,urlUtills.getUserDetails(), Response.Listener { response ->
            try {
                val  jsonArray = JSONArray("[$response]")
                val balance  = jsonArray.getJSONObject(0).get("balance")
                val  balenceArray = JSONArray("[$balance]")
                    val  name = balenceArray.getJSONObject(0).get("BRANCH_NAME")
                    val contact = balenceArray.getJSONObject(0).get("BRANCH_CONTACT")
                    val  gender = balenceArray.getJSONObject(0).get("BRANCH_GENDER")
                    val address = balenceArray.getJSONObject(0).get("BRANCH_ADDRESS")
                    val email = balenceArray.getJSONObject(0).get("BRANCH_EMAIL")
                    val shareLinks = balenceArray.getJSONObject(0).get("PLAY_STORE_URL")
                    val profileStatus = balenceArray.getJSONObject(0).get("PROFILE_STATUS")
                    val profilePhot  = balenceArray.getJSONObject(0).get("PATIENT_PHOTO")
                    DoctorModels.name = name.toString()
                    DoctorModels.contact = contact.toString()
                    DoctorModels.email = email.toString()
                    DoctorModels.appLinks = shareLinks.toString()
                    DoctorModels.gender = gender.toString()
                    DoctorModels.address  = address.toString()
                    DoctorModels.image  = profilePhot.toString()
                doctorStatus = profileStatus.toString()
                    sessionManager?.userDetails(name.toString(), contact.toString(),email.toString(),gender.toString(),address.toString(),profileStatus.toString(),profilePhot.toString())
                if (name!=null) {
                    userName.text = DoctorModels.name
                    userWelcome.visibility = View.GONE
                    userName.visibility = View.VISIBLE
                }else{
                    userWelcome.visibility = View.VISIBLE
                    userName.visibility = View.GONE
                }




            }catch (e: Exception){
                e.printStackTrace()
            } },
            Response.ErrorListener { error: VolleyError? ->
                Toast.makeText(activity,"Somthings went wrong",Toast.LENGTH_LONG).show()
            }) {
            override fun getParams(): MutableMap<String, String>? {
                val  params: MutableMap<String, String> = java.util.HashMap()
                params.put("loginid", id)
                return params

            }

            override fun getHeaders(): MutableMap<String, String> {
                val headers: MutableMap<String, String> = HashMap()
                headers.put("Authorizations", auth_token)
                return headers
            }
        }
        volleyRequestQueue?.add(strReq)
    }

    private fun updateKycDialog(){
            val mDialogView = LayoutInflater.from(this.requireContext()).inflate(R.layout.location_item, null)
            val mBuilder = AlertDialog.Builder(this.requireContext()).setView(mDialogView)
            val  mAlertDialog = mBuilder.show()
            mAlertDialog.setCanceledOnTouchOutside(false)
        mDialogView.locationName.visibility = View.GONE
        mDialogView.location_title.text =  "Update kyc"
        mDialogView.locationHome_button.text = "Go to KYC"
        if (doctorStatus == "2"){
            mDialogView.locationHome_button.text = "Ok"
            mDialogView.locationName.visibility = View.GONE
            mDialogView.location_title.text =  "You are pathology doctor. You no need to create meeting schedule"
        } else if (doctorStatus == "6"){
            mDialogView.locationHome_button.text = "Ok"
            mDialogView.locationName.visibility = View.GONE
            mDialogView.location_title.text =  "You are chemist doctor. You no need to create meeting schedule"
        }else if(doctorStatus == "3"){
            mDialogView.locationHome_button.text = "Ok"
            mDialogView.locationName.visibility = View.GONE
            mDialogView.location_title.text =  "You are Ambulance. You no need to create meeting schedule"
        }
        if (doctorStatus == "2"){
            mDialogView.locationHome_button.visibility = View.GONE
            mDialogView.locationCancel_button.visibility = View.GONE
            mDialogView.ok_button.visibility = View.VISIBLE
            mDialogView.ok_button.setOnClickListener {
                mAlertDialog.dismiss()
            }
        }else if (doctorStatus == "6"){
            mDialogView.locationHome_button.visibility = View.GONE
            mDialogView.locationCancel_button.visibility = View.GONE
            mDialogView.ok_button.visibility  = View.VISIBLE
            mDialogView.ok_button.setOnClickListener {
                mAlertDialog.dismiss()
            }
        } else if (doctorStatus == "3"){
            mDialogView.ok_button.visibility = View.VISIBLE
            mDialogView.locationHome_button.visibility = View.GONE
            mDialogView.locationCancel_button.visibility = View.GONE
            mDialogView.ok_button.setOnClickListener {
                mAlertDialog.dismiss()
            }
        }
        mDialogView.locationHome_button.setOnClickListener {
            val intent = Intent(activity,ReferalActivity::class.java)
            startActivity(intent)
        }
        mDialogView.locationCancel_button.setOnClickListener {
            mAlertDialog.dismiss()
        }

            mBuilder.setCancelable(false)
        }


    }


