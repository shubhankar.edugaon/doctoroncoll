package com.example.doctoroncalls.Activityes

import android.content.ContentValues
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.bumptech.glide.Glide
import com.example.doctoroncalls.Models.DoctorModels
import com.example.doctoroncalls.Models.SessionManager
import com.example.doctoroncalls.Models.UrlUtills
import com.example.doctoroncalls.NotificationModules.*
import com.example.doctoroncalls.R
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_appointment_view_details.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.json.JSONArray


class AppointmentViewDetailsActivity : LoaderActivity() {
    private var urlUtills: UrlUtills? = UrlUtills()
    val  TAG = "NotificationActivity"
    private var sessionManager: SessionManager? = null
    val SHARED_PREFS = "shared_prefs"
    val ID: String = "id"
    var sharedpreferences: SharedPreferences? = null
    var id = ""
    var status = ""
    var appointmentId = ""
    var photoUrl = ""
    var AUTH_TOKEN = "AUTH_TOKEN"
    var mobile = ""
    var token = ""
    var appointmentDate = ""
    var patientName = ""
    var loginToken = ""
    var authId = ""
    var da_login_id = ""
    var doctorName = ""
    var message = ""
    var DMS_START_TIME = ""
    var DMS_END_TIME = ""
    var appointmentTpe = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_appointment_view_details)
        sessionManager = SessionManager(this@AppointmentViewDetailsActivity)
        sharedpreferences = getSharedPreferences(SHARED_PREFS, Context.MODE_PRIVATE)
        id = sharedpreferences?.getString(ID,"").toString()

       authId = DoctorModels.authId
        appointmentId = intent.getStringExtra("appointmentId").toString()
        token = intent.getStringExtra("token").toString()
        appointmentDate = intent.getStringExtra("appointmentDate").toString()
        patientName = intent.getStringExtra("patientName").toString()
        da_login_id = intent.getStringExtra("da_login_id").toString()
        photoUrl = intent.getStringExtra("imageUrl").toString()
        doctorName = intent.getStringExtra("doctor_name").toString()
        message = intent.getStringExtra("message").toString()
        DMS_START_TIME = intent.getStringExtra("DMS_START_TIME").toString()
        DMS_END_TIME = intent.getStringExtra("DMS_END_TIME").toString()
        appointmentTpe = intent.getStringExtra("appointmentType").toString()

        val p = message
        Glide.with(this).load(photoUrl).into(userImage_view)
        if (da_login_id == id){
            buttonLayout.visibility = View.GONE
        }else if (message == "Cancelled"){
            buttonLayout.visibility  = View.GONE
        } else if (message == "Appointment Confirmed"){
            buttonLayout.visibility = View.GONE
        }
        patientName_textView.text = patientName
        appointmentTime.text = DMS_START_TIME+" "+DMS_END_TIME
        appointmentDate_textView.text = appointmentDate
            if (appointmentTpe == "1"){
                appointmentType_textView.text = "Online consult"
            } else if (appointmentTpe == "2"){
                appointmentType_textView.text = "Visit clinic"
            }
        sessionManager = SessionManager(this)
        sharedpreferences = getSharedPreferences(SHARED_PREFS, Context.MODE_PRIVATE)
        authId = sharedpreferences?.getString("phoneAuth","").toString()
        loginToken = sharedpreferences?.getString(AUTH_TOKEN,"").toString()


        cancelButton.setOnClickListener {
            val cancel = "2"
            status = cancel
            appointmentAcception()
            val title = "Your appointment has Cancel because"
            if (title.isNotEmpty()){
                if (title.isNotEmpty()) {
                    PushNotification(
                        NotificationData(title,""),
                        token
                    ).also {
                        sendNotificationToPatient(it)
                    }
                }
            }
        }
        acceptButton.setOnClickListener {
            val accept = "1"
            status = accept
            appointmentAcception()
            val title_n = "Your appointment Confirm"
            val messageT = "You will get notification to take video call links your appointment time, Go  to appointment List, Select your appointment item start video call"
            if (title.isNotEmpty() && messageT.isNotEmpty()) {
                PushNotification(
                    NotificationData(title_n, messageT),
                    token
                ).also {
                    sendNotificationToPatient(it)
                }
            }
        }
    }

    private fun sendNotificationToPatient(notification: PushNotification) = CoroutineScope(Dispatchers.IO).launch {
        try {
            val response = RetrofitInstance.api.postNotification(notification)
            if (response.isSuccessful){
                Log.d(ContentValues.TAG,"Response: ${Gson().toJson(response)}")
                val intent = Intent(this@AppointmentViewDetailsActivity,DashActivity::class.java)
                startActivity(intent)
            } else{
                Log.e(ContentValues.TAG, response.errorBody().toString())
            }
        } catch (e: Exception){
            Log.e(ContentValues.TAG, e.toString())
        }
    }

    private fun appointmentAcception(){
        var volleyRequestQueue: RequestQueue? = null
        val TAG = "Handy Opinion Tutorials"

        volleyRequestQueue = Volley.newRequestQueue(this)
        val strReq: StringRequest = object : StringRequest(Method.POST,urlUtills?.updateAppoint(), Response.Listener { response ->
            Log.e(TAG, "response: " + response)
                dismissLoaderActivity()
            try {
                val jsonArray = JSONArray("[$response]")
                val jsonObject = jsonArray.getJSONObject(0)
                val message = jsonObject.getString("message")
                val contact = jsonObject.getString("CONTACT")
                mobile = contact
                if (mobile !=null){
                    val title = message.toString()
                    val description = "Congratls your appointment accepted"
                    if (title.isNotEmpty() && description.isNotEmpty()){
                    }
                }
                else if (message == "Appointment Confirmed"){
                    val intent = Intent(this,DashActivity::class.java)
                    startActivity(intent)
                } else if (message == "Cancelled"){
                    Toast.makeText(this,"Your appointment cancel",Toast.LENGTH_LONG).show()
                    val intent = Intent(this,DashActivity::class.java)
                    startActivity(intent)
                }
            } catch (e: Exception) { // caught while parsing the response
                Toast.makeText(this, "Something went wrong!!!", Toast.LENGTH_SHORT).show()
                e.printStackTrace()
            }
        },
            Response.ErrorListener { volleyError -> // error occurred
                Toast.makeText(applicationContext, "Server encountered some problem !!!", Toast.LENGTH_SHORT).show()
            }) {

            override fun getParams(): MutableMap<String, String> {
                val params: MutableMap<String, String> = java.util.HashMap()
                params.put("status",status)
                params.put("appointment_id",appointmentId)
                params.put("loginid", id)
                params.put("id", authId)
                params.put("MEETING_LINKS","https://meet.jit.si/"+patientName)
                return params
            }

            override fun getHeaders(): Map<String, String> {
                val headers: MutableMap<String, String> = HashMap()
                headers.put("Authorizations", loginToken)
                return headers
            }
        }
        volleyRequestQueue?.add(strReq)
        showLoaderActivity()
    }
}