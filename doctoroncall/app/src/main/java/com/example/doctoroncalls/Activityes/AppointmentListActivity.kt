package com.example.doctoroncalls.Activityes

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.example.doctoroncalls.Fragments.AppointmentPendingFragment
import com.example.doctoroncalls.Fragments.ApponitmetCompletedFragment
import com.example.doctoroncalls.Fragments.MeetingUpcomigFragment
import com.example.doctoroncalls.R
import kotlinx.android.synthetic.main.activity_appointment_list.*
import kotlinx.android.synthetic.main.activity_appointment_list.view.*
import kotlinx.android.synthetic.main.activity_rate_us.*
import java.util.ArrayList

class AppointmentListActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_appointment_list)
//        arrowBak_imageView.setOnClickListener {
//            onBackPressed()
//        }

        val adapter = AppointmentUpcomingListViewPagerAdapter(supportFragmentManager)
        adapter.addFragments(AppointmentPendingFragment(), "Pending")
        adapter.addFragments(ApponitmetCompletedFragment(),"Completed")
        adapter.addFragments(MeetingUpcomigFragment(), "Upcoming")
        appointmentDetails_viewpager.adapter = adapter
        textViewLayout_textView.setupWithViewPager(appointmentDetails_viewpager)
    }


    class AppointmentUpcomingListViewPagerAdapter(fragment: FragmentManager): FragmentPagerAdapter(fragment){
        private val fragmentList : MutableList<Fragment> = ArrayList()
        private val titleList : MutableList<String> = ArrayList()
        override fun getCount(): Int {
            return  fragmentList.size
        }

        override fun getItem(position: Int): Fragment {
            return fragmentList[position]
        }

        fun addFragments(fragment: Fragment, title: String){
            fragmentList.add(fragment)
            titleList.add(title)
        }

        override fun getPageTitle(position: Int): CharSequence? {
            return  titleList[position]
        }
    }
}