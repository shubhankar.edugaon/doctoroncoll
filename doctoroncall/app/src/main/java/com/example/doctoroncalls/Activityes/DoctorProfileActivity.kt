package com.example.doctoroncalls.Activityes

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.VolleyError
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.bumptech.glide.Glide
import com.example.doctoroncalls.Adapter.DoctorAdapter
import com.example.doctoroncalls.Adapter.DoctorHospitalListAdapter
import com.example.doctoroncalls.Models.HospitalList
import com.example.doctoroncalls.Models.SessionManager
import com.example.doctoroncalls.Models.UrlUtills
import com.example.doctoroncalls.R
import com.google.gson.JsonArray
import kotlinx.android.synthetic.main.activity_doctor_profile.*
import kotlinx.android.synthetic.main.activity_doctor_profile.arroback
import kotlinx.android.synthetic.main.activity_doctor_profile_list.*
import org.json.JSONArray

class DoctorProfileActivity : LoaderActivity() {
        var doctorCall = ""
        var doctorWhatApp = ""
        var doctorEmail = ""
        var doctorName = ""
        var doctorPhoto = ""
        var consultFee = ""
        var profileStatus = ""
        var doctorExperience = ""
        var SPECIALITY = ""
        var onlineConsult = ""
        var branchAddress = ""
    val SHARED_PREFS = "shared_prefs"
    val KEY_LOGIN_TOKEN = "KEY_LOGIN_TOKEN"
    var auth_token = ""
    var doctorId = ""
    var AUTH_TOKEN = "AUTH_TOKEN"
    var licenseNo = ""
    var ambulanceNo = ""
    var sharedpreferences: SharedPreferences? = null
    private var hospitalArrayList = ArrayList<HospitalList>()
    private var urlUtills: UrlUtills? = UrlUtills()
    private var sessionManager: SessionManager? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_doctor_profile)
        sessionManager = SessionManager(this@DoctorProfileActivity)
        sharedpreferences = getSharedPreferences(SHARED_PREFS, Context.MODE_PRIVATE)
        auth_token = sharedpreferences?.getString(AUTH_TOKEN,"").toString()

        sessionManager = SessionManager(this)
//        goToHome.setOnClickListener {
//            onBackPressed()
//        }
        doctorExperience = intent.getStringExtra("experience").toString()
        doctorName = intent.getStringExtra("BRANCH_NAME").toString()
        doctorCall = intent.getStringExtra("pranchContact").toString()
        doctorWhatApp = intent.getStringExtra("pranchContact").toString()
        doctorEmail = intent.getStringExtra("email").toString()
        consultFee  = intent.getStringExtra("consultationFees").toString()
        doctorPhoto = intent.getStringExtra("doctorPhoto").toString()
        profileStatus = intent.getStringExtra("PROFILE_STATUS").toString()
        SPECIALITY = intent.getStringExtra("SPECIALITY").toString()
        onlineConsult = intent.getStringExtra("online").toString()
        branchAddress = intent.getStringExtra("address").toString()
        doctorId = intent.getStringExtra("doctorId").toString()
        licenseNo = intent.getStringExtra("licenseNo").toString()
        ambulanceNo = intent.getStringExtra("ambulanceNo").toString()
        Glide.with(this).load(doctorPhoto).into(image_title)
        arroback.setOnClickListener {
            onBackPressed()
        }
        if (profileStatus == "4"){
            specialist_name.text = "Homeopathy doctor"
            detailsScreen_textView.text = "Homeopathy details"
            doctorName_textView.text = doctorName
            experience_name.text = doctorExperience
            specialist_name.text = SPECIALITY
        } else if (profileStatus == "1"){
            specialist_name.text = "Allopathic doctor"
            detailsScreen_textView.text = "Allopathic details"
            doctorName_textView.text = doctorName
            experience_name.text = doctorExperience
            specialist_name.text = SPECIALITY
        }else if (profileStatus == "5"){
            specialist_name.text = "Ayurveda doctor"
            detailsScreen_textView.text = "Ayurveda details"
            doctorName_textView.text = doctorName
            experience_name.text = doctorExperience
            specialist_name.text = SPECIALITY
        } else if (profileStatus == "6"){
            specialist_name.text = "Chemist"
            detailsScreen_textView.text = "Chemist details"
            doctorInformation_cardView.visibility = View.GONE
            doctor_list_recyclerView.visibility  = View.GONE
            doctorName_textView.text = doctorName
            experience_name.text = doctorExperience
//            specialist_name.text = SPECIALITY
            specialist_name.text = licenseNo
        } else  if (profileStatus == "2"){
            specialist_name.text = "Pathology"
            detailsScreen_textView.text = "Pathology details"
            doctorInformation_cardView.visibility = View.GONE
            doctor_list_recyclerView.visibility  = View.GONE
            specialist_name.text = licenseNo
            doctorName_textView.text = doctorName
            experience_name.text = doctorExperience
            //specialist_name.text = SPECIALITY
        } else if (profileStatus == "3"){
            specialist_name.text = "Ambulance"
            detailsScreen_textView.text = "Ambulance Details"
            specialist_name.text = licenseNo
            specialist_name.text = ambulanceNo
            specialist_name.text = licenseNo
        }

        if (onlineConsult == "null"){
            onlineConsultFee.text  = "Not found"
        }else{
                onlineConsultFee.text = "₹ "+onlineConsult
        }

        if (consultFee == "null"){
            offlineConsultFee.text = "Not found"
        }else{
            offlineConsultFee.text = "₹ "+consultFee
        }

        whatApp_imageView.setOnClickListener {
            val i = Intent(
                Intent.ACTION_VIEW,
                Uri.parse("https://api.whatsapp.com/send?phone=" + doctorWhatApp)
            )
            startActivity(i)
        }
//        message_cardView.setOnClickListener {
//                val intent = Intent()
//                intent.action = Intent.ACTION_SEND
//                intent.putExtra(Intent.EXTRA_SUBJECT, "Android Studio Pro")
//                intent.putExtra(Intent.EXTRA_TEXT, doctorEmail)
//                intent.type = "text/plain"
//                startActivity(intent)
//        }
        getHospitalName()
    }

    private fun makeCall(number: String){
        val intent = Intent(Intent.ACTION_CALL)
        intent.data = Uri.parse("tel:$number")
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.CALL_PHONE), 100)
            return
        }
        startActivity(intent)
    }

    private fun getHospitalName() {
        var volleyRequestQueue: RequestQueue? = null
        val TAG = "Handy Optinion Tuorials"
        volleyRequestQueue = Volley.newRequestQueue(this)
        var strReq: StringRequest = object : StringRequest(
            Method.POST, urlUtills?.meetingSchedule(), Response.Listener { response ->
                try {
                    val jsonArray = JSONArray("[$response]")
                    val messageObj = jsonArray.getJSONObject(0).get("message")
                    val messageArray = JSONArray("[$messageObj]")
                    val meetingObj = messageArray.getJSONObject(0).get("meetings") as JSONArray
                    val count = meetingObj.length()
                    val p = count.toString() + "{$hospitalArrayList(s)}" + "found"
                    for (i in 0 until meetingObj.length()) {
                        val DOS_HOSPITAL_NAME =
                            meetingObj.getJSONObject(i).getString("DOS_HOSPITAL_NAME")
                        val DMS_START_TIME = meetingObj.getJSONObject(i).getString("DMS_START_TIME")
                        val DMS_END_TIME = meetingObj.getJSONObject(i).getString("DMS_END_TIME")
                        val DAYS_NAME = meetingObj.getJSONObject(i).getString("DAYS_NAME")
                        val DOS_ADDRESS = meetingObj.getJSONObject(i).getString("DOS_ADDRESS")
                        val hospitalList = HospitalList(
                            DOS_HOSPITAL_NAME,
                            DMS_START_TIME,
                            DMS_END_TIME,
                            DAYS_NAME,
                            DOS_ADDRESS
                        )
                        hospitalArrayList.add(hospitalList)
                    }
                    if (hospitalArrayList != null) {
                        showHospitalList(hospitalArrayList)
                    } else {
//                    doctorListRecycler.visibility = View.VISIBLE
//                    doctorListRecycler.visibility = View.GONE
                    }

                } catch (e: Exception) {
                    e.printStackTrace()
                    Toast.makeText(this, "Something went wrong!!!", Toast.LENGTH_SHORT).show()
                }
            }, Response.ErrorListener { error: VolleyError? ->
                Toast.makeText(
                    applicationContext,
                    "Server encountered some problem !!!",
                    Toast.LENGTH_SHORT
                ).show()
            }
        ){
            override fun getParams(): MutableMap<String, String>? {
                val params: MutableMap<String, String> = java.util.HashMap()
                params.put("loginid",doctorId)
                return params
            }

            override fun getHeaders(): MutableMap<String, String> {
                val headers: MutableMap<String, String> = HashMap()
                // Add your Header paramters here
                headers.put("Authorizations", auth_token)
                return headers
            }
        }
        volleyRequestQueue.add(strReq)
    }

    private fun showHospitalList(hospitalLit: List<HospitalList>){
        val doctorAdapter = DoctorHospitalListAdapter(hospitalLit)
        val layoutManage = LinearLayoutManager(this, LinearLayoutManager.VERTICAL,false)
        doctor_list_recyclerView.layoutManager = layoutManage
        doctor_list_recyclerView.adapter = doctorAdapter
    }

}
