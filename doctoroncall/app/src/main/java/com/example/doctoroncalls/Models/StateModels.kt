package com.example.doctoroncalls.Models

class StateModels {
}

data class StateL(
    var STATE_ID: String? = null,
    var STATE_NAME: String? = null,
    var COUNTRY_ID: String? = null
)

data class Dist(
    var DISTRICT_ID: String? = null,
    var DISTRICT_NAME: String? = null
)
