package com.example.doctoroncalls.Activityes

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.Toast
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.example.doctoroncalls.Models.DoctorModels
import com.example.doctoroncalls.Models.SessionManager
import com.example.doctoroncalls.Models.UrlUtills
import com.example.doctoroncalls.NotificationModules.FirebaseService
import com.example.doctoroncalls.R
import com.google.android.material.textfield.TextInputEditText
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.messaging.FirebaseMessaging
import kotlinx.android.synthetic.main.activity_register_activity.*
import org.json.JSONArray

class RegisterActivityActivity : LoaderActivity() {
    lateinit var register_btn: Button
    lateinit var userName: TextInputEditText
    lateinit var userMobile: TextInputEditText
    lateinit var userPass: TextInputEditText
    private var urlUtills: UrlUtills? = UrlUtills()
    val SHARED_PREFS = "shared_prefs"
    val ID: String = "id"
    var sharedpreferences: SharedPreferences? = null
    private lateinit var auth: FirebaseAuth
    lateinit var firestore: FirebaseFirestore
    private var sessionManager: SessionManager? = null
    var loginId = ""
    var mobileAuth:String = ""
    var VERIF_ID = "VERIFICATION_ID"
    var token = ""
    val email = ""
    var name = ""
    var mobile = ""
    var secret_key = ""
    var authId = ""
    var firebase_token = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register_activity)
        mobile = intent.getStringExtra("userMobile").toString()
        FirebaseMessaging.getInstance().token.addOnSuccessListener { result ->
            FirebaseService.token = result
            firebase_token  = result
        }
        signIn.setOnClickListener {
            val intent = Intent(this,LoginActivity::class.java)
            startActivity(intent)
        }

        auth = FirebaseAuth.getInstance()
        firestore = FirebaseFirestore.getInstance()

        sessionManager = SessionManager(this@RegisterActivityActivity)
        register_btn = findViewById(R.id.register_btn)
        userName = findViewById(R.id.name_edit_text)
        userMobile = findViewById(R.id.mobile_number)
        userPass = findViewById(R.id.pass_text)
        sharedpreferences = getSharedPreferences(SHARED_PREFS, Context.MODE_PRIVATE)
        authId = sharedpreferences?.getString(VERIF_ID,"").toString()
        mobile_number.setText(mobile)

        register_btn.setOnClickListener {
            register()
        }
    }
    private fun register(){
        var volleyRequestQueue: RequestQueue? = null
        val TAG = "Handy Opinion Tutorials"

        volleyRequestQueue = Volley.newRequestQueue(this)
        val strReq: StringRequest = object : StringRequest(Method.POST,urlUtills?.register(), Response.Listener { response ->
            Log.e(TAG, "response: " + response)
            try {
                dismissLoaderActivity()
                val jsonArray = JSONArray("[$response]")
                val jsonObject = jsonArray.getJSONObject(0)
                val message = jsonObject.getString("message")
                if (message == "already"){
                        Toast.makeText(this,"mobile number already register",Toast.LENGTH_LONG).show()
                } else if (message == "ok"){
                     loginId = jsonObject.getString("id")
                    val key_id = jsonObject.getString("KEY_ID")
                    name = jsonObject.getString("name")
                     mobile = jsonObject.getString("contact")
                     secret_key = jsonObject.getString("KEY_SECRET")
                    val registerToken = jsonObject.getString("token")
                    val REFERAL_CODE = jsonObject.getString("REFERAL_CODE")
                    sessionManager?.storeToken(registerToken)
                            sessionManager?.createRegisterSession(loginId,key_id,name,mobile,registerToken,REFERAL_CODE)
                        if (registerToken !=null){
                            storeDada()
                            showLoaderActivity()
                        }
                }
            } catch (e: Exception) { // caught while parsing the response
                Toast.makeText(this, "Something went wrong!!!", Toast.LENGTH_SHORT).show()
                e.printStackTrace()
            }
        },
            Response.ErrorListener { volleyError -> // error occurred
                Toast.makeText(applicationContext, "Server encountered some problem !!!", Toast.LENGTH_SHORT).show()
            }) {
            override fun getParams(): MutableMap<String, String> {
                val params: MutableMap<String, String> = java.util.HashMap()
                mobileAuth = DoctorModels.authId
                var g = authId
                params.put("name", userName.text.toString().trim())
                params.put("contact", userMobile.text.toString().trim())
                params.put("password", userPass.text.toString().trim())
                params.put("token", firebase_token)
                params.put("referal_code", "111")
                return params
            }

            override fun getHeaders(): Map<String, String> {

                val headers: MutableMap<String, String> = HashMap()
                return headers
            }
        }
        volleyRequestQueue?.add(strReq)
        showLoaderActivity()
    }
    private fun storeDada(){
            val data = HashMap<String,Any>()
            data["email"] = name
            data["mobile"] = mobile
            data["mobileId"] = mobileAuth
            data["name"] = name
            data["email"] = email
            data["token"] = firebase_token
            data["status"] = "0"
                firestore.collection("users").document(mobileAuth)
                    .set(data)
                    .addOnSuccessListener {
                        dismissLoaderActivity()
                        Toast.makeText(this,"Upload successfully",Toast.LENGTH_LONG).show()
                        val intent = Intent(this,DashActivity::class.java)
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
                        startActivity(intent)
                        finish()
                    }.addOnFailureListener{
                        Toast.makeText(this,"failed", Toast.LENGTH_LONG).show()
                    }
    }
}