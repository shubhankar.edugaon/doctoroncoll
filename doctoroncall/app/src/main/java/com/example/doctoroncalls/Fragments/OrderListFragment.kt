package com.example.doctoroncalls.Fragments
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import androidx.viewpager2.widget.ViewPager2
import com.example.doctoroncalls.Activityes.DashActivity
import com.example.doctoroncalls.Adapter.TabPageAdapter
import com.example.doctoroncalls.Models.*
import com.example.doctoroncalls.R
import com.google.android.material.tabs.TabLayout
import kotlinx.android.synthetic.main.fragment_order_list.*
import kotlinx.android.synthetic.main.fragment_order_list.view.*

class OrderListFragment : LoaderFragment() {
private var sessionManager: SessionManager? = null
 private var urlUtills: UrlUtills? = UrlUtills()
    var sharedpreferences: SharedPreferences? = null
    private var doctorList = ArrayList<Doctor>()
    val SHARED_PREFS = "shared_prefs"
    var id = ""
    val ID = "id"
    var token = ""
    var patientName = ""
    var appointmentDate = ""
    val KEY_LOGIN_TOKEN = "KEY_LOGIN_TOKEN"
    var loginToken = ""
    private lateinit var tab_layot: TabLayout
    lateinit var viewPager_layout: ViewPager2
    private var appointmentLiat = ArrayList<DoctorList>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_order_list, container, false)
    viewPager_layout = view.findViewById(R.id.viewPager)
        tab_layot = view.findViewById(R.id.tabLayout)
    view.arrowBak_imageView.setOnClickListener {
        val intent = Intent(activity,DashActivity::class.java)
        startActivity(intent)
    }
        setUpAdapter()
        return view
    }

    private fun setUpAdapter(){
        val adapter = TabPageAdapter(requireActivity(), tab_layot.tabCount)
            viewPager_layout.adapter = adapter
        viewPager_layout.registerOnPageChangeCallback(object: ViewPager2.OnPageChangeCallback(){
            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
                super.onPageScrolled(position, positionOffset, positionOffsetPixels)
                    tab_layot.selectTab(tab_layot.getTabAt(position))
            }
        })
            tab_layot.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener{
                override fun onTabSelected(tab: TabLayout.Tab?) {
                    viewPager_layout.currentItem = tab!!.position
                }

                override fun onTabUnselected(tab: TabLayout.Tab?) {

                }

                override fun onTabReselected(tab: TabLayout.Tab?) {

                }

            })
    }
}