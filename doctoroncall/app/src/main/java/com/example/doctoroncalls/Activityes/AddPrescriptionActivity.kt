package com.example.doctoroncalls.Activityes

import android.app.AlertDialog
import android.app.DatePickerDialog
import android.app.ProgressDialog
import android.content.ContentValues.TAG
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.widget.*
import androidx.core.net.toUri
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.example.doctoroncalls.Models.SessionManager
import com.example.doctoroncalls.Models.UrlUtills
import com.example.doctoroncalls.R
import com.google.android.gms.tasks.Continuation
import com.google.android.gms.tasks.Task
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.textfield.TextInputEditText
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.UploadTask
import de.hdodenhof.circleimageview.CircleImageView
import kotlinx.android.synthetic.main.activity_add_prescription.*
import kotlinx.android.synthetic.main.image_upload_layout.view.*
import kotlinx.android.synthetic.main.upcoming_view_dialog.*
import org.json.JSONArray
import java.io.ByteArrayOutputStream
import java.io.File
import java.text.SimpleDateFormat
import java.util.*

class AddPrescriptionActivity : LoaderActivity() {
    private var sessionManager: SessionManager? =  null
    lateinit var upload_btn:Button
    lateinit var referalId: TextInputEditText
    lateinit var date: EditText
    lateinit var description: EditText
    var progressDialog: ProgressDialog? = null
    private val IMG = 2
    lateinit var filePath: Uri
    private val pickImage = 100
    private val cameraRequest = 1888
    private var userImageUri: Uri? = null
    private var insuranceBottomSheetDialog: BottomSheetDialog? = null
    lateinit var selectDate: TextView
    lateinit var showImage: CircleImageView
    private var urlUtills: UrlUtills? = UrlUtills()
    var sharedpreferences: SharedPreferences? = null
    val SHARED_PREFS = "shared_prefs"
    var AUTH_TOKEN = "AUTH_TOKEN"
    val ID = "id"
    private val IMG_REQUEST = 1
    var imageUri: Uri? = null
    var bitmap: Bitmap? = null
    var id = ""
    var refer_id = ""
    var currentDate = ""
    var patientDescription = ""
    var imageURL = ""
    var tokan = ""
    var branchId = ""
    var photo = ""
    var loginToken = ""
    var patientName = ""
    var PROFILE_STATUS = ""
    private val calendar = Calendar.getInstance()
    var consultrationFee: String = ""
    var storage = FirebaseStorage.getInstance()
    private val storageRef = storage.reference

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_prescription)
        arroback.setOnClickListener {
            onBackPressed()
        }

        sessionManager = SessionManager(this@AddPrescriptionActivity)
        sharedpreferences = getSharedPreferences(SHARED_PREFS, Context.MODE_PRIVATE)
        id = sharedpreferences?.getString(ID,"").toString()
        loginToken = sharedpreferences?.getString(AUTH_TOKEN,"").toString()

        Log.e(TAG, "response: " + id)
        branchId = intent.getStringExtra("branch_id").toString()
        PROFILE_STATUS = intent.getStringExtra("PROFILE_STATUS").toString()
        consultrationFee = intent.getStringExtra("DOCTOR_CONSULTATION_FEE").toString()
        tokan = intent.getStringExtra("branchToken").toString()
        patientName = intent.getStringExtra("doctorName").toString()
        upload_btn = findViewById(R.id.upload_prescription_btn)
        referalId = findViewById(R.id.mobile_edit_text)
        date = findViewById(R.id.date_editText)
        description = findViewById(R.id.uploadImage_editText)
        showImage = findViewById(R.id.showImage_upload)


        selectDate = findViewById(R.id.startDateCalender)
        doctor_name.text = patientName
        showImage_upload.setOnClickListener {
          uploadPrescriptionBottomSheet()
        }
        upload_btn.setOnClickListener {
            validation()
        }
        selectDate.setOnClickListener {
            selectDate()
        }
    }

    private fun uploadPrescriptionBottomSheet() {
        insuranceBottomSheetDialog = BottomSheetDialog(this)
        val view = layoutInflater.inflate(R.layout.image_upload_layout, null)
        val openFile = view.findViewById<TextView>(R.id.openFile_textView)
        val openCamera = view.findViewById<TextView>(R.id.takeImage_textView)
        val openGallery = view.findViewById<TextView>(R.id.gallery_textView)
        val cancel = view.findViewById<Button>(R.id.cancel_button)
        val uploadButton = view.findViewById<Button>(R.id.uploadDriverDocument_button)
        view.uploadDoctorPic_textView.text = "Upload prescription image"
        uploadButton.setOnClickListener {
            showLoaderActivity()
            uploadUserFirebaseImage()
            insuranceBottomSheetDialog!!.cancel()
        }
        openFile.setOnClickListener {
            aadharFontTakeImageFromFile()
        }
        openCamera.setOnClickListener {
        selectimage()
        }

        openGallery.setOnClickListener {
            aadharFontTFromGallery()
        }
        cancel.setOnClickListener {
            insuranceBottomSheetDialog!!.dismiss()
        }

        insuranceBottomSheetDialog!!.setContentView(view)
        insuranceBottomSheetDialog!!.show()

        insuranceBottomSheetDialog!!.setCancelable(false)
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 111 && resultCode == RESULT_OK && data != null) {
            filePath = data.data!!
            val bitmap = MediaStore.Images.Media.getBitmap(contentResolver, filePath)
            showImage_upload.setImageBitmap(bitmap)

        } else if (resultCode == RESULT_OK && requestCode == pickImage) {
            filePath = data?.data!!
            val bitmap = MediaStore.Images.Media.getBitmap(contentResolver, filePath)
            showImage_upload.setImageBitmap(bitmap)
        }
        else if (requestCode == cameraRequest && resultCode == RESULT_OK){
            val imageBitmap = data?.extras?.get("data") as Bitmap
            showImage_upload.setImageBitmap(imageBitmap)

            val file = File(this.cacheDir, "File")
            file.delete()
            file.createNewFile()
            val fileOutputStream = file.outputStream()
            val byteArrayOutputStream = ByteArrayOutputStream()
            imageBitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream)
            val byteArray = byteArrayOutputStream.toByteArray()
            fileOutputStream.write(byteArray)
            fileOutputStream.flush()
            fileOutputStream.close()
            byteArrayOutputStream.close()

            filePath = file.toUri()
        }
    }

    private fun uploadUserFirebaseImage(){
        if (filePath != null) {
            val ref = storageRef.child("doctors/userImages/aadharFont")
            val uploadTask = ref.putFile(filePath)
            uploadTask.continueWithTask(Continuation<UploadTask.TaskSnapshot, Task<Uri>> { task ->
                if (!task.isSuccessful) {
                    task.exception?.let {
                        throw it
                    }
                }
                return@Continuation ref.downloadUrl
            })
                .addOnCompleteListener { task ->
                    dismissLoaderActivity()
                    if (task.isSuccessful) {
                        userImageUri = task.result
                        photo = userImageUri.toString()
                    }
                }
                .addOnFailureListener {
                    dismissLoaderActivity()
                    it.message
                    Toast.makeText(this, "Please upload an Image", Toast.LENGTH_LONG).show()
                }
        }
    }

    private fun validation(){
        refer_id = referalId.text.toString().trim()
        currentDate = selectDate.text.toString().trim()
        patientDescription = description.text.toString().trim()
        imageURL = showImage_upload.toString().trim()
        if (refer_id ==""){
            AlertDialog.Builder(this)
                .setMessage("Please enter your Referred By")
                .show()
        } else if (currentDate == ""){
            AlertDialog.Builder(this)
                .setMessage("Please select date")
                .show()
        } else if (patientDescription == ""){
            AlertDialog.Builder(this)
                .setMessage("Please write a description")
                .show()
        }else if (photo.isEmpty()){
                AlertDialog.Builder(this)
                    .setMessage("Please select image")
                    .show()
        }else{
            UploadPrescription()
        }

    }

    private fun selectDate(){
        val dateSetListener = object : DatePickerDialog.OnDateSetListener{
            override fun onDateSet(view: DatePicker?, year: Int, monthOfYear: Int, dayOfMonth: Int){
                calendar.set(Calendar.YEAR,year)
                calendar.set(Calendar.MONTH,monthOfYear)
                calendar.set(Calendar.DAY_OF_MONTH,dayOfMonth)
                userMedicineStartDate()
            }
        }

        DatePickerDialog(this,dateSetListener,
            calendar.get(Calendar.YEAR),
            calendar.get(Calendar.MONTH),
            calendar.get(Calendar.DAY_OF_MONTH)).show()
    }
    private fun userMedicineStartDate():String {
        val myFormat = "dd-MM-yyyy"
        val sdf = SimpleDateFormat(myFormat,Locale.US)
        selectDate.text = sdf.format(calendar.time)
        return sdf.format(calendar.time)
    }
    private fun selectimage() {
        val i = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        startActivityForResult(i, IMG_REQUEST)
    }

    private fun aadharFontTakeImageFromCamera() {
        val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        startActivityForResult(cameraIntent, cameraRequest)
    }

    private fun aadharFontTFromGallery() {
        val gallery = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI)
        startActivityForResult(gallery, pickImage)
    }

    private fun aadharFontTakeImageFromFile() {
        val intent = Intent()
        intent.type = "image/*"
        intent.action = Intent.ACTION_GET_CONTENT
        startActivityForResult(Intent.createChooser(intent, "chooser picture"), 111)
    }
    private fun UploadPrescription(){
        var volleyRequestQueue: RequestQueue? = null
        val TAG = "Handy Opinion Tutorials"

        volleyRequestQueue = Volley.newRequestQueue(this)
        val strReq: StringRequest = object : StringRequest(Method.POST, urlUtills?.prescriptionDetails(), Response.Listener { response ->
            Log.e(TAG, "response: " + response)
            try {
                dismissLoaderActivity()
                val jsonArray = JSONArray("[$response]")
                val messageObj = jsonArray.getJSONObject(0).get("message")
                val confirmMessageObj = jsonArray.getJSONObject(0).get("confirm_messsage")
                val confirmMessageArray = JSONArray("[$confirmMessageObj]")
                val confirmMessage = confirmMessageArray.getJSONObject(0)
                    val name = confirmMessage.get("BRANCH_NAME")
                    val refer_by = confirmMessage.get("REFERED_BY")
                    val message = confirmMessage.get("MESSAGE")
                    val custoner_file = confirmMessage.get("CUSTOMER_FILE")
                    val patient_name = confirmMessage.get("PATIENT_NAME")
                    val patient_phone = confirmMessage.get("PATIENT_PHONE")
                        sessionManager?.prescriptionDetails(name.toString(),refer_by.toString(), message.toString(),custoner_file.toString(),patient_name.toString(),patient_phone.toString())
                        if (messageObj == "ok"){
                                val intent = Intent(this,AppointmentConfirmActivity::class.java)
                                intent.putExtra("BRANCH_NAME", name.toString())
                                intent.putExtra("REFERED_BY", refer_by.toString())
                                intent.putExtra("BRANCH_TOKEN", tokan)
                                intent.putExtra("MESSAGE", message.toString())
                                intent.putExtra("CUSTOMER_FILE", custoner_file.toString())
                                intent.putExtra("PATIENT_NAME",patient_name.toString())
                                intent.putExtra("PATIENT_PHONE", patient_phone.toString())
                                intent.putExtra("DOCTOR_CONSULTATION_FEE",consultrationFee)
                                intent.putExtra("PROFILE_STATUS",PROFILE_STATUS)
                                intent.putExtra("message",messageObj.toString())
                                    startActivity(intent)
                            finish()
                        }
            } catch (e: Exception) { // caught while parsing the response
                Toast.makeText(this, "Something went wrong!!!", Toast.LENGTH_SHORT).show()
                e.printStackTrace()
            }
        },
            Response.ErrorListener { volleyError -> // error occurred
                //
                Toast.makeText(applicationContext, "Server encountered some problem !!!", Toast.LENGTH_SHORT).show()
            }) {

            override fun getParams(): MutableMap<String, String> {
                val params: MutableMap<String, String> = java.util.HashMap()
                                params.put("loginid",id)
                                params.put("refered_by", refer_id)
                                params.put("app_date", currentDate)
                                params.put("short_desc", patientDescription)
                                params.put("fileToUpload",photo)
                                params.put("appointment_to",branchId)

                                return params
            }

            override fun getHeaders(): Map<String, String> {
                val headers: MutableMap<String, String> = HashMap()
                headers.put("Authorizations", loginToken)
                return headers
            }
        }
        // Adding request to request queue
        volleyRequestQueue?.add(strReq)
        showLoaderActivity()
    }
}