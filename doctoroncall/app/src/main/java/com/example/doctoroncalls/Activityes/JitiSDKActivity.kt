package com.example.doctoroncalls.Activityes

import android.content.ContentValues.TAG
import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.example.doctoroncalls.Models.SessionManager
import com.example.doctoroncalls.NotificationModules.NotificationData
import com.example.doctoroncalls.NotificationModules.PushNotification
import com.example.doctoroncalls.NotificationModules.RetrofitInstance
import com.example.doctoroncalls.R
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_jiti_sdkactivity.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.jitsi.meet.sdk.JitsiMeetActivity
import org.jitsi.meet.sdk.JitsiMeetConferenceOptions
import java.net.MalformedURLException
import java.net.URL


class JitiSDKActivity : AppCompatActivity() {
    private val MY_PERMISSIONS_REQUEST_SEND_SMS = 0
    var meetingId = ""
    var patientName = ""
    var token = ""
    var appoint_start_time = ""
    var appoitm_end_time = ""
    private var PRIVATE_MODE = 0
    private val PREF_NAME = "mindorks-welcome"
    private var sessionManager: SessionManager? = null
    val SHARED_PREFS = "shared_prefs"
    val ID: String = "id"
    var sharedpreferences: SharedPreferences? = null
    var id = ""
    var da_login_id = ""
    var sendBtn: Button? = null
    var txtphoneNo: EditText? = null
    var txtMessage: EditText? = null
    var phoneNo: String? = null
    var message: String? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_jiti_sdkactivity)

        arrowBac_ddijiti.setOnClickListener {
            onBackPressed()
        }

        invite_to_patient.setOnClickListener {
        }

        sessionManager = SessionManager(this@JitiSDKActivity)
        sharedpreferences = getSharedPreferences(SHARED_PREFS, Context.MODE_PRIVATE)
        id = sharedpreferences?.getString(ID,"").toString()
        da_login_id = intent.getStringExtra("da_login_id").toString()
        token = intent.getStringExtra("token").toString()
        patientName = intent.getStringExtra("patientName").toString()
        appoint_start_time = intent.getStringExtra("appoint_start_time").toString()
        appoitm_end_time = intent.getStringExtra("appoint_end_time").toString()

        if (da_login_id == id){
                invite_to_patient.visibility = View.GONE

            } else{
                invite_to_patient.visibility = View.VISIBLE
            createId_editText.hint = "Create a links"
        }
        joinMeeting_btn.setOnClickListener {
            validation()
        }
        invite_to_patient.setOnClickListener {
            message()
        }
        try {
            val options = JitsiMeetConferenceOptions.Builder()
                .setServerURL(URL(""))
                .setWelcomePageEnabled(false)
                .build()
        } catch (e: MalformedURLException) {
            e.printStackTrace()
        }
    }

    private fun validation(){
        meetingId = createId_editText.text.toString().trim()
        if (meetingId.isEmpty()){
            AlertDialog.Builder(this)
                .setMessage("Please enter room id")
        } else{
            joinMeeting()
            message()
        }
    }

private fun joinMeeting(){
    (meetingId.length > 0)
    val options = JitsiMeetConferenceOptions.Builder()
        .setRoom(meetingId)
        .build()
    JitsiMeetActivity.launch(this, options)
}

    private  fun message(){
        val  links  = "https://meet.jit.si/"+meetingId
        if (links !=null){
            val title = "Invite to join"+ links
            val description = "Hi $patientName, here is a video call links please copy it and go to your appointment section and start video call btn and join, your appointment time is $appoint_start_time - $appoitm_end_time"
                if (title.isNotEmpty() && description.isNotEmpty()) {
                    PushNotification(
                        NotificationData(title,description),
                        TOPIC1
                    ).also {
                        sendNotification(it)
                    }
                }
        }
    }
    private fun sendNotification(notification: PushNotification) = CoroutineScope(Dispatchers.IO).launch{
        try {
            val response = RetrofitInstance.api.postNotification(notification)
            if(response.isSuccessful){
                Toast.makeText(this@JitiSDKActivity,"successfully",Toast.LENGTH_LONG).show()
                Log.d(TAG,"message: ${Gson().toJson(response)}")
            }else{
                Log.e(TAG,response.errorBody().toString())
            }
        }catch (e: Exception){
            Log.e(TAG, e.toString())
        }
    }

    private fun storeLinks(){

    }
}