package com.example.doctoroncalls.Activityes

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.example.doctoroncalls.Models.SessionManager
import com.example.doctoroncalls.Models.UrlUtills
import com.example.doctoroncalls.R
import kotlinx.android.synthetic.main.activity_referal.*
import org.json.JSONArray

class ReferalActivity : LoaderActivity() {
    private var urlUtills: UrlUtills? = UrlUtills()
    private var sessionManager: SessionManager? = null
    val SHARED_PREFS = "shared_prefs"
    val ID: String = "id"
    var AUTH_TOKEN = "AUTH_TOKEN"
    var loginId = ""
    var token = ""
    var sharedpreferences: SharedPreferences? = null
    var referalBy = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_referal)
        sessionManager = SessionManager(this@ReferalActivity)
        sharedpreferences = getSharedPreferences(SHARED_PREFS, Context.MODE_PRIVATE)
        loginId = sharedpreferences?.getString(ID,"").toString()
        token = sharedpreferences?.getString(AUTH_TOKEN,"").toString()
        previousButton.setOnClickListener {
            onBackPressed()
        }
        submitReferralId.setOnClickListener {
            validation()
        }
        arrowBack_imageView.setOnClickListener {
            onBackPressed()
        }
    }

    private fun validation(){
        referalBy = referred_id_editText.text.toString().trim()
        if(referalBy.isEmpty()){
            androidx.appcompat.app.AlertDialog.Builder(this)
                .setMessage("please enter your  valid referral id")
                .show()
        }else{
            addReferralId()
        }
    }

    private fun addReferralId(){
        var volleyRequestQueue: RequestQueue? = null
        val TAG = "Handy Opinion Tutorials"

        volleyRequestQueue = Volley.newRequestQueue(this)
        val strReq: StringRequest = object : StringRequest(Method.POST,urlUtills?.referalId(), Response.Listener { response ->
            Log.e(TAG, "response: " + response)
            try {
                dismissLoaderActivity()
                val jsonArray = JSONArray("[$response]")
                val jsonObject = jsonArray.getJSONObject(0)
                val title = jsonObject.getString("title")
                val message = jsonObject.getString("message")
                if (title == "Success"){
                    val intent = Intent(this,SelectCatogaryActivity::class.java)
                    startActivity(intent)
                } else if (title == "Fail"){
                    Toast.makeText(this,"Invalid referId",Toast.LENGTH_LONG).show()
                }

            } catch (e: Exception) { // caught while parsing the response
                dismissLoaderActivity()
                Toast.makeText(this, "Something went wrong!!!", Toast.LENGTH_SHORT).show()
                e.printStackTrace()
            }
        },
            Response.ErrorListener { volleyError -> // error occurred
                //
                dismissLoaderActivity()
                Toast.makeText(applicationContext, "Server encountered some problem !!!", Toast.LENGTH_SHORT).show()
            }) {
            override fun getParams(): MutableMap<String, String> {
                val params: MutableMap<String, String> = java.util.HashMap()
                params["referal_ID"] =  referalBy
                return params
            }
            override fun getHeaders(): Map<String, String> {
                val headers: MutableMap<String, String> = HashMap()
                headers.put("Authorizations", token)
                return headers
            }
        }
        volleyRequestQueue?.add(strReq)
        showLoaderActivity()
    }

}