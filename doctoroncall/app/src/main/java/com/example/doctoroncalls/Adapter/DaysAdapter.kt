package com.example.doctoroncalls.Adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.compose.ui.graphics.Color
import androidx.recyclerview.widget.RecyclerView
import com.example.doctoroncalls.Models.Days
import com.example.doctoroncalls.Models.SessionManager
import com.example.doctoroncalls.R
import kotlinx.android.synthetic.main.days_layout.view.*

class DaysAdapter(var daysList: List<Days>, private var onItemClickListener: DaysAdapter.OnItemClickListener): RecyclerView.Adapter<DaysAdapter.DaysViewHolder>(){
    private var selectedItemPosition: Int = 0
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DaysAdapter.DaysViewHolder {
        return DaysViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.days_layout,parent,false),onItemClickListener)
    }

    @SuppressLint("ResourceAsColor")
    override fun onBindViewHolder(holder: DaysAdapter.DaysViewHolder, @SuppressLint("RecyclerView") position: Int) {
        holder.bind(daysList[position])
        val  data = daysList[position]
        holder.daysList = data
    }

    override fun getItemCount(): Int {
     return daysList.size
    }

    class DaysViewHolder(itemView: View, onItemClickListener: OnItemClickListener): RecyclerView.ViewHolder(itemView){
        lateinit var daysList: Days
        private var sessionManager: SessionManager? = null
        fun bind(days: Days){
                    val name = itemView.findViewById<TextView>(R.id.day_textView)
            name.text = days.DAYS_NAME
        }
        init {
            itemView.setOnClickListener {
                onItemClickListener.dayClick(daysList)
            }
        }
    }
    interface OnItemClickListener{
        fun dayClick(days: Days)
    }
}