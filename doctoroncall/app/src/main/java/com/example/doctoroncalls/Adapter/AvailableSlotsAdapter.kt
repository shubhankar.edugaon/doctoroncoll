package com.example.doctoroncalls.Adapter

import android.R.color.background_dark
import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.doctoroncalls.Models.Slots
import com.example.doctoroncalls.R
import kotlinx.android.synthetic.main.slots_layout.view.*

class AvailableSlotsAdapter(private val slotsList: List<Slots>, private var onItemClickListener: OnItemClickListener): RecyclerView.Adapter<AvailableSlotsAdapter.SlotsViewHolder>() {
        private var selectedItemPosition: Int = 0
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SlotsViewHolder {
        return SlotsViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.slots_layout, parent, false), onItemClickListener)
    }

    override fun onBindViewHolder(holder: SlotsViewHolder, @SuppressLint("RecyclerView") position: Int) {
        holder.bind(slotsList[position])
        val data = slotsList[position]
        holder.availableSlotsModels = data
       // holder.availableSlotsModels = data
//        holder.itemView.setOnClickListener {
//            selectedItemPosition = position
//            //notifyDataSetChanged()
//        }
//        if (selectedItemPosition == position)
//            holder.itemView.cardView.setBackgroundColor(Color.parseColor("#33CC99"))
//        else
//            holder.itemView.cardView.setBackgroundColor(Color.parseColor("#8AF6F2F2"))
}

    override fun getItemCount(): Int {
        return slotsList.size
    }
    @SuppressLint("ResourceAsColor")
    class SlotsViewHolder(itemView: View, onItemClickListener: OnItemClickListener): RecyclerView.ViewHolder(itemView){
        lateinit var availableSlotsModels: Slots
        //val context: Context = itemView.context
            fun bind(slots: Slots){
                        val startSlotsTime = itemView.findViewById<TextView>(R.id.slots_title)
                            startSlotsTime.text = slots.DMS_START_TIME+" "+"-"+slots.DMS_END_TIME
            }
        init {
                itemView.setOnClickListener {
                        onItemClickListener.onSlotsClick(availableSlotsModels,itemView)
                }
        }
    }
interface OnItemClickListener {
    fun onSlotsClick(slots: Slots, itemView: View)
}
}