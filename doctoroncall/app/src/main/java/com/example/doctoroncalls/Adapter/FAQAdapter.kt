package com.example.doctoroncalls.Adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.doctoroncalls.Models.FAQList
import com.example.doctoroncalls.R
import kotlinx.android.synthetic.main.faq_item_view.view.*

class FAQAdapter(var faqList: List<FAQList>, private var faqItemClickListener: FaqOnItemClickListener): RecyclerView.Adapter<FAQAdapter.FaqViewHolder>(){
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FAQAdapter.FaqViewHolder {
      return FaqViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.faq_item_view,parent,false),faqItemClickListener)
    }

    override fun onBindViewHolder(holder: FAQAdapter.FaqViewHolder, position: Int) {
        holder.bind(faqList[position])
        val data = faqList[position]
        holder.faqList = data
    }

    override fun getItemCount(): Int {
      return faqList.size
    }

    class FaqViewHolder(itemView: View,faqItemClickListener: FaqOnItemClickListener): RecyclerView.ViewHolder(itemView){
        lateinit var faqList: FAQList
        fun bind(faqList: FAQList){
                itemView.question_textView.text = faqList.FANSWER
            //itemView.faqDescription_textView.text = faqList.FQUESTION
        }
        init {
            itemView.setOnClickListener {
            faqItemClickListener.onClick(faqList)
            }
        }
    }

    interface FaqOnItemClickListener{
        fun onClick(faqList: FAQList)
    }
}