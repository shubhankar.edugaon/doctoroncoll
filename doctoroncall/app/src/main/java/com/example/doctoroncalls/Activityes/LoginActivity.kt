package com.example.doctoroncalls.Activityes

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.provider.Settings.Global.putString
import android.util.Log
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.example.doctoroncalls.Models.DoctorModels
import com.example.doctoroncalls.Models.SessionManager
import com.example.doctoroncalls.Models.UrlUtills
import com.example.doctoroncalls.NotificationModules.FirebaseService
import com.example.doctoroncalls.R
import com.google.android.material.textfield.TextInputEditText
import com.google.firebase.messaging.FirebaseMessaging
import kotlinx.android.synthetic.main.activity_login2.*
import org.json.JSONArray


class LoginActivity : LoaderActivity() {
    lateinit var registerBtn: TextView
    lateinit var loginButton: Button
    lateinit var mobileNumber: TextInputEditText
    lateinit var password: TextInputEditText
    private var urlUtills: UrlUtills? = UrlUtills()
    private var PRIVATE_MODE = 0
    private val PREF_NAME = "mindorks-welcome"
    private var sessionManager: SessionManager? = null
    val SHARED_PREFS = "shared_prefs"
    val ID: String = "id"
    var sharedpreferences: SharedPreferences? = null
    var kryId = ""
    var verfyId = ""
    var otificationToken = ""
    private val sharedPrefFile = "kotlinsharedpreference"
    var firebase_token = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login2)
        FirebaseMessaging.getInstance().token.addOnSuccessListener { result ->
            FirebaseService.token = result
            firebase_token  = result
        }
        val sharedPreferences: SharedPreferences = this.getSharedPreferences(sharedPrefFile,Context.MODE_PRIVATE)
        registerBtn = findViewById(R.id.registe_page)
        mobileNumber = findViewById(R.id.mobile_edit_text)
        password = findViewById(R.id.pass_edit_text)
        loginButton = findViewById(R.id.login_btn)
        registe_page.setOnClickListener {
            val intent = Intent(this,MobActivity::class.java)
            startActivity(intent)
        }
        sessionManager = SessionManager(this@LoginActivity)
        sharedpreferences = getSharedPreferences(SHARED_PREFS, Context.MODE_PRIVATE)

//         kryId = sharedpreferences?.getString(sessionManager?.createRegisterSession(ID,"","","","","").toString(),null).toString()
//                var i = kryId
//        Log.d("i_value" , i)
                    if (sharedpreferences?.getString(ID,null)!=null){
                        intent = Intent(this,DashActivity::class.java)
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
                        startActivity(intent)
                        finish()
                    }

        loginButton.setOnClickListener {
           val mobile = mobileNumber.text.toString()
            val pass = password.text.toString().trim()
            if (mobile == ""){
                AlertDialog.Builder(this)
                    .setMessage("please enter your mobile")
                    .show()
            } else if (pass == ""){
                AlertDialog.Builder(this)
                    .setMessage("Please enter your valid password")
                    .show()
            } else{
                    login()
            }
        }
    }

    private fun login(){
        var volleyRequestQueue: RequestQueue? = null
        val TAG = "Handy Opinion Tutorials"

        volleyRequestQueue = Volley.newRequestQueue(this)
        val strReq: StringRequest = object : StringRequest(Method.POST,urlUtills?.login(), Response.Listener { response ->
            Log.e(TAG, "response: " + response)
            dismissLoaderActivity()
            try {
                val jsonArray = JSONArray("[$response]")
                val jsonObject = jsonArray.getJSONObject(0)
                val message = jsonObject.getString("message")
                val id = jsonObject.getString("id")
                val token = jsonObject.getString("token")
                val KEY_ID = jsonObject.getString("KEY_ID")
                val KEY_SECRET = jsonObject.getString("KEY_SECRET")
                val REFERAL_CODE = jsonObject.getString("REFERAL_CODE")
                val FIREBASE_TOKEN = jsonObject.getString("FIREBASE_TOKEN")
                val contact = jsonObject.getString("contact")
                sessionManager?.storeToken(token.toString())
                sessionManager?.createRegisterSession(id, KEY_ID,contact,token.toString(),KEY_SECRET,REFERAL_CODE.toString())
                if (message == "ok"){
                    Toast.makeText(this,"successFully",Toast.LENGTH_LONG).show()
                    val intent= Intent(this,DashActivity::class.java)
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
                    startActivity(intent)
                    finish()
                } else if (message == "error"){
                    dismissLoaderActivity()
                    dismissLoaderActivity()
                    Toast.makeText(this,"Invalid details", Toast.LENGTH_LONG).show()
                }
            } catch (e: Exception) { // caught while parsing the response
                Toast.makeText(this, "Something went wrong!!!", Toast.LENGTH_SHORT).show()
                e.printStackTrace()
            }
        },
            Response.ErrorListener { volleyError -> // error occurred
                Toast.makeText(applicationContext, "Server encountered some problem !!!", Toast.LENGTH_SHORT).show()
            }) {

            override fun getParams(): MutableMap<String, String> {
                val params: MutableMap<String, String> = java.util.HashMap()
                params.put("contact", mobileNumber.text.toString().trim())
                params.put("password", password.text.toString().trim())
                params.put("firebase_token",firebase_token)
                params.put("code", "001")
                return params
            }

            override fun getHeaders(): Map<String, String> {
                val headers: MutableMap<String, String> = HashMap()
                return headers
            }
        }
        volleyRequestQueue?.add(strReq)
        showLoaderActivity()
    }

}

