package com.example.doctoroncalls.Activityes

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.webkit.WebView
import com.example.doctoroncalls.Models.UrlUtills
import com.example.doctoroncalls.R
import kotlinx.android.synthetic.main.activity_privacy_policy.*

class PrivacyPolicyActivity : AppCompatActivity() {
    lateinit var webView: WebView
    private var urlUtil: UrlUtills = UrlUtills()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_privacy_policy)

        arroback.setOnClickListener {
            onBackPressed()
        }
        webView = findViewById(R.id.webView)
        webView.loadUrl(urlUtil.privacyPolicy())
        webView.settings.javaScriptEnabled = true
        webView.settings.setSupportZoom(true)

    }

    override fun onBackPressed() {
        // if your webview can go back it will go back
        if (webView.canGoBack())
            webView.goBack()
        // if your webview cannot go back
        // it will exit the application
        else
            super.onBackPressed()
    }
}