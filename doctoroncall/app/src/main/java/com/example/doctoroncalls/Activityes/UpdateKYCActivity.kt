package com.example.doctoroncalls.Activityes

import android.annotation.SuppressLint
import android.content.*
import android.graphics.Bitmap
import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.net.toUri
import com.android.volley.*
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.example.doctoroncalls.Models.SessionManager
import com.example.doctoroncalls.Models.UrlUtills
import com.example.doctoroncalls.R
import com.google.android.gms.tasks.Continuation
import com.google.android.gms.tasks.Task
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.UploadTask
import kotlinx.android.synthetic.main.activity_update_kycactivity.*
import org.json.JSONArray
import java.io.ByteArrayOutputStream
import java.io.File


class UpdateKYCActivity : LoaderActivity() {
    private var urlUtills: UrlUtills? = UrlUtills()
    private var sessionManager: SessionManager? = null
    val SHARED_PREFS = "shared_prefs"
    var AUTH_TOKEN = "AUTH_TOKEN"
    val ID: String = "id"
    lateinit var filePath: Uri
    private val pickImage = 100
    private val cameraRequest = 1888
    private var userImageUri: Uri? = null
    private var aadharFontUri:Uri? = null
    private var aadharBackUri:Uri? = null
    private var insuranceBottomSheetDialog:BottomSheetDialog? = null
    var loginId = ""
    var sharedpreferences: SharedPreferences? = null
    var aadhar = ""
    var bankName = ""
    var accountNumber = ""
    var ifscCode = ""
    var upiId = ""
    var token = ""
    var photo = ""
    var fontPhot = ""
    var backPhoto = ""
    lateinit var imageImage: ImageView
    private var imageData: ByteArray? = null
    private val db = FirebaseFirestore.getInstance()
    private val auth = FirebaseAuth.getInstance()
    var storage = FirebaseStorage.getInstance()
   private val storageRef = storage.reference
//    private val id = auth.currentUser!!.uid
    companion object {
        private const val IMAGE_PICK_CODE = 999
        private const val AADHAR_FONT_REQUEST_CODE = 112
        private const val AADHAR_BACK_REQUEST_CODE = 113

    }
    var status = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_update_kycactivity)
         status = intent.getStringExtra("status").toString()
        sessionManager = SessionManager(this)
        sharedpreferences = getSharedPreferences(SHARED_PREFS, Context.MODE_PRIVATE)
        loginId = sharedpreferences?.getString(ID,"").toString()
        token = sharedpreferences?.getString(AUTH_TOKEN,"").toString()

        arrowBack_kyc.setOnClickListener {
            appointmentAlert()
        }
        upload_font_aadhar.setOnClickListener {
            uploadAadharFontBottomSheet()
        }
        submitKyc.setOnClickListener {
            validation()
        }
        upload_photo.setOnClickListener {
            uploadProfileBottomSheet()
        }

        upload_back_aadhar.setOnClickListener {
            uploadAadharBackBottomSheet()
        }
    }

    private fun uploadProfileBottomSheet() {
        insuranceBottomSheetDialog = BottomSheetDialog(this)
        val view = layoutInflater.inflate(R.layout.image_upload_layout, null)
        val openFile = view.findViewById<TextView>(R.id.openFile_textView)
        val openCamera = view.findViewById<TextView>(R.id.takeImage_textView)
        val openGallery = view.findViewById<TextView>(R.id.gallery_textView)
        imageImage = view.findViewById(R.id.uploadedDriverDocument_imageView)
        val cancel = view.findViewById<Button>(R.id.cancel_button)
        val uploadButton = view.findViewById<Button>(R.id.uploadDriverDocument_button)
        uploadButton.setOnClickListener {
            insuranceBottomSheetDialog!!.cancel()
            uploadUserImage()
            showLoaderActivity()
        }
        openFile.setOnClickListener {
            takeImageFromFile()
        }
        openCamera.setOnClickListener {
            takeImageFromCamera()
        }

        openGallery.setOnClickListener {
            takeImageFromGallery()
        }
        cancel.setOnClickListener {
            insuranceBottomSheetDialog!!.dismiss()
        }

        insuranceBottomSheetDialog!!.setContentView(view)
        insuranceBottomSheetDialog!!.show()

        insuranceBottomSheetDialog!!.setCancelable(false)
    }

    private fun uploadAadharFontBottomSheet() {
        insuranceBottomSheetDialog = BottomSheetDialog(this)
        val view = layoutInflater.inflate(R.layout.image_upload_layout, null)
        val openFile = view.findViewById<TextView>(R.id.openFile_textView)
        val openCamera = view.findViewById<TextView>(R.id.takeImage_textView)
        val openGallery = view.findViewById<TextView>(R.id.gallery_textView)
        imageImage = view.findViewById(R.id.uploadedDriverDocument_imageView)
        val cancel = view.findViewById<Button>(R.id.cancel_button)
        val uploadButton = view.findViewById<Button>(R.id.uploadDriverDocument_button)
        uploadButton.setOnClickListener {
            insuranceBottomSheetDialog!!.cancel()
            //Toast.makeText(this, "Save Successfully", Toast.LENGTH_SHORT).show()
            uploadAadharFontFirebaseImage()
            showLoaderActivity()
        }
        openFile.setOnClickListener {
            takeImageFromFile()
        }
        openCamera.setOnClickListener {
            takeImageFromCamera()
        }

        openGallery.setOnClickListener {
            takeImageFromGallery()
        }
        cancel.setOnClickListener {
            insuranceBottomSheetDialog!!.dismiss()
        }

        insuranceBottomSheetDialog!!.setContentView(view)
        insuranceBottomSheetDialog!!.show()

        insuranceBottomSheetDialog!!.setCancelable(false)
    }

    private fun takeImageFromCamera() {
        val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        startActivityForResult(cameraIntent, cameraRequest)
    }

    private fun takeImageFromGallery() {
        val gallery = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI)
        startActivityForResult(gallery, pickImage)
    }

    private fun takeImageFromFile() {
        val intent = Intent()
        intent.type = "image/*"
        intent.action = Intent.ACTION_GET_CONTENT
        startActivityForResult(Intent.createChooser(intent, "chooser picture"), 111)
    }

    private fun aadharFontTakeImageFromCamera() {
        val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        startActivityForResult(cameraIntent, cameraRequest)
    }

    private fun aadharFontTFromGallery() {
        val gallery = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI)
        startActivityForResult(gallery, pickImage)
    }

    private fun aadharFontTakeImageFromFile() {
        val intent = Intent()
        intent.type = "image/*"
        intent.action = Intent.ACTION_GET_CONTENT
        startActivityForResult(Intent.createChooser(intent, "chooser picture"), 111)
    }

    private fun uploadAadharBackBottomSheet() {
        insuranceBottomSheetDialog = BottomSheetDialog(this)
        val view = layoutInflater.inflate(R.layout.image_upload_layout, null)
        val openFile = view.findViewById<TextView>(R.id.openFile_textView)
        val openCamera = view.findViewById<TextView>(R.id.takeImage_textView)
        imageImage = view.findViewById(R.id.uploadedDriverDocument_imageView)
        val openGallery = view.findViewById<TextView>(R.id.gallery_textView)
        val cancel = view.findViewById<Button>(R.id.cancel_button)
        val uploadButton = view.findViewById<Button>(R.id.uploadDriverDocument_button)
        uploadButton.setOnClickListener {
            uploadAadharBackFirebaseImage()
            showLoaderActivity()
            insuranceBottomSheetDialog!!.cancel()
        }
        openFile.setOnClickListener {
            aadharFontTakeImageFromFile()
        }
        openCamera.setOnClickListener {
            aadharFontTakeImageFromCamera()
        }

        openGallery.setOnClickListener {
            aadharFontTFromGallery()
        }
        cancel.setOnClickListener {
            insuranceBottomSheetDialog!!.dismiss()
        }

        insuranceBottomSheetDialog!!.setContentView(view)
        insuranceBottomSheetDialog!!.show()

        insuranceBottomSheetDialog!!.setCancelable(false)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 111 && resultCode == RESULT_OK && data != null) {
            filePath = data.data!!
            val bitmap = MediaStore.Images.Media.getBitmap(contentResolver, filePath)
            imageImage.setImageBitmap(bitmap)

        } else if (resultCode == RESULT_OK && requestCode == pickImage) {
            filePath = data?.data!!
            val bitmap = MediaStore.Images.Media.getBitmap(contentResolver, filePath)
            imageImage.setImageBitmap(bitmap)
        }
        else if (requestCode == cameraRequest && resultCode == RESULT_OK){
            val imageBitmap = data?.extras?.get("data") as Bitmap
            imageImage.setImageBitmap(imageBitmap)

            val file = File(this.cacheDir, "File")
            file.delete()
            file.createNewFile()
            val fileOutputStream = file.outputStream()
            val byteArrayOutputStream = ByteArrayOutputStream()
            imageBitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream)
            val byteArray = byteArrayOutputStream.toByteArray()
            fileOutputStream.write(byteArray)
            fileOutputStream.flush()
            fileOutputStream.close()
            byteArrayOutputStream.close()

            filePath = file.toUri()
        }
    }

    private fun validation(){
        aadhar = aadhar_editText.text.toString().trim()
        bankName = bankName_editText.text.toString().trim()
        accountNumber = accountNo.text.toString().trim()
        ifscCode = ifsc_code_editText.text.toString().trim()
        upiId = upiId_editText.text.toString().trim()

        if (aadhar.isEmpty()){
            AlertDialog.Builder(this)
                .setMessage("please enter your aadhar")
                .show()
        }  else if (bankName.isEmpty()){
            AlertDialog.Builder(this)
                .setMessage("please enter your bank name")
                .show()
        } else if (accountNumber.isEmpty()){
            AlertDialog.Builder(this)
                .setMessage("please enter your account number")
                .show()
        } else if (ifscCode.isEmpty()){
            AlertDialog.Builder(this)
                .setMessage("please enter your IFSC Code")
                .show()
        } else if (upiId.isEmpty()){
            AlertDialog.Builder(this)
                .setMessage("please enter your UPI id")
                .show()
        } else if (photo.isEmpty()){
            AlertDialog.Builder(this)
                .setMessage("please upload photo")
                .show()
        } else if (fontPhot.isEmpty()){
            AlertDialog.Builder(this)
                .setMessage("please upload aadhar font photo")
                .show()
        } else if (backPhoto.isEmpty()) {
            AlertDialog.Builder(this)
                .setMessage("please upload aadhar back photo")
                .show()
        } else{
            updateKyc()
        }
    }

    private fun updateKyc(){
        var volleyRequestQueue: RequestQueue? = null
        val TAG = "Handy Opinion Tutorials"

        volleyRequestQueue = Volley.newRequestQueue(this)
        val strReq: StringRequest = object : StringRequest(Method.POST,urlUtills?.updateKYc(), Response.Listener { response ->
                Log.e(TAG, "response: " + response)
            dismissLoaderActivity()
                try {
                    val jsonArray = JSONArray("[$response]")
                    val jsonObject = jsonArray.getJSONObject(0)
                    val message = jsonObject.getString("message")
                    if (message == "ok"){
                        if(status == "2"){
                        startActivity(Intent(this,DashActivity::class.java))
                        finish()
                    } else if (status == "6"){
                        startActivity(Intent(this,DashActivity::class.java))
                        finish()
                    }else if (status == "3"){
                        startActivity(Intent(this,DashActivity::class.java))
                        finish()
                    } else if (status == "5"){
                            Toast.makeText(this,"KYC Verification completed",Toast.LENGTH_LONG).show()
                            startActivity(Intent(this,MeetingScheduleActivity::class.java))
                            intent.putExtra("status",status)
                            sessionManager?.DoctorStatus(status)
                            finish()
                        } else if (status == "4"){
                            Toast.makeText(this,"KYC Verification completed",Toast.LENGTH_LONG).show()
                            startActivity(Intent(this,MeetingScheduleActivity::class.java))
                            intent.putExtra("status",status)
                            sessionManager?.DoctorStatus(status)
                            finish()
                        } else if (status == "1"){
                            Toast.makeText(this,"KYC Verification completed",Toast.LENGTH_LONG).show()
                            startActivity(Intent(this,MeetingScheduleActivity::class.java))
                            intent.putExtra("status",status)
                            sessionManager?.DoctorStatus(status)
                            finish()
                        }
                    }
                } catch (e: Exception) {
                    Toast.makeText(this, "Something went wrong!!!", Toast.LENGTH_SHORT).show()
                    dismissLoaderActivity()
                    e.printStackTrace()
                }
            },
            Response.ErrorListener { volleyError -> // error occurred
                dismissLoaderActivity()
                Toast.makeText(applicationContext, "Server encountered some problem !!!", Toast.LENGTH_SHORT).show()
            }) {

            override fun getParams(): MutableMap<String, String> {
                val params: MutableMap<String, String> = java.util.HashMap()
                params.put("aadhar_no", aadhar)
                params.put("bank_name", bankName)
                params.put("account_no", accountNumber)
                params.put("ifsc_code", ifscCode)
                params.put("virtualid", upiId)
                params.put("loginid", loginId)
                return params
            }

            override fun getHeaders(): Map<String, String> {
                val headers: MutableMap<String, String> = HashMap()
                headers.put("Authorizations", token)
                return headers
            }
        }
        volleyRequestQueue?.add(strReq)
        showLoaderActivity()
    }

    private fun uploadUserImage(userImage:String){
        var volleyRequestQueue: RequestQueue? = null
        val TAG = "Handy Opinion Tutorials"

        volleyRequestQueue = Volley.newRequestQueue(this)
        val strReq: StringRequest = @SuppressLint("SetTextI18n")
        object : StringRequest(
            Method.POST,urlUtills?.addUserImage(), Response.Listener { response ->
                Log.e(TAG, "response: " + response)
                try {
                    val jsonArray = JSONArray("[$response]")
                    val jsonObject = jsonArray.getJSONObject(0)
                    val message = jsonObject.getString("message")
                    if (message == "ok"){
                        upload_photo.text = "user Image Uploaded"
                       // Toast.makeText(this,"user image Uploaded",Toast.LENGTH_LONG).show()
                    }else{
                    }
                } catch (e: Exception) {
                    Toast.makeText(this, "Something went wrong!!!", Toast.LENGTH_SHORT).show()
                    e.printStackTrace()
                }
            },
            Response.ErrorListener { volleyError -> // error occurred
                //
                Toast.makeText(applicationContext, "Server encountered some problem !!!", Toast.LENGTH_SHORT).show()
            }) {

            override fun getParams(): MutableMap<String, String> {
                val params: MutableMap<String, String> = java.util.HashMap()
                params.put("photo",userImage)
                params.put("loginid", loginId)
                return params
            }

            override fun getHeaders(): Map<String, String> {
                val headers: MutableMap<String, String> = HashMap()
                headers.put("Authorizations", token)
                return headers
            }
        }
        volleyRequestQueue?.add(strReq)
    }

    private fun uploadUserImage(){
        if (filePath != null) {
            val ref = storageRef.child("doctors/userImages/image")
            val uploadTask = ref.putFile(filePath)
            uploadTask.continueWithTask(Continuation<UploadTask.TaskSnapshot, Task<Uri>> { task ->
                if (!task.isSuccessful) {
                    task.exception?.let {
                        throw it

                    }
                }
                return@Continuation ref.downloadUrl
            })
                .addOnCompleteListener { task ->
                    dismissLoaderActivity()
                    if (task.isSuccessful) {
                        userImageUri = task.result
                        photo = userImageUri.toString()
                        uploadUserImage(userImageUri.toString())
                    }

                }
                .addOnFailureListener {
                    dismissLoaderActivity()
                    it.message
                    Toast.makeText(this, "Please upload an Image", Toast.LENGTH_LONG).show()
                }
        }
    }

    private fun uploadAadharFontFirebaseImage(){
        if (filePath != null) {
            val ref = storageRef.child("doctors/userImages/aadharFont")
            val uploadTask = ref.putFile(filePath)
            uploadTask.continueWithTask(Continuation<UploadTask.TaskSnapshot, Task<Uri>> { task ->
                if (!task.isSuccessful) {
                    task.exception?.let {
                        throw it
                    }
                }
                return@Continuation ref.downloadUrl
            })
                .addOnCompleteListener { task ->
                    dismissLoaderActivity()
                    if (task.isSuccessful) {
                        aadharFontUri = task.result
                        fontPhot = aadharFontUri.toString()
                        uploadAadharFontImage(aadharFontUri.toString())
                    }

                }
                .addOnFailureListener {
                    dismissLoaderActivity()
                    it.message
                    Toast.makeText(this, "Please upload an Image", Toast.LENGTH_LONG).show()
                }
        }
    }

    private fun uploadAadharFontImage(aadharFont:String){
        var volleyRequestQueue: RequestQueue? = null
        val TAG = "Handy Opinion Tutorials"

        volleyRequestQueue = Volley.newRequestQueue(this)
        val strReq: StringRequest = object : StringRequest(
            Method.POST,urlUtills?.addAddharFont(), Response.Listener { response ->
                Log.e(TAG, "response: " + response)
                dismissLoaderActivity()
                try {
                    val jsonArray = JSONArray("[$response]")
                    val jsonObject = jsonArray.getJSONObject(0)
                    val message = jsonObject.getString("message")
                    if (message == "ok"){
                        upload_font_aadhar.text = "Aadhar font updated"
                        //Toast.makeText(this,"aadhar font Uploaded",Toast.LENGTH_LONG).show()
                    }else{
                    }
                } catch (e: Exception) {
                    dismissLoaderActivity()
                    Toast.makeText(this, "Something went wrong!!!", Toast.LENGTH_SHORT).show()
                    e.printStackTrace()
                }
            },
            Response.ErrorListener { volleyError -> // error occurred
                dismissLoaderActivity()
                Toast.makeText(applicationContext, "Server encountered some problem !!!", Toast.LENGTH_SHORT).show()
            }) {

            override fun getParams(): MutableMap<String, String> {
                val params: MutableMap<String, String> = java.util.HashMap()
                params.put("aadharPhotoImage",aadharFont)
                params.put("loginid", loginId)
                return params
            }

            override fun getHeaders(): Map<String, String> {
                val headers: MutableMap<String, String> = HashMap()
                headers.put("Authorizations", token)
                return headers
            }
        }
        volleyRequestQueue?.add(strReq)
    }


    private fun uploadAadharBackFirebaseImage(){
        if (filePath != null) {
            val ref = storageRef.child("doctors/userImages/backAadhar")
            val uploadTask = ref.putFile(filePath)
            uploadTask.continueWithTask(Continuation<UploadTask.TaskSnapshot, Task<Uri>> { task ->
                if (!task.isSuccessful) {
                    task.exception?.let {
                        throw it
                    }
                }
                return@Continuation ref.downloadUrl
            })
                .addOnCompleteListener { task ->
                    dismissLoaderActivity()
                    if (task.isSuccessful) {
                        aadharBackUri = task.result
                        backPhoto = aadharBackUri.toString()
                        uploadAadharBackImage(aadharBackUri.toString())
                    }

                }
                .addOnFailureListener {
                    dismissLoaderActivity()
                    it.message
                    Toast.makeText(this, "Please upload an Image", Toast.LENGTH_LONG).show()
                }
        }
    }


    private fun uploadAadharBackImage(aadharBack:String){
        var volleyRequestQueue: RequestQueue? = null
        val TAG = "Handy Opinion Tutorials"

        volleyRequestQueue = Volley.newRequestQueue(this)
        val strReq: StringRequest = object : StringRequest(
            Method.POST,urlUtills?.addAadharBack(), Response.Listener { response ->
                Log.e(TAG, "response: " + response)
                try {
                    val jsonArray = JSONArray("[$response]")
                    val jsonObject = jsonArray.getJSONObject(0)
                    val message = jsonObject.getString("message")
                    if (message == "ok"){
                        upload_back_aadhar.text = "Aadhar back updated"
                        //Toast.makeText(this,"aadhar back Uploaded",Toast.LENGTH_LONG).show()
                    }else{
                    }
                } catch (e: Exception) {
                    Toast.makeText(this, "Something went wrong!!!", Toast.LENGTH_SHORT).show()
                    e.printStackTrace()
                }
            },
            Response.ErrorListener { volleyError -> // error occurred
                //
                Toast.makeText(applicationContext, "Server encountered some problem !!!", Toast.LENGTH_SHORT).show()
            }) {
            override fun getParams(): MutableMap<String, String> {
                val params: MutableMap<String, String> = java.util.HashMap()
                params.put("aadharBackPhoto",aadharBack)
                params.put("loginid", loginId)
                return params
            }

            override fun getHeaders(): Map<String, String> {
                val headers: MutableMap<String, String> = HashMap()
                headers.put("Authorizations", token)
                return headers
            }
        }
        volleyRequestQueue?.add(strReq)
    }

    override fun onBackPressed() {
        appointmentAlert()
    }

    private fun appointmentAlert() {
        val alertDialog: android.app.AlertDialog.Builder = android.app.AlertDialog.Builder(this)
        alertDialog.setTitle("KYC Alert !")
        alertDialog.setMessage("Are you sure ? Don't want KYC now!")
        alertDialog.setIcon(R.drawable.ic_baseline_warning)

        alertDialog.setPositiveButton("Yes") { _, _ ->

            val intent = Intent(this, DashActivity::class.java)
            startActivity(intent)
            this.finish()

        }
        alertDialog.setNegativeButton("Cancel") { dialog, _ ->
            dialog.cancel()
        }

        val alert = alertDialog.create()
        alert.setCanceledOnTouchOutside(false)
        alert.show()
        val negativeButton = alert.getButton(DialogInterface.BUTTON_NEGATIVE)
        negativeButton.setTextColor(Color.RED)

        val positiveButton = alert.getButton(DialogInterface.BUTTON_POSITIVE)
        positiveButton.setTextColor(Color.GREEN)
    }

}