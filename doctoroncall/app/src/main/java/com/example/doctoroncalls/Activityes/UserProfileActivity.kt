package com.example.doctoroncalls.Activityes

import android.content.ContentValues.TAG
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.example.doctoroncalls.Models.*
import com.example.doctoroncalls.R
import kotlinx.android.synthetic.main.activity_user_profile.*
import org.json.JSONArray

class UserProfileActivity : LoaderActivity() {
    private var urlUtills: UrlUtills? = UrlUtills()
    var sharedpreferences: SharedPreferences? = null
    private var sessionManager: SessionManager? = null
    val SHARED_PREFS = "shared_prefs"
    val ID = "id"
    var AUTH_TOKEN = "AUTH_TOKEN"
    var stateId = ""
    var stateName = ""
    var id = ""
    var distId = ""
    var login_toekn = ""
    private val allDistList = ArrayList<Dist>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user_profile)
        submit_details.setOnClickListener {
            updateProfile()
        }
        arrowBack.setOnClickListener {
            onBackPressed()
        }
        name_editText.setText(DoctorModels.name)
        if (DoctorModels.email.equals("null")){
            email_editText.setHint("Enter your email")
        }else {
            email_editText.setText(DoctorModels.email)
        }
        selectGender_editText.setText(DoctorModels.gender)
        address_editText.setText(DoctorModels.address)
        sessionManager = SessionManager(this@UserProfileActivity)
        sharedpreferences = getSharedPreferences(SHARED_PREFS, Context.MODE_PRIVATE)
        id = sharedpreferences?.getString(ID,"").toString()
        login_toekn = sharedpreferences?.getString(AUTH_TOKEN,"").toString()
        Log.e(TAG, "login: " + login_toekn)
    }

    private fun updateProfile(){
        var volleyRequestQueue: RequestQueue? = null
        val TAG = "Handy Opinion Tutorials"
        volleyRequestQueue = Volley.newRequestQueue(this)
        val strReq: StringRequest = object : StringRequest(Method.POST,urlUtills?.updateProfile(), Response.Listener { response ->
            dismissLoaderActivity()
            Log.e(TAG, "response: " + response)
                val jsonArray = JSONArray("[$response]")
            val message = jsonArray.getJSONObject(0).get("message")
            Toast.makeText(this,"Profile updated",Toast.LENGTH_LONG).show()
            if (message != null){
                val intent = Intent(this,DashActivity::class.java)
                startActivity(intent)
                finish()
            }
        },
            Response.ErrorListener{ volleyError -> // error occurred
                Toast.makeText(applicationContext, "Server encountered some problem !!!", Toast.LENGTH_SHORT).show()
            }) {
            override fun getParams(): MutableMap<String, String> {
                val params: MutableMap<String, String> = java.util.HashMap()
                params.put("loginid",id)
                params.put("name",name_editText.text.toString().trim())
                params.put("email",email_editText.text.toString().trim())
                params.put("gender",selectGender_editText.text.toString().trim())
                params.put("address",address_editText.text.toString().trim())
                return params
            }
            override fun getHeaders(): Map<String, String> {
                val headers: MutableMap<String, String> = HashMap()
                headers.put("Authorizations", login_toekn)
                return headers
            }
        }
        volleyRequestQueue?.add(strReq)
        showLoaderActivity()
    }

    }
