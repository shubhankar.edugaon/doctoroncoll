package com.example.doctoroncalls.Fragments

import android.app.AlertDialog
import android.app.DownloadManager
import android.content.*
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import android.icu.number.NumberFormatter.with
import android.icu.number.NumberRangeFormatter.with
import android.net.Uri
import android.os.*
import android.provider.MediaStore
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.bumptech.glide.GenericTransitionOptions.with
import com.bumptech.glide.Glide
import com.bumptech.glide.Glide.with
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.with
import com.example.doctoroncalls.Activityes.DashActivity
import com.example.doctoroncalls.Adapter.OrderListAdapter
import com.example.doctoroncalls.Models.*
import com.example.doctoroncalls.R
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_appointment_view_details.*
import kotlinx.android.synthetic.main.fragment_transation.*
import kotlinx.android.synthetic.main.fragment_transation.view.*
import kotlinx.android.synthetic.main.order_view.*
import kotlinx.android.synthetic.main.order_view.view.*
import org.json.JSONArray
import java.io.*
import java.net.HttpURLConnection
import java.net.MalformedURLException
import java.net.URL
import java.util.concurrent.Executors

class TransationFragment : LoaderFragment(), OrderListAdapter.OnItemClickListener{

    private var sessionManager: SessionManager? = null
    private var urlUtills: UrlUtills? = UrlUtills()
    var sharedpreferences: SharedPreferences? = null
    private var OrderList = ArrayList<OrderList>()
    var mImage: Bitmap? = null
    val myExecutor = Executors.newSingleThreadExecutor()
    val myHandler = Handler(Looper.getMainLooper())
    val SHARED_PREFS = "shared_prefs"
    var id = ""
    var AUTH_TOKEN = "AUTH_TOKEN"
    val ID = "id"
    var loginToken = ""
    var patientName = ""
    var patientAddress = ""
    var patientMobile = ""
    var time = ""
    var patientAge = ""
    var profileStatus = ""
    var doctorName = ""
    var bookingDate = ""
    var price = ""
    var orderImage = ""
    var pfl_status = ""
    private var appointmentLiat = ArrayList<DoctorList>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_transation, container, false)
        sessionManager = SessionManager(requireActivity().applicationContext)
        sharedpreferences = activity?.getSharedPreferences(SHARED_PREFS, Context.MODE_PRIVATE)
        id = sharedpreferences?.getString(ID,"").toString()
        loginToken = sharedpreferences?.getString(AUTH_TOKEN,"").toString()
        view.arroback.setOnClickListener {
            val intent = Intent(activity,DashActivity::class.java)
            startActivity(intent)
        }
        if (isOnline()) {
            getOrderList()
            displayLoaderFragment()
        }else{
            showAlert()
        }

        return view
    }

    private fun getOrderList(){
        var volleyRequestQueue: RequestQueue? = null
        val TAG = "Handy Opinion Tutorials"

        volleyRequestQueue = Volley.newRequestQueue(activity)
        val strReq: StringRequest = object : StringRequest(Method.POST, urlUtills?.orderList(), Response.Listener { response ->
            Log.e(TAG, "response: " + response)
            try {
                dismissLoader()
                val jsonArray = JSONArray("[$response]")
                val messageObj = jsonArray.getJSONObject(0).get("order") as JSONArray
                val count = messageObj.length()
                if (count == 0){
                    data_not_found.visibility = View.VISIBLE
                }else{
                    data_not_found.visibility = View.GONE
                }
                appointmentLiat.clear()
                messageObj.length()
                for (i in 0 until messageObj.length()){
                    val branchName = messageObj.getJSONObject(i).get("BRANCH_NAME")
                    val branchContact = messageObj.getJSONObject(i).get("BRANCH_CONTACT")
                    val consultrationFee = messageObj.getJSONObject(i).get("DOCTOR_CONSULTATION_FEE")
                    val orderDate = messageObj.getJSONObject(i).get("BRANCH_TT")
                    val prlId = messageObj.getJSONObject(i).get("PFL_ORDER_ID")
                    val PFL_PATIENT_NAME = messageObj.getJSONObject(i).get("PFL_PATIENT_NAME")
                    val PFL_PATIENT_AGE = messageObj.getJSONObject(i).get("PFL_PATIENT_AGE")
                    val  PFL_REPORT_FILE = messageObj.getJSONObject(i).get("PFL_REPORT_FILE")
                    val PFL_PATIENT_PHONE = messageObj.getJSONObject(i).get("PFL_PATIENT_PHONE")
                    val  PROFILE_STATUS = messageObj.getJSONObject(i).get("PROFILE_STATUS")
                    val BRANCH_ID = messageObj.getJSONObject(i).get("BRANCH_ID")
                    val PFL_STATUS = messageObj.getJSONObject(i).get("PFL_STATUS")
                    val PFL_DATE = messageObj.getJSONObject(i).get("PFL_DATE")
                    val PFL_PRICE = messageObj.getJSONObject(i).get("PFL_PRICE")
                    val PATIENT_PHOTO  = messageObj.getJSONObject(i).get("PATIENT_PHOTO")
                    val PFL_BRANCH_ID =  messageObj.getJSONObject(i).get("PFL_BRANCH_ID")
                    val order = OrderList(branchName.toString(),branchContact.toString(),prlId.toString(),orderDate.toString(),consultrationFee.toString(),PFL_PATIENT_NAME.toString(),PFL_PATIENT_AGE.toString(),PFL_REPORT_FILE.toString(),PFL_PATIENT_PHONE.toString(),PROFILE_STATUS.toString(),BRANCH_ID.toString(),PFL_STATUS.toString(),PFL_DATE.toString(),PFL_PRICE.toString(),PATIENT_PHOTO.toString(),PFL_BRANCH_ID.toString())
                    OrderList.add(order)
                    if(appointmentLiat !=null){
                        showOrderList(OrderList)
                    }
                    //branchId = profileObj.getJSONObject(i).getString("BRANCH_ID")
                }
                Log.d("profile", messageObj.toString())

            } catch (e: Exception) { // caught while parsing the response
                dismissLoader()
                Toast.makeText(activity, "Something went wrong!!!", Toast.LENGTH_SHORT).show()
                e.printStackTrace()
            }
        },
            Response.ErrorListener { volleyError -> // error occurred
                //
                dismissLoader()
                Toast.makeText(activity, "Server encountered some problem !!!", Toast.LENGTH_SHORT).show()
            }) {

            override fun getParams(): MutableMap<String, String> {
                val params: MutableMap<String, String> = java.util.HashMap()
                params.put("loginid",id)
                return params
            }

            override fun getHeaders(): Map<String, String> {
                val headers: MutableMap<String, String> = HashMap()
                headers.put("Authorizations", loginToken)
                // Add your Header paramters here
                return headers
            }
        }
        // Adding request to request queue
        volleyRequestQueue?.add(strReq)
    }

    private fun showOrderList(orderList: List<OrderList>){
        val orderListAdapter = OrderListAdapter(orderList,this)
        val layoutManager = LinearLayoutManager(activity,LinearLayoutManager.VERTICAL, false)
        orderListRecyclerview.layoutManager = layoutManager
        orderListRecyclerview.adapter = orderListAdapter
    }

    private fun showAlert() {
        val alertDialog: AlertDialog.Builder = AlertDialog.Builder(activity)
        alertDialog.setTitle("Check Connection!")
        alertDialog.setIcon(R.drawable.ic_baseline_warning)
        alertDialog.setPositiveButton("Try Again") { _, _ ->
            if (isOnline()) {
                displayLoaderFragment()
                getOrderList()
                Toast.makeText(activity, "You are Online!", Toast.LENGTH_SHORT).show()
            } else {
                showAlert()
                Toast.makeText(activity, "Please Go Online!", Toast.LENGTH_SHORT).show()
            }
        }
        alertDialog.setNegativeButton("Cancel") { dialog, id ->
            dialog.cancel()
        }

        val alert = alertDialog.create()
        alert.setCanceledOnTouchOutside(false)
        alert.show()
        val negativeButton = alert.getButton(DialogInterface.BUTTON_NEGATIVE)
        negativeButton.setTextColor(Color.RED)

        val positiveButton = alert.getButton(DialogInterface.BUTTON_POSITIVE)
        positiveButton.setTextColor(Color.GREEN)
    }

    override fun profileView(orderList: OrderList) {
       viewProfileDialog()
        patientName = orderList.PFL_PATIENT_NAME
        patientAge = orderList.PFL_PATIENT_AGE
        patientMobile = orderList.PFL_PATIENT_PHONE
        profileStatus = orderList.PROFILE_STATUS
        doctorName =  orderList.BRANCH_NAME
        bookingDate = orderList.PFL_DATE
        price = orderList.PFL_PRICE
        orderImage = orderList.PATIENT_PHOTO
        pfl_status = orderList.PFL_STATUS
    }
    private fun viewProfileDialog(){
        val mDialogView = LayoutInflater.from(activity).inflate(R.layout.order_view, null)
        val mBuilder = androidx.appcompat.app.AlertDialog.Builder(this.requireContext()).setView(mDialogView)
            mDialogView.orderPatientName.text = patientName
            mDialogView.orderPatientAddress.text = patientAddress
            mDialogView.oderPatientMobile.text = patientMobile
            mDialogView.orderDoctorName.text = doctorName
            mDialogView.bookingDate.text = bookingDate
        if (orderImage !=null) {
            Glide.with(this).load(orderImage).into(mDialogView.patientPhoto)
        }else{
            R.drawable.doctor
        }
      if (pfl_status == "6"){
          mDialogView.cancel_button.visibility = View.VISIBLE
          mDialogView.ok_button.visibility = View.GONE
      }
//        mDialogView.download_btn.setOnClickListener {
//            myExecutor.execute {
//                mImage = mLoad(orderImage)
//                myHandler.post {
//                   // mDialogView.patientPhoto.setImageBitmap(mImage)
//                    if (mImage != null) {
//                        mSaveMediaToStorage(mImage)
//                    }
//                }
//            }
//        }
        val  mAlertDialog = mBuilder.show()
        mDialogView.ok_button.setOnClickListener {
            mAlertDialog.dismiss()
        }
        mDialogView.next_button.setOnClickListener {
            mAlertDialog.dismiss()
        }
    }


    private fun mLoad(string: String): Bitmap? {
        val url: URL = mStringToURL(string)!!
        val connection: HttpURLConnection?
        try {
            connection = url.openConnection() as HttpURLConnection
            connection.connect()
            val inputStream: InputStream = connection.inputStream
            val bufferedInputStream = BufferedInputStream(inputStream)
            return BitmapFactory.decodeStream(bufferedInputStream)
        } catch (e: IOException) {
            e.printStackTrace()
            Toast.makeText(activity, "Error", Toast.LENGTH_LONG).show()
        }
        return null
    }

    private fun mStringToURL(string: String): URL? {
        try {
            return URL(string)
        } catch (e: MalformedURLException) {
            e.printStackTrace()
        }
        return null
    }

    private fun mSaveMediaToStorage(bitmap: Bitmap?) {
        val filename = "${System.currentTimeMillis()}.jpg"
        var fos: OutputStream? = null
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            activity?.contentResolver?.also { resolver ->
                val contentValues = ContentValues().apply {
                    put(MediaStore.MediaColumns.DISPLAY_NAME, filename)
                    put(MediaStore.MediaColumns.MIME_TYPE, "image/jpg")
                    put(MediaStore.MediaColumns.RELATIVE_PATH, Environment.DIRECTORY_PICTURES)
                }
                val imageUri: Uri? =
                    resolver.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, contentValues)
                fos = imageUri?.let { resolver.openOutputStream(it) }
            }
        } else {
            val imagesDir =
                Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES)
            val image = File(imagesDir, filename)
            fos = FileOutputStream(image)
        }
        fos?.use {
            bitmap?.compress(Bitmap.CompressFormat.JPEG, 100, it)
            Toast.makeText(activity, "Saved to Gallery", Toast.LENGTH_SHORT).show()
        }
    }
}