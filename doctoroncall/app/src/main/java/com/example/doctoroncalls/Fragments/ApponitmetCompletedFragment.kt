package com.example.doctoroncalls.Fragments

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.example.doctoroncalls.Activityes.AppointmentViewDetailsActivity
import com.example.doctoroncalls.Adapter.AppointmentListAdapter
import com.example.doctoroncalls.Models.Doctor
import com.example.doctoroncalls.Models.DoctorList
import com.example.doctoroncalls.Models.SessionManager
import com.example.doctoroncalls.Models.UrlUtills
import com.example.doctoroncalls.R
import kotlinx.android.synthetic.main.fragment_appointment_pending.*
import kotlinx.android.synthetic.main.fragment_appointment_pending.doctorAppointmentList
import kotlinx.android.synthetic.main.fragment_apponitmet_completed.*
import kotlinx.android.synthetic.main.fragment_meeting_upcomig.*
import kotlinx.android.synthetic.main.fragment_order_list.*
import org.json.JSONArray

class ApponitmetCompletedFragment : LoaderFragment(), AppointmentListAdapter.DoctorProfileOnItemClickListener {

    private var sessionManager: SessionManager? = null
    private var urlUtills: UrlUtills? = UrlUtills()
    var sharedpreferences: SharedPreferences? = null
    private var doctorList = ArrayList<Doctor>()
    val SHARED_PREFS = "shared_prefs"
    var id = ""
    val ID = "id"
    var token = ""
    var patientName = ""
    var appointmentDate = ""
    var AUTH_TOKEN = "AUTH_TOKEN"
    var loginToken = ""


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_apponitmet_completed, container, false)
        sessionManager = SessionManager(requireActivity().applicationContext)
        sharedpreferences = activity?.getSharedPreferences(SHARED_PREFS, Context.MODE_PRIVATE)
        id = sharedpreferences?.getString(ID,"").toString()
        loginToken = sharedpreferences?.getString(AUTH_TOKEN,"").toString()

//        if (isOnline()) {
//            getAppointmetList()
//            displayLoaderFragment()
//        }else{
//            showAlert()
//        }

        return view
    }

    override fun onResume() {
        super.onResume()
        getAppointmetList()
    }

    private fun getAppointmetList(){
        var volleyRequestQueue: RequestQueue? = null
        val TAG = "Handy Opinion Tutorials"
        volleyRequestQueue = Volley.newRequestQueue(activity)
        val strReq: StringRequest = @SuppressLint("SetTextI18n")
        object : StringRequest(
            Method.POST, urlUtills?.appointList(), Response.Listener { response ->
                try {
                    //dismissLoader()
                    val jsonArray = JSONArray("[$response]")
                    var order = jsonArray.getJSONObject(0).getString("order")
                    Log.e(TAG,"order: " + order)
                    if (order == "Sorry No Data Found"){
                        Toast.makeText(activity,"Sorry No Data Found", Toast.LENGTH_LONG).show()
                    }else {
                        val messageObj = jsonArray.getJSONObject(0).get("order") as JSONArray
                        var appointmentLiat = ArrayList<DoctorList>()
                        appointmentLiat.clear()
                        val count = messageObj.length()
                        if (count == 0) {
                            cancel_data_not_found.visibility = View.VISIBLE
                            cancel_data_not_found.text = "Data not found?"
                        } else {
                            cancel_data_not_found.visibility = View.GONE
                        }
                        totalAppointmentPending_textView.text =
                            "Total Appointment: " + count.toString()
                        for (i in 0 until messageObj.length()) {
                            patientName = messageObj.getJSONObject(i).getString("PATIENT_NAME")
                            val BRANCH_NAME = messageObj.getJSONObject(i).getString("BRANCH_NAME")
                            val PATIENT_ADDRESS =
                                messageObj.getJSONObject(i).getString("PATIENT_ADDRESS")
                            val DA_DISEAS = messageObj.getJSONObject(i).getString("DA_DISEAS")
                            val APPOINT_DATE = messageObj.getJSONObject(i).getString("APPOINT_DATE")
                            val MESSAGE = messageObj.getJSONObject(i).getString("MESSAGE")
                            val DA_ID = messageObj.getJSONObject(i).getString("DA_ID")
                            val BRANCH_TOKEN = messageObj.getJSONObject(i).getString("BRANCH_TOKEN")
                            val DA_LOGIN_ID = messageObj.getJSONObject(i).getString("DA_LOGIN_ID")
                            val DMS_START_TIME =
                                messageObj.getJSONObject(i).getString("DMS_START_TIME")
                            val DMS_END_TIME = messageObj.getJSONObject(i).getString("DMS_END_TIME")
                            val PATIENT_PHOTO =
                                messageObj.getJSONObject(i).getString("PATIENT_PHOTO")
                            val PATIENT_MOBILE =
                                messageObj.getJSONObject(i).getString("PATIENT_MOBILE")
                            val DA_MEETING_LINK =
                                messageObj.getJSONObject(i).getString("DA_MEETING_LINK")

                            val appoinmte = DoctorList(
                                patientName,
                                BRANCH_NAME,
                                PATIENT_ADDRESS,
                                DA_DISEAS,
                                APPOINT_DATE,
                                MESSAGE,
                                DA_ID,
                                BRANCH_TOKEN,
                                DA_LOGIN_ID,
                                DMS_START_TIME,
                                DMS_END_TIME,
                                PATIENT_PHOTO,
                                PATIENT_MOBILE,
                                DA_MEETING_LINK
                            )
                            if (MESSAGE != "Appointment Confirmed" && MESSAGE != "Waiting For Appointment") {
                                appointmentLiat.add(appoinmte)
                            }
                            showAppointmentList(appointmentLiat)
                            //branchId = profileObj.getJSONObject(i).getString("BRANCH_ID")
                            val count = appointmentLiat.size
                            totalAppointmentPending_textView.text =
                                "Total Appointments " + count.toString()
                        }
                    }
                } catch (e: Exception) { // caught while parsing the response
                    //dismissLoader()
                    Toast.makeText(activity, "Something went wrong!!!", Toast.LENGTH_SHORT).show()
                    e.printStackTrace()
                }
            },
            Response.ErrorListener { volleyError -> // error occurred
                //dismissLoader()
                Toast.makeText(activity, "Server encountered some problem !!!", Toast.LENGTH_SHORT).show()
            }) {

            override fun getParams(): MutableMap<String, String> {
                val params: MutableMap<String, String> = java.util.HashMap()
                params.put("loginid",id)
                return params
            }

            override fun getHeaders(): Map<String, String> {
                val headers: MutableMap<String, String> = HashMap()
                headers.put("Authorizations", loginToken)
                // Add your Header paramters here
                return headers
            }
        }
        volleyRequestQueue?.add(strReq)
    }

    private fun showAppointmentList(showAppointmentList: List<DoctorList>){
        val doctorAdapter = AppointmentListAdapter(showAppointmentList,this)
        val layoutManage = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL,false)
        doctorAppointmentList.layoutManager = layoutManage
        doctorAppointmentList.adapter = doctorAdapter
    }
    private fun showAlert() {
        val alertDialog: AlertDialog.Builder = AlertDialog.Builder(activity)
        alertDialog.setTitle("Check Connection!")
        alertDialog.setIcon(R.drawable.ic_baseline_warning)
        alertDialog.setPositiveButton("Try Again") { _, _ ->
            if (isOnline()) {
                displayLoaderFragment()
                getAppointmetList()
                Toast.makeText(activity, "You are Online!", Toast.LENGTH_SHORT).show()
            } else {
                showAlert()
                Toast.makeText(activity, "Please Go Online!", Toast.LENGTH_SHORT).show()
            }
        }
        alertDialog.setNegativeButton("Cancel") { dialog, id ->
            dialog.cancel()
        }

        val alert = alertDialog.create()
        alert.setCanceledOnTouchOutside(false)
        alert.show()
        val negativeButton = alert.getButton(DialogInterface.BUTTON_NEGATIVE)
        negativeButton.setTextColor(Color.RED)

        val positiveButton = alert.getButton(DialogInterface.BUTTON_POSITIVE)
        positiveButton.setTextColor(Color.GREEN)
    }

    override fun docotrList(doctorList: DoctorList) {
        val  intent = Intent(activity, AppointmentViewDetailsActivity::class.java)
        intent.putExtra("appointmentId",doctorList.DA_ID)
        intent.putExtra("token", doctorList.BRANCH_TOKEN)
        intent.putExtra("appointmentDate", appointmentDate)
        intent.putExtra("patientName", doctorList.PATIENT_NAME)
        intent.putExtra("doctor_name",doctorList.BRANCH_NAME)
        intent.putExtra("da_login_id",doctorList.DA_LOGIN_ID)
        intent.putExtra("imageUrl",  doctorList.PATIENT_PHOTO)
        intent.putExtra("message",doctorList.MESSAGE)
        intent.putExtra("status", "")
        startActivity(intent)
    }

    override fun startCall(doctorList: DoctorList) {
        Toast.makeText(activity,"onClick",Toast.LENGTH_LONG).show()
    }


}