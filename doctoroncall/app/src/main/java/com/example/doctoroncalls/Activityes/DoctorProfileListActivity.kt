package com.example.doctoroncalls.Activityes

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.widget.*
import android.widget.SearchView
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.bumptech.glide.Glide
import com.example.doctoroncalls.Adapter.DoctorAdapter
import com.example.doctoroncalls.Models.*
import com.example.doctoroncalls.R
import kotlinx.android.synthetic.main.activity_doctor_profile_list.*
import kotlinx.android.synthetic.main.doctor_alert_dialog.view.*
import kotlinx.android.synthetic.main.doctor_list_item.view.*
import org.json.JSONArray

class DoctorProfileListActivity : LoaderActivity(), DoctorAdapter.OnItemClickListener {
    lateinit var doctorListRecycler: RecyclerView
    lateinit var dataNotAvailabele: TextView
    lateinit var ambulanceList: TextView
    private var doctorList = ArrayList<Doctor>()
    private var urlUtills: UrlUtills? = UrlUtills()
    private var sessionManager: SessionManager? = null
    val SHARED_PREFS = "shared_prefs"
    val KEY_LOGIN_TOKEN = "KEY_LOGIN_TOKEN"
    var AUTH_TOKEN = "AUTH_TOKEN"
    val ID = "id"
    var sharedpreferences: SharedPreferences? = null
    var id: String? = ""
    var distId: String? = ""
    var branchId: String? = ""
    var branchName: String? = ""
    var name = ""
    var profileStatus: String? = ""
    var searchView: String? = ""
    var auth_token = ""
    var specialist = ""
    var loginId = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_doctor_profile_list)

        sessionManager = SessionManager(this@DoctorProfileListActivity)
        sharedpreferences = getSharedPreferences(SHARED_PREFS, Context.MODE_PRIVATE)
        auth_token = sharedpreferences?.getString(AUTH_TOKEN,"").toString()
        loginId = sharedpreferences?.getString(ID,"").toString()
        doctorListRecycler= findViewById(R.id.doctorList_recyclerView)
        searchView = findViewById<View?>(R.id.searchView_name).toString()
        dataNotAvailabele = findViewById(R.id.dataNotAvailabe_title)
        ambulanceList = findViewById(R.id.amulance_title)
        val back = findViewById<ImageView>(R.id.arroback)

        back.setOnClickListener {
            onBackPressed()
        }
        id = intent.getStringExtra("id").toString()
        distId = intent.getStringExtra("distId").toString()

        if (id == "3"){
            val ambulanceListT = "Ambulance"
            specialist = ambulanceListT
        } else if (id == "6") {
            val chemust = "Chemist"
            specialist = chemust
        } else if (id == "2"){
                val pathologyTitle = "Pathology doctor"
            specialist = pathologyTitle
        } else if (id == "1") {
            val alopathy = "Allopathic doctor"
            specialist = alopathy
        } else if (id == "4"){
            val homo = "Homeopathy doctor"
            specialist= homo
        } else if (id == "5"){
            val ayurveda = "Ayurveda"
            specialist = ayurveda
        }
        else{

        }
        getPreToken()
        searchView_name.setOnQueryTextListener(object : SearchView.OnQueryTextListener{
            override fun onQueryTextSubmit(p0: String?): Boolean {
                return false
            }
            override fun onQueryTextChange(searchText: String?): Boolean {
                    val filter: ArrayList<Doctor> = ArrayList()
                if (doctorList.size != 0){
                        doctorList.forEach {
                            if (searchText!= null){
                                if (it.BRANCH_NAME!!.contains(searchText)){
                                    filter.add(it)
                                }
                            }
                                showList(filter)
                        }
                }
                return true
            }
        })
    }

    private fun getPreToken(){
        var volleyRequestQueue: RequestQueue? = null
        val TAG = "Handy Opinion Tutorials"
        volleyRequestQueue = Volley.newRequestQueue(this)
        val strReq: StringRequest = object : StringRequest(Method.POST, urlUtills?.doctorDetailsListAPi()+"$id", Response.Listener { response ->
            Log.e(TAG, "response: " + response)
            try {
                dismissLoaderActivity()
                    doctorList.clear()
                val jsonArray = JSONArray("[$response]")
                val messageObj = jsonArray.getJSONObject(0).get("message")
                val messageArray = JSONArray("[$messageObj]")
                val profileObj = messageArray.getJSONObject(0).get("profile") as JSONArray
                val  count =profileObj.length()
                val p = count.toString()+"{$specialist(s)}"+"found"
                doctorNumber.text = p
                if (count == 0){
                    dataNotAvailabe_title.text = "data not available?"
                    dataNotAvailabe_title.visibility = View.VISIBLE
                }else{
                    dataNotAvailabele.visibility = View.GONE

                }
                for (i in 0 until profileObj.length()){
                    val branchId = profileObj.getJSONObject(i).getString("BRANCH_ID")
                    val branchName = profileObj.getJSONObject(i).getString("BRANCH_NAME")
                    val profileStatus = profileObj.getJSONObject(i).getString("PROFILE_STATUS")
                    val branch_cont = profileObj.getJSONObject(i).getString("BRANCH_CONTACT")
                    val pranchLocation  = profileObj.getJSONObject(i).getString("LOCATION")
                    val experience = profileObj.getJSONObject(i).getString("EXPERIENCE")
                    val branchEmail = profileObj.getJSONObject(i).getString("BRANCH_EMAIL")
                    val branchPhoto = profileObj.getJSONObject(i).getString("PATIENT_PHOTO")
                    val branch_status = profileObj.getJSONObject(i).getString("BRANCH_STATUS")
                    val consultationFees = profileObj.getJSONObject(i).getString("DOCTOR_CONSULTATION_FEE")
                    val BRANCH_ONLINE_FEE = profileObj.getJSONObject(i).getString("BRANCH_ONLINE_FEE")
                    val firmId = profileObj.getJSONObject(i).getString("FIRM_ID")
                    val branchCode = profileObj.getJSONObject(i).getString("BRANCH_CODE")
                    val BRANCH_TOKEN = profileObj.getJSONObject(i).getString("BRANCH_TOKEN")
                    val BRANCH_ADDRESS = profileObj.getJSONObject(i).getString("BRANCH_ADDRESS")
                    val SPECIALITY = profileObj.getJSONObject(i).getString("SPECIALITY")
                    val license_no = profileObj.getJSONObject(i).getString("LICENCE_NO")
                    val ambulanse_no = profileObj.getJSONObject(i).getString("AMBULANCE_NO")
                    val doctor = Doctor(branchId,firmId,branchName,branchCode,branch_cont,profileStatus,consultationFees,branchEmail,experience,pranchLocation,BRANCH_TOKEN,branchPhoto,BRANCH_ADDRESS, SPECIALITY,BRANCH_ONLINE_FEE,branch_status,license_no,ambulanse_no)
                    doctorList.add(doctor)
                    }
                if (doctorList != null){
                    showList(doctorList)
                }else{
                    doctorListRecycler.visibility = View.VISIBLE
                    doctorListRecycler.visibility = View.GONE
                }
                Log.d("profile", profileObj.toString())

            } catch (e: Exception) { // caught while parsing the response
                dismissLoaderActivity()
                Toast.makeText(this, "Something went wrong!!!", Toast.LENGTH_SHORT).show()
                e.printStackTrace()
            }
        },
            Response.ErrorListener { volleyError -> // error occurred
                dismissLoaderActivity()
                Toast.makeText(applicationContext, "Server encountered some problem !!!", Toast.LENGTH_SHORT).show()
            }) {

            override fun getParams(): MutableMap<String, String> {
                val params: MutableMap<String, String> = java.util.HashMap()
                params.put("districtId",distId.toString())
                return params
            }

            override fun getHeaders(): Map<String, String> {
                val headers: MutableMap<String, String> = HashMap()
                // Add your Header paramters here
                headers.put("Authorizations", auth_token)
                return headers
            }
        }
        // Adding request to request queue
        volleyRequestQueue.add(strReq)
        showLoaderActivity()
    }

private fun showList(doctorList: List<Doctor>){
     val doctorAdapter = DoctorAdapter(doctorList, this)
    val layoutManage = LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false)
            doctorListRecycler.layoutManager = layoutManage
            doctorListRecycler.adapter = doctorAdapter
}

    override fun viewButton(doctor: Doctor) {
         val intent = Intent(this,DoctorProfileActivity::class.java)
        intent.putExtra("PROFILE_STATUS", doctor.PROFILE_STATUS)
        intent.putExtra("BRANCH_NAME", doctor.BRANCH_NAME)
        intent.putExtra("pranchContact", doctor.BRANCH_CONTACT)
        intent.putExtra("location", doctor.LOCATION)
        intent.putExtra("email", doctor.BRANCH_EMAIL)
        intent.putExtra("doctorPhoto", doctor.PATIENT_PHOTO)
        intent.putExtra("consultationFees", doctor.DOCTOR_CONSULTATION_FEE)
        intent.putExtra("online", doctor.BRANCH_ONLINE_FEE)
        intent.putExtra("experience", doctor.EXPERIENCE)
        intent.putExtra("SPECIALITY",doctor.SPECIALITY)
        intent.putExtra("address", doctor.BRANCH_ADDRESS)
        intent.putExtra("doctorId",doctor.BRANCH_ID)
        intent.putExtra("licenseNo", doctor.LICENCE_NO)
        intent.putExtra("ambulanceNo", doctor.AMBULANCE_NO)
        startActivity(intent)
    }

    override fun bookAppointment(doctor: Doctor) {
        val image = doctor.PATIENT_PHOTO
        if (loginId == doctor.BRANCH_ID){
            if (image != null) {
                walletsDialog(image)
            }
        }else {
            val intent = Intent(this, BookAppointmentActivity::class.java)
            intent.putExtra("PROFILE_STATUS", doctor.PROFILE_STATUS)
            intent.putExtra("BRANCH_NAME", doctor.BRANCH_NAME)
            intent.putExtra("profileStatus", doctor.PROFILE_STATUS)
            intent.putExtra("pranchContact", doctor.BRANCH_CONTACT)
            intent.putExtra("BRANCH_TOKEN", doctor.BRANCH_TOKEN)
            intent.putExtra("distId", distId)
            intent.putExtra("branchId", doctor.BRANCH_ID)
            intent.putExtra("doctorId", doctor.BRANCH_ID)
            intent.putExtra("BRANCH_STATUS", doctor.BRANCH_STATUS)
            intent.putExtra("appointmentType", "2")
            intent.putExtra("fee", doctor.DOCTOR_CONSULTATION_FEE)
            startActivity(intent)
        }
    }

    override fun addPrescription(doctor: Doctor) {
        val image = doctor.PATIENT_PHOTO
        if (loginId == doctor.BRANCH_ID){
            if (image != null) {
                walletsDialog(image)
            }
        }else {
            val intent = Intent(this, AddPrescriptionActivity::class.java)
            intent.putExtra("branch_id", doctor.BRANCH_ID)
            intent.putExtra("DOCTOR_CONSULTATION_FEE", doctor.DOCTOR_CONSULTATION_FEE)
            intent.putExtra("BRANCH_TOKEN", doctor.BRANCH_TOKEN)
            intent.putExtra("doctorId", doctor.BRANCH_ID)
            intent.putExtra("doctorName", doctor.BRANCH_NAME)
            intent.putExtra("PROFILE_STATUS", doctor.PROFILE_STATUS)
            startActivity(intent)
        }
    }

    override fun callToAmbulance(doctor: Doctor) {
        val  image = doctor.PATIENT_PHOTO
        if (loginId == doctor.BRANCH_ID){
            if (image != null) {
                walletsDialog(image)
            }
        }else {
            makeCall(doctor.BRANCH_CONTACT.toString())
        }
    }

    override fun onlineConsult(doctor: Doctor) {
        if (loginId == doctor.BRANCH_ID){
            val image = doctor.PATIENT_PHOTO
            if (image != null) {
                walletsDialog(image)
            }
        }else {
            val intent = Intent(this, BookAppointmentActivity::class.java)
            intent.putExtra("PROFILE_STATUS", doctor.PROFILE_STATUS)
            intent.putExtra("BRANCH_NAME", doctor.BRANCH_NAME)
            intent.putExtra("profileStatus", doctor.PROFILE_STATUS)
            intent.putExtra("pranchContact", doctor.BRANCH_CONTACT)
            intent.putExtra("onlineConsult", doctor.BRANCH_ONLINE_FEE)
            intent.putExtra("BRANCH_TOKEN", doctor.BRANCH_TOKEN)
            intent.putExtra("distId", distId)
            intent.putExtra("branchId", doctor.BRANCH_ID)
            intent.putExtra("doctorId", doctor.BRANCH_ID)
            intent.putExtra("BRANCH_STATUS", doctor.BRANCH_STATUS)
            intent.putExtra("appointmentType", "1")
            intent.putExtra("fee",doctor.BRANCH_ONLINE_FEE)
            startActivity(intent)
        }
    }

    override fun viewProfile(doctor: Doctor) {
        val intent = Intent(this,DoctorProfileActivity::class.java)
        intent.putExtra("PROFILE_STATUS", doctor.PROFILE_STATUS)
        intent.putExtra("BRANCH_NAME", doctor.BRANCH_NAME)
        intent.putExtra("pranchContact", doctor.BRANCH_CONTACT)
        intent.putExtra("location", doctor.LOCATION)
        intent.putExtra("email", doctor.BRANCH_EMAIL)
        intent.putExtra("doctorPhoto", doctor.PATIENT_PHOTO)
        intent.putExtra("dist", doctor.DOCTOR_CONSULTATION_FEE)
        intent.putExtra("consultationFees", doctor.DOCTOR_CONSULTATION_FEE)
        intent.putExtra("experience", doctor.EXPERIENCE)
        intent.putExtra("SPECIALITY",doctor.SPECIALITY)
        intent.putExtra("address",doctor.BRANCH_ADDRESS)
        intent.putExtra("doctorId",doctor.BRANCH_ID)
        intent.putExtra("licenseNo", doctor.LICENCE_NO)
        intent.putExtra("ambulanceNo", doctor.AMBULANCE_NO)
        startActivity(intent)

    }

    private fun makeCall(number: String){
        val intent = Intent(Intent.ACTION_CALL)
        intent.data = Uri.parse("tel:$number")
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.CALL_PHONE), 100)
           // Toast.makeText(this,"Permission denied",Toast.LENGTH_LONG).show()
            return
        }
        startActivity(intent)
    }
    private fun walletsDialog(image:String){
        val mDialogView = LayoutInflater.from(this).inflate(R.layout.doctor_alert_dialog, null)
        val mBuilder = AlertDialog.Builder(this).setView(mDialogView)
        val  mAlertDialog = mBuilder.show()
        if (id == "3"){
            mDialogView.title_textView.text = "You are Ambulance driver. you don't need to book Ambulance"
        } else if (id == "6") {
            mDialogView.title_textView.text = "You are chemist and you don't need to update meeting schedule"
        } else if (id == "2"){
            mDialogView.title_textView.text = "You are Pathology and you don't need to update meeting schedule"
        } else if (id == "1") {
            mDialogView.title_textView.text = "You are Pathology and you don't need to update meeting schedule"
        } else if (id == "4"){
            mDialogView.title_textView.text = "You are Homeopathy and you don't need to update meeting schedule"
        } else if (id == "5"){
            mDialogView.title_textView.text = "You are Ayurveda and you don't need to update meeting schedule"
        }
        else{

        }
        Glide.with(this).load(image).into(mDialogView.image_imageView)
        mAlertDialog.setCanceledOnTouchOutside(false)

        mDialogView.ok_button_dialog.setOnClickListener {
            mAlertDialog.dismiss()
        }
        mBuilder.setCancelable(false)
    }

}