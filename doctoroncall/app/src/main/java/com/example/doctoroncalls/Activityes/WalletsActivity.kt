package com.example.doctoroncalls.Activityes

import android.content.ContentValues
import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.example.doctoroncalls.Models.DoctorModels
import com.example.doctoroncalls.Models.SessionManager
import com.example.doctoroncalls.Models.UrlUtills
import com.example.doctoroncalls.R
import com.razorpay.Checkout
import com.razorpay.PaymentResultListener
import kotlinx.android.synthetic.main.activity_wallets.*
import org.json.JSONArray
import org.json.JSONObject


class WalletsActivity : LoaderActivity(), PaymentResultListener {
    lateinit var addAmountBtn: Button
    lateinit var money: TextView
    var sharedpreferences: SharedPreferences? = null
    val SHARED_PREFS = "shared_prefs"
    val KEY_ID = "KEY_ID"
    val ID = "id"
    val KEY_SECRET = "KEY_SECRET"
    var USER_CONTACT = "USER_CONTACT"
    var USER_EMAIL = "USER_EMAIL"
    var AUTH_TOKEN = "AUTH_TOKEN"
    var payment_key = ""
    var loginId = ""
    private var transatioId = ""
    private var urlUtills: UrlUtills  = UrlUtills()
    var doller = null
    var amountRupayes = ""
    var mobile = ""
    var contact = ""
    var email = ""
    var loginToken = ""
    var payment = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_wallets)
        addAmountBtn = findViewById(R.id.addAmount_button)
        money = findViewById(R.id.amount_textView)

        //getBalance()

        arroback.setOnClickListener {
            onBackPressed()
        }
        var sessionManager = SessionManager(this@WalletsActivity)
        sharedpreferences = getSharedPreferences(SHARED_PREFS, Context.MODE_PRIVATE)
         payment_key = sharedpreferences?.getString(KEY_ID,"").toString()
        loginId = sharedpreferences?.getString(ID,"").toString()
        val secretId = sharedpreferences?.getString(KEY_SECRET,"").toString()
        contact = sharedpreferences?.getString(USER_CONTACT,"").toString()
        email = sharedpreferences?.getString(USER_EMAIL,"").toString()
        loginToken = sharedpreferences?.getString(AUTH_TOKEN,"").toString()

        addAmountBtn.setOnClickListener {
            chargeable_amountLayout.visibility = View.VISIBLE
//            val samount = findAmount_editText.getText().toString()
//            startPayment(samount.toInt())
        }
        pay_button.setOnClickListener {
             payment = startPayment_editText.text.toString()
            startPayment()
        }
    }

    override fun onResume() {
        super.onResume()
        getBalance()
    }

    private fun startPayment() {
        val checkout = Checkout()
        val o = checkout.setKeyID(payment_key)
                checkout.setImage(R.drawable.logo)
        val activity = this
        try {
            val options = JSONObject()
            options.put("name", "Doctoroncalls")
            options.put("description", "Wallets charge")
            options.put("currency", "INR")
            var total = payment.toDouble()
            total = total * 100
            options.put("amount", total)
            val preFill = JSONObject()
            preFill.put("email", email)
            preFill.put("contact", contact)
            checkout.open(this,options)
        }
        catch (e:Exception) {
            Toast.makeText(activity, "Error in payment: " + e.message, Toast.LENGTH_SHORT).show()
        }
    }

    override fun onPaymentSuccess(transationid: String) {
        dismissLoaderActivity()
                transatioId = transationid.toString()
        Toast.makeText(this,"Payment successfully",Toast.LENGTH_LONG).show()
        chargeable_amountLayout.visibility = View.GONE
        if (transationid != null){
           addBalance()
        }
    }

    override fun onPaymentError(p0: Int, p1: String?) {
    dismissLoaderActivity()
    }

    private fun addBalance() {
        var volleyRequestQueue: RequestQueue? = null
        volleyRequestQueue = Volley.newRequestQueue(this)
        val strReq: StringRequest = object : StringRequest(Method.POST, urlUtills?.addAmount(),
            Response.Listener { response ->
                //dismissLoaderActivity()
                Log.e(ContentValues.TAG, "response: " + response)
                try {
                    val jsonArray = JSONArray("[$response]")
                    val message = jsonArray.getJSONObject(0).get("message")
                    if (message == "success"){
                        Toast.makeText(this,"Payment add successfully !!",Toast.LENGTH_LONG).show()
                        chargeable_amountLayout.visibility = View.GONE
                    }
                } catch (e: Exception) { // caught while parsing the response
                   // dismissLoaderActivity()
                    Toast.makeText(this, "Something went wrong!!!", Toast.LENGTH_SHORT).show()
                    e.printStackTrace()
                }
            },
            Response.ErrorListener { volleyError -> // error occurred
              //  dismissLoaderActivity()
                Toast.makeText(this, "Server encountered some problem !!!", Toast.LENGTH_SHORT
                ).show()
            }) {

            override fun getParams(): MutableMap<String, String> {
                val params: MutableMap<String, String> = java.util.HashMap()
                params.put("loginid", loginId)
                params.put("transactionid",transatioId)
                params.put("amount", payment)
                params.put("payment_type", "1")
                return params
            }

            override fun getHeaders(): Map<String, String> {
                val headers: MutableMap<String, String> = HashMap()
                headers.put("Authorizations", loginToken)
                // Add your Header paramters here
                return headers
            }
        }
        // Adding request to request queue
        volleyRequestQueue?.add(strReq)

    }
    private fun getBalance() {
        var volleyRequestQueue: RequestQueue? = null
        volleyRequestQueue = Volley.newRequestQueue(this)
        val strReq: StringRequest = object : StringRequest(Method.POST, urlUtills?.getCurrentBalance(),
                Response.Listener { response ->
                    dismissLoaderActivity()
                    Log.e(ContentValues.TAG, "response: " + response)
                    try {
                        val jsonArray = JSONArray("[$response]")
                        val balance = jsonArray.getJSONObject(0).get("balance")
                        amount_textView.text = "₹"+balance+" "+"/-"
                    } catch (e: Exception) { // caught while parsing the response
                        dismissLoaderActivity()
                        Toast.makeText(this, "Something went wrong!!!", Toast.LENGTH_SHORT).show()
                        e.printStackTrace()
                    }
                },
                Response.ErrorListener { volleyError -> // error occurred
                    dismissLoaderActivity()
                    Toast.makeText(
                        this, "Server encountered some problem !!!", Toast.LENGTH_SHORT
                    ).show()
                }) {

                override fun getParams(): MutableMap<String, String> {
                    val params: MutableMap<String, String> = java.util.HashMap()
                            params.put("loginid", loginId)
                    return params
                }

                override fun getHeaders(): Map<String, String> {
                    val headers: MutableMap<String, String> = HashMap()
                    headers.put("Authorizations", loginToken)
                    return headers
                }
            }
        // Adding request to request queue
        volleyRequestQueue?.add(strReq)
        //showLoaderActivity()
    }
}